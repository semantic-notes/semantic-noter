#!/bin/bash

kubectl create secret generic clouddns-dns01-solver-svc-acct \
 --namespace cert-manager \
 --from-file=gcloud-key.json
