package com.gardi.ontoEntity

import akka.NotUsed
import com.lightbend.lagom.scaladsl.api.Service.restCall
import com.lightbend.lagom.scaladsl.api._
import com.lightbend.lagom.scaladsl.api.transport.Method
import org.gardi.dto.OntoIndividualDto

trait OntoEntityService extends Service {

  def getEntity(uid: String, threadId: String, indId: String): ServiceCall[NotUsed, OntoIndividualDto]
  def createEntity(uid: String, threadId: String): ServiceCall[OntoIndividualDto, OntoIndividualDto]
  def updateEntity(uid: String, threadId: String, indId: String): ServiceCall[OntoIndividualDto, OntoIndividualDto]
  def deleteEntity(uid: String, threadId: String, indId: String): ServiceCall[OntoIndividualDto, Boolean]

  override final def descriptor = {
    import Service._    // @formatter:off
    named("onto-entity-service")
      .withCalls(
        restCall(Method.POST, "/api/internal/user/:uid/threads/:id/ind", createEntity _),
        restCall(Method.GET, "/api/internal/user/:uid/threads/:id/ind/:ind", getEntity _),
        restCall(Method.PUT, "/api/internal/user/:uid/threads/:id/ind/:ind", updateEntity _),
        restCall(Method.DELETE, "/api/internal/user/:uid/threads/:id/ind/:ind", deleteEntity _)
      )
    // @formatter:on
  }
}
