package com.gardi.ontoEntity

import akka.NotUsed
import akka.actor.ActorSystem
import com.gardi.base.service.BaseService
import com.gardi.jena.fuseki.FusekiService
import com.gardi.shared.OntoParams
import com.lightbend.lagom.scaladsl.api.ServiceCall
import com.lightbend.lagom.scaladsl.api.transport.NotFound
import com.lightbend.lagom.scaladsl.server.ServerServiceCall
import com.typesafe.config.Config
import org.apache.jena.shared.Lock
import org.gardi.dto.OntoIndividualDto
import org.gardi.onto.interfaces.IOntContainer
import org.gardi.security.{SecConfig, SmnSecuredService}
import org.slf4j.{Logger, LoggerFactory}
import play.api.inject.ApplicationLifecycle

import scala.concurrent.duration._
import scala.concurrent.{ExecutionContext, Future, _}

class OntoEntityServiceImpl(config: Config,
                            actorSystem: ActorSystem,
                            override val securityConfig: SecConfig,
                            applicationLifecycle: ApplicationLifecycle,
                            fuseki: FusekiService,
                            ontoParams: Option[OntoParams] = None
                           )(implicit ec: ExecutionContext)
                                extends OntoEntityService with BaseService with SmnSecuredService {

  override def getOntoConfigPath: Option[OntoParams] = ontoParams

  final val log: Logger = LoggerFactory.getLogger(classOf[OntoEntityService])

  override def serviceId(): Int = 2

  val future = initService(config, actorSystem, applicationLifecycle, fuseki)(ec)
  Await.ready(future, 10 seconds)


  override def getEntity(uid: String, threadId: String, indId: String): ServiceCall[NotUsed, OntoIndividualDto] =
    ServerServiceCall { _: NotUsed =>
      val ontoName = threadsManager.map(man => man.getOntoNameFromId(uid, threadId)).get
      ontoManager.map(manager => {
        manager.action(uid, ontoName) { ontCntr =>
          ontCntr.transact(Lock.READ) { _ =>
            import org.gardi.utils._
            getIndividualByShortName(indId, ontCntr) match {
              case Some(iri) => {
                Future.successful(getOntoIndividual(iri, ontCntr))
              }
              case None => {
                Future.failed(NotFound(s"Individual ${indId} not found"))
              }
            }
          }
        }
      }).get
    }

  override def updateEntity(uid: String, threadId: String, indId: String): ServiceCall[OntoIndividualDto, OntoIndividualDto] =
    ServerServiceCall { entity: OntoIndividualDto =>
      val ontoName = threadsManager.map(man => man.getOntoNameFromId(uid, threadId)).get
      ontoManager.map(manager => {
        manager.action(uid, ontoName) { ontCntr: IOntContainer =>
          import org.gardi.utils._
          getIndividualByShortName(indId, ontCntr) match {
            case Some(iri) => {
              Future.successful(replaceEntity(ontCntr, iri, entity))
            }
            case None => Future.failed(NotFound(s"${indId} was not fond"))
          }
        }
      }).get
    }

  override def createEntity(uid: String, threadId: String): ServiceCall[OntoIndividualDto, OntoIndividualDto] =
    ServerServiceCall { entity: OntoIndividualDto =>
      val ontoName = threadsManager.map(man => man.getOntoNameFromId(uid, threadId)).get
      ontoManager.map(manager => {
        manager.action(uid, ontoName) { ontCntr: IOntContainer =>
          val newEntity = org.gardi.utils.createEntity(ontCntr, entity)
          Future.successful(newEntity)
        }
      }).get

    }

  override def deleteEntity(uid: String, threadId: String, indId: String): ServiceCall[OntoIndividualDto, Boolean] =
    ServerServiceCall { entity: OntoIndividualDto =>
      val ontoName = threadsManager.map(man => man.getOntoNameFromId(uid, threadId)).get
      ontoManager.map(manager => {
        manager.action(uid, ontoName) { ontCntr: IOntContainer =>
          import org.gardi.utils._
          getIndividualByShortName(indId, ontCntr) match {
            case Some(iri) => {
              val result = org.gardi.utils.deleteEntity(ontCntr, entity)
              Future.successful(result)
            }
            case None => Future.failed(NotFound(s"${indId} was not fond"))
          }
        }
      }).get
    }
}
