#!/bin/bash

microk8s.kubectl delete -n semantic-noter \
	deploy/jena-fuseki \
	deploy/user-service \
	deploy/onto-entity-service \
	deploy/thread-service 
