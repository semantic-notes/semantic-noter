package com.gardi.users

import akka.NotUsed
import com.lightbend.lagom.scaladsl.api.deser.DefaultExceptionSerializer
import com.lightbend.lagom.scaladsl.api.transport.Method
import com.lightbend.lagom.scaladsl.api.{Service, ServiceCall}
import org.gardi.users._
import play.api.{Environment, Mode}

trait UserService extends Service {

  def create(): ServiceCall[User, UserResponse]
  def login(): ServiceCall[UserLogin, UserAuth]
  def refreshToken(): ServiceCall[NotUsed, UserAuth]
  def userData(uuid: String): ServiceCall[NotUsed, UserResponse]

  def loginWithGoogle(): ServiceCall[NotUsed, UserAuth]

  def loginForTelegram(): ServiceCall[User, UserAuth]

  def loginByToken(): ServiceCall[NotUsed, UserAuth]
  def getLoginToken(uuid: String, from: String): ServiceCall[User, UserLoginToken]

  def sparkTest(): ServiceCall[NotUsed, NotUsed]

  override final def descriptor = {
    import Service._
    // @formatter:off
    named("user-service")
      .withCalls(
        restCall(Method.GET, "/api/spark2", sparkTest _),
        restCall(Method.POST, "/api/login", login _),
        restCall(Method.POST, "/api/refresh", refreshToken _),
        restCall(Method.GET, "/api/login-with-google", loginWithGoogle _),
        restCall(Method.POST, "/api/users", create _),
        restCall(Method.GET, "/api/users/:uuid/data", userData _),
        restCall(Method.POST, "/api/login-for-telegram", loginForTelegram _),
        restCall(Method.POST, "/api/get-login-token/:uuid?from", getLoginToken _),
        restCall(Method.POST, "/api/login-by-token", loginByToken _)
      )
      .withAutoAcl(true)
      .withExceptionSerializer(new DefaultExceptionSerializer(Environment.simple(mode = Mode.Dev)))
    // @formatter:on
  }
}
