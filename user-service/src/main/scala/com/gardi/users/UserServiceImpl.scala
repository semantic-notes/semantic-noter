package com.gardi.users

import akka.NotUsed
import akka.actor.ActorSystem
import com.gardi.base.service.BaseService
import com.gardi.jena.fuseki.FusekiService
import com.gardi.shared.OntoParams
import com.lightbend.lagom.scaladsl.api.ServiceCall
import com.lightbend.lagom.scaladsl.api.transport._
import com.lightbend.lagom.scaladsl.server.ServerServiceCall
import com.typesafe.config.Config
import org.apache.commons.codec.binary.Base64
import org.gardi.security.{SecConfig, SmnSecuredService, UserPathAuth}
import org.gardi.users.UserOntoRel.UserOntoRel
import org.gardi.users._
import org.joda.time.DateTime
import org.pac4j.core.authorization.authorizer.IsAnonymousAuthorizer.isAnonymous
import org.pac4j.core.authorization.authorizer.IsAuthenticatedAuthorizer.isAuthenticated
import org.pac4j.core.context.HttpConstants
import org.pac4j.core.profile.CommonProfile
import org.pac4j.oauth.profile.google2.Google2Profile
import org.slf4j.{Logger, LoggerFactory}
import play.api.inject.ApplicationLifecycle
import play.api.libs.json.Json

import scala.concurrent.duration._
import scala.concurrent.{Await, ExecutionContext, Future}

class UserServiceImpl(config: Config,
                      actorSystem: ActorSystem,
                      override val securityConfig: SecConfig,
                      applicationLifecycle: ApplicationLifecycle,
                      fuseki: FusekiService,
                      ontoParams: Option[OntoParams] = None
                     )(implicit ec: ExecutionContext) extends UserService with BaseService with SmnSecuredService {

  override def getOntoConfigPath: Option[OntoParams] = ontoParams

  val log: Logger = LoggerFactory.getLogger(classOf[UserService])
  private val tokenAuth = org.gardi.security.initializeJwtAuthenticator()

  override def serviceId(): Int = 0

  val future = initService(config, actorSystem, applicationLifecycle, fuseki)(ec)
  Await.ready(future, 10 seconds)


  willStop.subscribe((_) => {
    log.info("Stopped")
  })

  def checkOtp[Request, Response]
  (provider: String)
  (authenticatedServiceCall: () => ServerServiceCall[Request, Response]): ServerServiceCall[Request, Response] = {
    ServerServiceCall.compose { requestHeader =>
      requestHeader.getHeader("totp") match {
        case Some(totp) => {
          if (checkTOTP(provider, totp)) {
            authenticatedServiceCall()
          } else {
            throw Forbidden("Authorization failed")
          }
        }
        case None => throw Forbidden("Authorization failed")
      }
    }
  }

  override def create(): ServiceCall[User, UserResponse] = {
    authorize(
      isAnonymous[CommonProfile](),
      (_: CommonProfile) => ServerServiceCall { user: User =>
        log.debug("Create user", user)
        usersManager
          .map(man => man.createUser(user))
          .map(res => Future.successful(res))
          .get
      })
  }

  override def userData(uuid: String): ServiceCall[NotUsed, UserResponse] = {
    authorize(UserPathAuth.isUserPathFor(uuid), (profile: CommonProfile) => ServerServiceCall { _: NotUsed =>
      usersManager
        .map(man => man.getUserBy(UserOntoRel.UUID, profile.getId))
        .map(res => res match {
          case Some(userDto) => {
            if (userDto.id == uuid) {
              Future.successful(UserInfo.userInfoToUserResponse(userDto))
            } else {
              Future.failed(Forbidden("Access denied"))
            }
          }
          case None => Future.failed(Forbidden("Access denied"))
        }).get
    })
  }

  private def genTokenPair(userInfo: UserInfo): (Seq[String], Seq[String]) = {
    val accessToken = org.gardi.security.genToken(usersManager.map(man => man.userToCommonProfile(userInfo)).get)
    val refreshToken = org.gardi.security.genRefreshToken(usersManager.map(man => man.userToCommonProfile(userInfo)).get)
    (accessToken, refreshToken)
  }

  private def genLoginToken(uuid: String, from: String): Future[UserLoginToken] = {
    usersManager.map(man => man.getUserBy(UserOntoRel.UUID, uuid)).map(res => res match {
      case Some(userInfo) => {
        val profile = usersManager.map(man => man.userToCommonProfile(userInfo)).get
        val expirationTimeInMs = usersManager.map(man => man.ontoConfig.getLong("security.loginTknExpTimeMs")).get
        val loginCode = UserLoginCode(
          profile.getId,
          from,
          genTOTP("user-service", time = System.currentTimeMillis / expirationTimeInMs),
          DateTime.now().plus(expirationTimeInMs).getMillis)

        val jsonCode = UserLoginCode.format.writes(loginCode).toString()
        val enc = org.gardi.security.crypto.loginCodeCryptoEng.encrypt(jsonCode)
        Future.successful(UserLoginToken(Base64.encodeBase64String(enc.getBytes("utf-8"))))
      }
      case None => Future.failed(Forbidden("Not user"))
    }).get
  }

  def genUserAuthBy(rel: UserOntoRel, value: String): Option[UserAuth] = {
    usersManager.flatMap(man => man.getUserBy(rel, value)).map(userInfo => {
      val tokenPair = genTokenPair(userInfo)
      UserAuth(
        userInfo.id,
        org.gardi.security.tokenExpTime,
        tokenPair._1.mkString(""),
        Option(tokenPair._2.mkString("")))
    })
  }

  override def refreshToken(): ServiceCall[NotUsed, UserAuth] = ServerServiceCall { (requestHeader, _) =>
    def clearToken(token: String) = token
      .replace(HttpConstants.BEARER_HEADER_PREFIX, "")
      .replace(" ", "")

    val refreshToken = requestHeader
      .getHeader(HttpConstants.AUTHORIZATION_HEADER)
      .map(clearToken)

    refreshToken
      .flatMap(token => Option(tokenAuth.validateToken(token)))
      .filter(el => el.getAttribute("type") == "refresh" )
      .flatMap(profile => genUserAuthBy(UserOntoRel.UUID, profile.getId)) match {
      case Some(newAuth) => {
        Future.successful(ResponseHeader.Ok, newAuth)
      }
      case None => {
        Future.failed(Forbidden("Token isn't valid"))
      }
    }
  }

  override def login(): ServiceCall[UserLogin, UserAuth] = {
    authorize(
      isAnonymous[CommonProfile](),
      (_: CommonProfile) => ServerServiceCall { user: UserLogin =>
        usersManager
          .map(man => man.checkPassword(user.email, user.password))
          .map(res => {
            if (res) {
              genUserAuthBy(UserOntoRel.Email, user.email) match {
                case Some(auth) => Future.successful(auth)
                case None => Future.failed(NotFound("User doesn't exist"))
              }
            } else {
              Future.failed(new RuntimeException("password is wrong"))
            }
          }).get
      })
  }

  override def loginWithGoogle(): ServiceCall[NotUsed, UserAuth] = {
    authorize(
      "Google",
      isAuthenticated[CommonProfile](),
      (profile: CommonProfile) => ServerServiceCall { _: NotUsed =>
        val google2Profile = profile.asInstanceOf[Google2Profile]
        log.debug(s" id[${google2Profile.getId}] : ${google2Profile.getDisplayName}: ${google2Profile.getEmail}1")

        var result: Option[UserAuth] = None
        do {
          result = genUserAuthBy(UserOntoRel.GoogleId, google2Profile.getId) match {
            case Some(auth) => Option(auth)
            case None => {
              val newUser = User(
                google2Profile.getDisplayName,
                email = Some(google2Profile.getEmail),
                id = Some(google2Profile.getId)
              )
              log.info(s"Register User from the google- ${google2Profile.getDisplayName})")
              usersManager.foreach(man => {
                man.createUser(newUser, Some((UserOntoRel.GoogleId, google2Profile.getId)))
              })
              None
            }
          }
        } while (result.isEmpty)

        Future.successful(result.get)
      }
    )
  }

  override def loginForTelegram(): ServiceCall[User, UserAuth] = authorize(isAnonymous[CommonProfile](),
    (_: CommonProfile) =>  checkOtp[User, UserAuth]("telegram") {
    () => ServerServiceCall { user: User =>
      var result: Option[UserAuth] = None
      do {
        user.id match {
          case Some(id) => {
            result = genUserAuthBy(UserOntoRel.TelegramId, id) match {
              case Some(auth) => Option(auth)
              case None => {
                usersManager.foreach(man => {
                  man.createUser(user, Some((UserOntoRel.TelegramId, id)))
                })
                log.info(s"Register User from the telegram- ${user.username})")
                None
              }
            }
          }
        }
      } while (result.isEmpty)

      Future.successful(result.get)
    }
  })

  override def getLoginToken(uuid: String, from: String): ServiceCall[User, UserLoginToken] =
    authorize(UserPathAuth.isUserPathFor(uuid),
      (_: CommonProfile) => checkOtp[User, UserLoginToken](from) {
        () => ServerServiceCall { _ =>
          genLoginToken(uuid, from)
        }
    })

  override def loginByToken(): ServiceCall[NotUsed, UserAuth] = ServerServiceCall { (requestHeader, _) =>
    def clearToken(token: String) = token
      .replace(HttpConstants.BEARER_HEADER_PREFIX, "")
      .replace(" ", "")

    val loginToken = requestHeader
      .getHeader(HttpConstants.AUTHORIZATION_HEADER)
      .map(clearToken)

    loginToken
      .flatMap(token => {
        val encStr = new String(Base64.decodeBase64(token), "utf-8")
        val decStr = org.gardi.security.crypto.loginCodeCryptoEng.decrypt(encStr)
        val jsonCode = UserLoginCode.format.reads(Json.parse(decStr))
        jsonCode.asOpt
      })
      .filter(code => {
        val expirationTimeInMs = usersManager.map(man => man.ontoConfig.getLong("security.loginTknExpTimeMs")).get
        code.validTo >= System.currentTimeMillis && checkTOTP("user-service",
          code.totp, time = System.currentTimeMillis / expirationTimeInMs)
      })
      .flatMap(profile => genUserAuthBy(UserOntoRel.UUID, profile.id))
        match {
          case Some(newAuth) => {
            Future.successful(ResponseHeader.Ok, newAuth)
          }
          case None => {
            Future.failed(Forbidden("Token isn't valid"))
          }
        }
  }

  override def sparkTest(): ServiceCall[NotUsed, NotUsed] = ServiceCall { _: NotUsed =>
    val sc = sparkSession match {
      case Some(ss) => {
        val sc = ss.sparkContext
        val count = sc.parallelize(1 to 1000).filter { _ =>
          val x = math.random
          val y = math.random
          x*x + y*y < 1
        }.count()
        println(s"!!!!!!22222 ${count}")
      }
      case None => {
        println("!!! NONE SC")
      }
    }

    Future.successful(NotUsed)
  }

}