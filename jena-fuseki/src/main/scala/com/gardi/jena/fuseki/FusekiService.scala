package com.gardi.jena.fuseki

import akka.NotUsed
import com.lightbend.lagom.scaladsl.api.broker.Topic
import com.lightbend.lagom.scaladsl.api.transport.Method
import com.lightbend.lagom.scaladsl.api.{Service, ServiceCall}
import play.api.libs.json.{Format, Json}

trait FusekiService extends Service {

  val serviceName = "jena-fuseki"

  def getSettings(): ServiceCall[NotUsed, TripleStoreInfo]

  override final def descriptor = {
    import Service._
    // @formatter:off
    named(serviceName)
      .withCalls(
        restCall(Method.POST, "/api/internal/storage/settings", getSettings _)
      )
      .withAutoAcl(true)
    // @formatter:on
  }
}

case class TripleStoreInfo(val url: String)

object TripleStoreInfo {
  implicit val format: Format[TripleStoreInfo] = Json.format
}
