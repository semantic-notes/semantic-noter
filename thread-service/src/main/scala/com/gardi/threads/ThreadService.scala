package com.gardi.threads

import akka.NotUsed
import com.lightbend.lagom.scaladsl.api.transport.Method
import com.lightbend.lagom.scaladsl.api.{Service, ServiceCall}
import org.gardi.dto.{CreateNoteDto, OntoIndividualDto, SearchParams, SearchResult}
import org.gardi.users.{AddThread, UserThread}

trait ThreadService extends Service {

    def addThreadForUser(uuid: String): ServiceCall[AddThread, String]
    def listUserThreads(uuid: String): ServiceCall[NotUsed, Seq[UserThread]]
    def getUserThread(uuid: String, threadId: String, detailed: Option[Boolean] = None): ServiceCall[NotUsed, UserThread]

    def addNote(uuid: String, threadId: String): ServiceCall[CreateNoteDto, String]
    def listNotes(uuid: String, threadId: String): ServiceCall[NotUsed, Set[OntoIndividualDto]]
    def listUnhandledNotes(uuid: String, threadId: String): ServiceCall[NotUsed, Set[OntoIndividualDto]]

    def getIndividuals(uuid: String, threadId: String): ServiceCall[NotUsed, Set[OntoIndividualDto]]
    def describeEntities(uuid: String, threadId: String): ServiceCall[Set[String], Set[OntoIndividualDto]]

    def getEntity(uuid: String,
                  threadId: String,
                  indId: String): ServiceCall[NotUsed, OntoIndividualDto]

    def createEntity(uuid: String, threadId: String): ServiceCall[OntoIndividualDto, OntoIndividualDto]
    def updateEntity(uuid: String,
                     threadId: String,
                     indId: String): ServiceCall[OntoIndividualDto, OntoIndividualDto]


    def clear(uuid: String,
              threadId: String): ServiceCall[NotUsed, NotUsed]

    def sparkTest(): ServiceCall[NotUsed, NotUsed]

    override final def descriptor = {
    import Service._    // @formatter:off
    named("thread-service")
      .withCalls(
          restCall(Method.GET, "/api/spark", sparkTest _),
        restCall(Method.POST, "/api/users/:uuid/threads", addThreadForUser _),
        restCall(Method.GET, "/api/users/:uuid/threads", listUserThreads _),
        restCall(Method.GET, "/api/users/:uuid/threads/:threadId?detailed", getUserThread _),
        restCall(Method.POST, "/api/users/:uuid/threads/:threadId/notes", addNote _),
        restCall(Method.GET, "/api/users/:uuid/threads/:threadId/notes", listNotes _),
        restCall(Method.GET, "/api/users/:uuid/threads/:threadId/notes/unhandled", listUnhandledNotes _),
          restCall(Method.POST, "/api/users/:uuid/threads/:threadId/entities/view", describeEntities _),
        restCall(Method.GET, "/api/users/:uuid/threads/:threadId/ind", getIndividuals _),
        restCall(Method.POST, "/api/users/:uuid/threads/:threadId/ind", createEntity _),
          restCall(Method.GET, "/api/users/:uuid/threads/:threadId/ind/:indId", getEntity _),
          restCall(Method.PUT, "/api/users/:uuid/threads/:threadId/ind/:indId", updateEntity _),
        restCall(Method.PUT, "/api/users/:uuid/threads/:threadId/clear", clear _)
      )
      .withAutoAcl(true)
//      .withExceptionSerializer(new DefaultExceptionSerializer(Environment.simple(mode = Mode.Prod)))
    // @formatter:on
  }
}
