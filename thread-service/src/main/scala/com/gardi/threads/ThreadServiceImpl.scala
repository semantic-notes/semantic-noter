package com.gardi.threads

import akka.NotUsed
import akka.actor.ActorSystem
import com.gardi.base.service.BaseService
import com.gardi.jena.fuseki.FusekiService
import com.gardi.ontoEntity.OntoEntityService
import com.gardi.shared.OntoParams
import com.lightbend.lagom.scaladsl.api.ServiceCall
import com.lightbend.lagom.scaladsl.server.ServerServiceCall
import com.typesafe.config.Config
import org.apache.jena.shared.Lock
import org.gardi.dto.{CreateNoteDto, EntityQueryParams, OntoIRI, OntoIndividualDto, SearchParams, SearchResult}
import org.gardi.onto
import org.gardi.onto.{TResultVariables, query}
import org.gardi.security.{SecConfig, SmnSecuredService, UserPathAuth}
import org.gardi.users.{AddThread, UserThread}
import org.pac4j.core.profile.CommonProfile
import org.slf4j.{Logger, LoggerFactory}
import play.api.inject.ApplicationLifecycle

import scala.concurrent.{ExecutionContext, Future}
import scala.util.{Failure, Success}
import com.lightbend.lagom.scaladsl.api.transport.{ExceptionMessage, TransportErrorCode, TransportException}
import org.gardi.onto.interfaces.IOntContainer
import org.gardi.utils.sparql.BaseOntoIRIs

import scala.collection.JavaConverters._

class ThreadServiceImpl(config: Config,
                        actorSystem: ActorSystem,
                        override val securityConfig: SecConfig,
                        applicationLifecycle: ApplicationLifecycle,
                        fuseki: FusekiService,
                        ontoEntity: OntoEntityService,
                        ontoParams: Option[OntoParams] = None
                       )(implicit ec: ExecutionContext)
                          extends ThreadService with BaseService with SmnSecuredService {

  override def getOntoConfigPath: Option[OntoParams] = ontoParams

  final val log: Logger = LoggerFactory.getLogger(classOf[ThreadService])

  override def serviceId(): Int = 1

  initService(config, actorSystem, applicationLifecycle, fuseki)(ec)
    .onComplete((result) => {
      result match {
        case Success(settings) => {
          log.info("ThreadService with", settings)
          log.info("Initialized")
        }
        case Failure(exc) => {
          exc.printStackTrace()
        }
      }
    })

  willStop.subscribe((_) => {
    println(s"Stop ${classOf[ThreadService]}")
    ontoManager.foreach(manager => manager.dispose())
  })

  override def listUserThreads(uuid: String): ServiceCall[NotUsed, Seq[UserThread]] = {
    authorize(UserPathAuth.isUserPathFor(uuid), (profile: CommonProfile) => ServerServiceCall { _: NotUsed =>
      threadsManager.map(man => {
        val threads = man.getThreadsForUser(profile)
        Future.successful(threads.keys.map(key => man.entityToThread(key, threads(key))).toSeq)
      }).get
    })
  }

  override def addThreadForUser(uuid: String): ServiceCall[AddThread, String] = {
    authorize(UserPathAuth.isUserPathFor(uuid), (profile: CommonProfile) => ServerServiceCall { evt: AddThread =>
      val ontoName = usersManager.map(man => man.getMd5(s"${profile.getId}:${evt.name}")).get
      ontoLoader.map(loader => {
        if (!loader.hasLoadedOnto(ontoName)) {
          loader.createOntoModelByName(ontoName)
          val graphName = loader.getGraphName(ontoName)
          val uuid = threadsManager.map(man => {
            man.addThreadForUser(profile, evt, graphName)
          })
          log.info(s"User - ${profile.getDisplayName} added thread ${evt.name}: (${graphName})")
          uuid match {
            case Some(value) => Future.successful(value)
            case None => Future.failed(TransportException
              .fromCodeAndMessage(
                TransportErrorCode.BadRequest,
                new ExceptionMessage("Can't create thread", s"name: ${evt.name}"))
            )
          }
        } else {
          Future.failed(TransportException
            .fromCodeAndMessage(
              TransportErrorCode.fromHttp(409),
              new ExceptionMessage(s"Thread already exist", s"name: ${evt.name}"))
          )
        }
      }).get
    })
  }

  override def getUserThread(uuid: String, threadId: String, detailed: Option[Boolean] = None): ServiceCall[NotUsed, UserThread] = {
    authorize(UserPathAuth.isUserPathFor(uuid), (profile: CommonProfile) => ServerServiceCall { _: NotUsed =>
      threadsManager.map(man => {
        val thread = man.getThreadForUser(profile, threadId)
        val ontoName = man.getOntoNameFromThread(thread)
        try {
          ontoManager.map(manager => {
            manager.action(uuid, ontoName) { ontCntr =>
              ontCntr.transact(Lock.READ) { _ =>
                val result = man.entityToThread(man.asResource(threadId), thread)
                import org.gardi.utils._

                result.prefixMapping = ontCntr.ontoApi.prefixMapping().getNsPrefixMap.asScala.toMap
                result.annotations = getAnnotations(ontCntr)
                result.classes = Some(getSubClassFor(getOWLClass(BaseOntoIRIs.SystemClasses.SemnThing), ontCntr))
                result.relations = Some(getSubRelationsFor(getOWLRelation("baseOntology:common"), ontCntr))
                result.dataProperties = Some(getSubDataPropFor(getOWLDataProp("baseOntology:concept-prop"), ontCntr))
                result.individuals = org.gardi.utils.getIndividuals(ontCntr, detailed.getOrElse(false))

                Future.successful(result)
              }
            }
          }).get

        } catch {
          case err: Throwable => {
            println(err.getMessage)
            err.printStackTrace()
            Future.failed(err)
          }
        }
      }).get
    })
  }

  override def getIndividuals(uuid: String, threadId: String): ServiceCall[NotUsed, Set[OntoIndividualDto]] = {
    authorize(UserPathAuth.isUserPathFor(uuid), (profile: CommonProfile) => ServerServiceCall { _: NotUsed =>
      threadsManager.map(man => {
        val ontoName = man.getOntoNameFromId(profile, threadId)
        val result = ontoManager.map(manager => {
          manager.action(uuid, ontoName) { ontCntr: IOntContainer =>
            val query = """SELECT ?x { ?x rdf:type owl:NamedIndividual . }"""
            ontCntr.queryReasoner(query) match {
              case TResultVariables(results) => {
                results.map(el => {
                  import org.gardi.utils._
                  val iri = el("x").result.asResource().getURI
                  getOntoIndividual(getIndividualFrom(iri), ontCntr)
                }).toSet
              }
            }
          }
        }).get
        Future.successful(result)
      }).get
    })
  }

  override def addNote(uuid: String, threadId: String): ServiceCall[CreateNoteDto, String] = {
    authorize(UserPathAuth.isUserPathFor(uuid), (profile: CommonProfile) => ServerServiceCall {
      newNote: CreateNoteDto =>
        threadsManager.map(man => {
          val ontoName = man.getOntoNameFromId(profile, threadId)
          ontoManager.map(manager => {
            manager.action(uuid, ontoName) { ontCntr: IOntContainer =>
              val nodeUri = genId()
              ontCntr.updateQuery(query.addNote(nodeUri, newNote))
              Future.successful(nodeUri.toString)
            }
          }).get
        }).get
    })
  }

  override def listNotes(uuid: String, threadId: String): ServiceCall[NotUsed, Set[OntoIndividualDto]] = {
    authorize(UserPathAuth.isUserPathFor(uuid), (profile: CommonProfile) => ServerServiceCall { _: NotUsed =>
      threadsManager.map(man => {
        val ontoName = man.getOntoNameFromId(profile, threadId)
        val result = ontoManager.map(manager => {
          manager.action(uuid, ontoName) { ontCntr: IOntContainer =>
            val query = """SELECT ?x { ?x a baseOntology:InfoItems . }"""
            ontCntr.queryReasoner(query) match {
              case TResultVariables(results) => {
                results.map(el => {
                  import org.gardi.utils._
                  val iri = el("x").result.asResource().getURI
                  getOntoIndividual(getIndividualFrom(iri), ontCntr)
                }).toSet
              }
            }
          }
        }).get
        Future.successful(result)
      }).get
    })
  }

  override def listUnhandledNotes(uuid: String, threadId: String): ServiceCall[NotUsed, Set[OntoIndividualDto]] = {
    authorize(UserPathAuth.isUserPathFor(uuid), (profile: CommonProfile) => ServerServiceCall { _: NotUsed =>
      threadsManager.map(man => {
        val ontoName = man.getOntoNameFromId(profile, threadId)
        val result = ontoManager.map(manager => {
          manager.action(uuid, ontoName) { ontCntr: IOntContainer =>
            val query = """SELECT ?x { ?x a baseOntology:Unhandled . }"""
            ontCntr.queryReasoner(query) match {
              case TResultVariables(results) => {
                results.map(el => {
                  import org.gardi.utils._
                  val iri = el("x").result.asResource().getURI
                  getOntoIndividual(getIndividualFrom(iri), ontCntr)
                }).toSet
              }
            }
          }
        }).get
        Future.successful(result)
      }).get
    })
  }

  override def getEntity(uuid: String, threadId: String, indId: String): ServiceCall[NotUsed, OntoIndividualDto] = {
    authorize(
      UserPathAuth.isUserPathFor(uuid),
      (profile: CommonProfile) => ServerServiceCall { _: NotUsed =>
        ontoEntity.getEntity(profile.getId, threadId, indId).invoke()
      })
  }

  override def createEntity(uuid: String, threadId: String): ServiceCall[OntoIndividualDto, OntoIndividualDto] = {
    authorize(
      UserPathAuth.isUserPathFor(uuid),
      (profile: CommonProfile) => ServerServiceCall { entity: OntoIndividualDto =>
        ontoEntity.createEntity(profile.getId, threadId).invoke(entity)
      })
  }

  override def updateEntity(uuid: String,
                            threadId: String,
                            indId: String): ServiceCall[OntoIndividualDto, OntoIndividualDto] = {
    authorize(
      UserPathAuth.isUserPathFor(uuid),
      (profile: CommonProfile) => ServerServiceCall { entity: OntoIndividualDto =>
        ontoEntity.updateEntity(profile.getId, threadId, indId).invoke(entity)
    })
  }

  // TODO!: Retink about this method
  override def clear(uuid: String, threadId: String): ServiceCall[NotUsed, NotUsed] = {
    authorize(UserPathAuth.isUserPathFor(uuid), (profile: CommonProfile) => ServerServiceCall { _: NotUsed =>
      threadsManager.map(man => {
        val ontoName = man.getOntoNameFromId(profile, threadId)
        ontoManager.map(manager => {
          manager.action(uuid, ontoName) { ontCntr: IOntContainer =>
            //        val query = s"""
            //            DELETE {
            //              ?x ?y ?z
            //            }
            //            WHERE  {
            //              ?x ?y ?z .
            //              ?x rdf:type owl:NamedIndividual .
            //            }"""
            //        ontCntr.updateQuery(query)
            Future.successful(NotUsed.getInstance())
          }
        }).get
      }).get

    })
  }

  def describeEntities(uuid: String, threadId: String): ServiceCall[Set[String], Set[OntoIndividualDto]] = {
    authorize(
      UserPathAuth.isUserPathFor(uuid),
      (profile: CommonProfile) => ServerServiceCall { iris: Set[String] =>
        val ontoName = threadsManager.map(man => man.getOntoNameFromId(uuid, threadId)).get
        ontoManager.map(manager => {
          manager.action(uuid, ontoName) { ontCntr: IOntContainer =>
            import org.gardi.utils._
            val results = iris.map(iri => getOntoIndividual(getIndividualFrom(iri), ontCntr))
            Future.successful(results)
          }
        }).get
      })
  }

  override def sparkTest(): ServiceCall[NotUsed, NotUsed] = ServiceCall { _: NotUsed =>
    println("sparkTest !!!!")
    val sc = sparkSession match {
      case Some(ss) => {
        try {
          val sc = ss.sparkContext

          {
            import net.sansa_stack.owl.spark.owl._
            import org.apache.jena.riot.Lang
            val path = onto.baseOntoFsPath;
            val lang: Lang = Lang.NTRIPLES
            //val triples =  ss.read.rdf(lang)(path)
            val rdd = ss.owl(Syntax.FUNCTIONAL)(path)
            println(s"!!!! ${rdd.count()}")

          }


          val count = sc.parallelize(1 to 5000).map { _ =>
            val x = math.random
            val y = math.random
            x+y
          }.sum()
          println(s"!!!!!! ${count}")
        } catch {
          case err: Throwable => {
            println(err, err.printStackTrace())
          }
        }

      }
      case None => {
        println("!!! NONE SC")
      }
    }

    Future.successful(NotUsed)
  }
}
