package com.gardi.threads

import com.gardi.jena.fuseki.FusekiService
import com.gardi.ontoEntity.OntoEntityService
import com.gardi.shared.OntoParams
import com.lightbend.lagom.internal.client.CircuitBreakerMetricsProviderImpl
import com.lightbend.lagom.scaladsl.akka.discovery.AkkaDiscoveryComponents
import com.lightbend.lagom.scaladsl.broker.kafka.LagomKafkaComponents
import com.lightbend.lagom.scaladsl.cluster.ClusterComponents
import com.lightbend.lagom.scaladsl.devmode.LagomDevModeComponents
import com.lightbend.lagom.scaladsl.persistence.cassandra.CassandraPersistenceComponents
import com.lightbend.lagom.scaladsl.playjson.{JsonSerializer, JsonSerializerRegistry}
import com.lightbend.lagom.scaladsl.server.{LagomApplication, LagomApplicationContext, LagomApplicationLoader}
import com.softwaremill.macwire._
import org.gardi.security.SecurityModel
import play.api.libs.ws.ahc.AhcWSComponents

import scala.collection.immutable

class ThreadServiceLoader extends LagomApplicationLoader {
  override def load(context: LagomApplicationContext): LagomApplication =
    new ThreadServiceApplication(context) with AkkaDiscoveryComponents {
      override lazy val circuitBreakerMetricsProvider = new CircuitBreakerMetricsProviderImpl(actorSystem)
    }

  override def loadDevMode(context: LagomApplicationContext): LagomApplication =
    new ThreadServiceApplication(context) with LagomDevModeComponents

  override def describeService = Some(readDescriptor[ThreadService])
}

abstract class ThreadServiceApplication(context: LagomApplicationContext)
  extends LagomApplication(context)
    with LagomKafkaComponents
    with CassandraPersistenceComponents
    with ClusterComponents
    with AhcWSComponents
    with SecurityModel {

  // ClusterComponents requires a JsonSerializerRegistry but we're not using any
  // type that requires a custom Serializer because we're just sending String
  // back and forth.
  override def jsonSerializerRegistry: JsonSerializerRegistry =
    new JsonSerializerRegistry {
      override def serializers = immutable.Seq.empty[JsonSerializer[_]]
    }

  // Bind the service that this server provides
  val ontoParams: Option[OntoParams] = Some(OntoParams("configurations/onto.conf"))
  lazy val fuseki = serviceClient.implement[FusekiService]
  lazy val ontoEntity = serviceClient.implement[OntoEntityService]
  override lazy val lagomServer = serverFor[ThreadService](wire[ThreadServiceImpl])
}