#!/bin/bash

registryUrl="localhost:32000"
kubectlCmd="kubectl"

wait_service () {
  eval "$kubectlCmd -n semantic-noter get pod -l app=$1 | grep Running > /dev/null 2>&1"
  while [ $? -ne 0 ]
  do
    echo "waiting for $1 pod to be ready..."
    sleep 2
    eval "$kubectlCmd -n semantic-noter get pod -l app=$1 | grep Running > /dev/null 2>&1"
  done
}

buildAll () {
   sbt -Ddocker.registry=$registryUrl \
        jenaFusekiImpl/docker:publish \
        user-service/docker:publish \
        onto-entity-service/docker:publish \
        thread-service/docker:publish
}

if [[ -n "$1" && $1 = "b" ]]; then
  buildAll
fi

eval "$kubectlCmd apply -f deployment/jena-fuseki.yaml"
wait_service jena-fuseki
eval "$kubectlCmd apply -f deployment/user-service.yaml"
eval "$kubectlCmd apply -f deployment/onto-entity-service.yaml"
wait_service onto-entity-service
eval "$kubectlCmd apply -f deployment/thread-service.yaml"

eval "$kubectlCmd apply -f deployment/ingress.yaml"

