package com.gardi.base.service

import java.io.File

import akka.actor.ActorSystem
import akka.stream.ActorMaterializer
import com.gardi.jena.fuseki.FusekiService
import com.gardi.shared.{OntoConfig, OntoParams}
import com.lightbend.lagom.internal.broker.kafka.KafkaConfig
import com.typesafe.config.{Config, ConfigFactory}
import org.apache.spark.sql.SparkSession
import org.gardi.LagomData
import org.gardi.onto.impl.{FusekiOntoLoader, FusekiParams, OntoApiProvider}
import org.gardi.onto.interfaces.IOntoLoader
import org.gardi.onto.OntoManager
import org.gardi.users.impl.{FusekiConnection, FusekiThreadsManager, FusekiUsersManager}
import org.gardi.users.interfaces.{IThreadsManager, IUsersManager}
import org.slf4j.Logger
import play.api.inject.ApplicationLifecycle
import rx.lang.scala.subjects.PublishSubject

import scala.concurrent.{ExecutionContext, Future}

trait BaseService {

  def getOntoConfigPath: Option[OntoParams] = None
  val baseOntoCfg = ConfigFactory
    .parseFile(new File(getOntoConfigPath.map(_.ontoConfigPath).getOrElse("configurations/onto.conf")))
  implicit val ontoApi = new OntoApiProvider()
  var ontoLoader: Option[IOntoLoader] = None
  var ontoManager: Option[OntoManager] = None
  var fusekiConn: Option[FusekiConnection] = None
  var usersManager: Option[IUsersManager] = None
  var threadsManager: Option[IThreadsManager] = None
  val willStop = PublishSubject[Unit]()
  var sparkSession: Option[SparkSession] = None

  implicit def loader = ontoLoader.get

  def serviceId: Int
  def log: Logger

  def initSpark(): Unit = {
    val sparkUrl = "local"
    sparkSession = Some(SparkSession.builder
      .master(sparkUrl)
      //      .appName("Word Count")
      //      .config("spark.some.config.option", "some-value")
      .getOrCreate())
  }

  def initService(config: Config,
                  actorSystem: ActorSystem,
                  applicationLifecycle: ApplicationLifecycle,
                  fuseki: FusekiService,
                  initSpark: Boolean = false)(ec: ExecutionContext): Future[Unit] = {

    implicit val materializer = ActorMaterializer()(actorSystem)
    implicit val exContext = ec
    val className = this.getClass.getSimpleName

    val lagomData = LagomData(actorSystem, className, KafkaConfig(config).brokers)

    org.gardi.utils.initIdGenerator(serviceId)

    if (initSpark) { this.initSpark() }

    applicationLifecycle.addStopHook(() => {
      willStop.onNext()
      sparkSession.foreach(ss => ss.stop())
      ontoLoader.foreach(loader => loader.dispose())
      fusekiConn.foreach(conn => conn.close())
      Future.successful()
    })

    fuseki.getSettings().invoke().map(settings => {
      log.info(s"${className} starting with storage - ${settings.url}")

      try {
        val cfg = OntoConfig(baseOntoCfg).copy(fusekiUrl = settings.url)
        val loader = new FusekiOntoLoader(ontoApi, FusekiParams(cfg, Some(lagomData)))
        ontoLoader = Some(loader)
        ontoManager = Some(new OntoManager(loader, Some(lagomData)))
        fusekiConn = Some(new FusekiConnection(cfg))
        usersManager = Some(new FusekiUsersManager(fusekiConn.get))
        threadsManager = Some(new FusekiThreadsManager(fusekiConn.get))

        usersManager.foreach(_.initialize)
      } catch {
        case exc: Throwable => exc.printStackTrace()
      }

      log.info(s"${className} initialized")
      settings
    })
  }

  def genId(): String = org.gardi.utils.getNextId()
}
