package com.semn.utils

import com.gardi.filterService.{FilterService, FilterServiceImpl}
import com.gardi.jena.fuseki.FusekiService
import com.gardi.ontoEntity.{OntoEntityService, OntoEntityServiceImpl}
import com.gardi.shared.OntoParams
import com.gardi.threads.{ThreadService, ThreadServiceImpl}
import com.gardi.users.{UserService, UserServiceImpl}
import com.lightbend.lagom.internal.kafka.KafkaLocalServer
import com.lightbend.lagom.scaladsl.server.LagomServer
import com.lightbend.lagom.scaladsl.testkit.ServiceTest
import com.semn.utils
import com.softwaremill.macwire.wire
import org.gardi.dto.{EntityQueryParams, OntoIndividualDto, SearchParams, SearchResult}
import org.gardi.users.{AddThread, User, UserAuth, UserLogin, UserThread}

import scala.concurrent.Await
import scala.concurrent.duration._

class TestCluster {
  private final val kafkaPort          = 9092
  private final val kafkaZooKeeperPort = 2181
  private final val kafkaUri = s"tcp://localhost:${kafkaPort}/kafka_native"

  val ontoParams: Option[OntoParams] = Some(OntoParams("configurations/test/onto.conf"))
  private val clsLoader = getClass.getClassLoader
  private val uri = clsLoader.getResource("kafka-dev-server.properties")
  private val kafkaSrv: KafkaLocalServer = KafkaLocalServer(
    kafkaPort,//KafkaLocalServer.DefaultPort,
    kafkaZooKeeperPort,//ZooKeeperLocalServer.DefaultPort,
    uri.getFile,
    None,
    true
  )

  private val staticLocations = Map(
    "kafka_native" -> kafkaUri
  )

  kafkaSrv.start()

  private val fusekiHolder = ServiceTest.startServer(
    ServiceTest
      .defaultSetup
      .withCluster(true)
      .withCassandra(true)
  ) { ctx =>
    new TestApplication(ctx, staticLocations) {
      def getOntoParams: Option[OntoParams] = ontoParams
      override lazy val lagomServer: LagomServer = serverFor[FusekiService](wire[FusekiMemSrv])
    }
  }

  val fusekiService = fusekiHolder.serviceClient.implement[FusekiService]

  private val userServer = ServiceTest.startServer(
    ServiceTest
      .defaultSetup
      .withCluster(true)
      .withCassandra(true)
  ) { ctx =>
    new TestApplication(ctx, staticLocations) {
      val fuseki = fusekiService
      def getOntoParams: Option[OntoParams] = ontoParams
      override def lagomServer: LagomServer = serverFor[UserService](wire[UserServiceImpl])
    }
  }

  val userService = userServer.serviceClient.implement[UserService]

  private val ontoEntityServer = ServiceTest.startServer(
    ServiceTest
      .defaultSetup
      .withCluster(true)
      .withCassandra(true)
  ) { ctx =>
    new TestApplication(ctx, staticLocations) {
      val fuseki = fusekiService
      def getOntoParams: Option[OntoParams] = ontoParams
      override def lagomServer: LagomServer = serverFor[OntoEntityService](wire[OntoEntityServiceImpl])
    }
  }

  val ontoEntityService = ontoEntityServer.serviceClient.implement[OntoEntityService]

  private val threadsServer = ServiceTest.startServer(
    ServiceTest
      .defaultSetup
      .withCluster(true)
      .withCassandra(true)
  ) { ctx =>
    new TestApplication(ctx, staticLocations) {
      val fuseki = fusekiService
      val ontoEntity = ontoEntityService
      def getOntoParams: Option[OntoParams] = ontoParams
      override def lagomServer: LagomServer = serverFor[ThreadService](wire[ThreadServiceImpl])
    }
  }

  val threadsService = threadsServer.serviceClient.implement[ThreadService]

  private val filterServer = ServiceTest.startServer(
    ServiceTest
      .defaultSetup
      .withCluster(true)
      .withCassandra(true)
  ) { ctx =>
    new TestApplication(ctx, staticLocations) {
      val fuseki = fusekiService
      def getOntoParams: Option[OntoParams] = ontoParams
      override def lagomServer: LagomServer = serverFor[FilterService](wire[FilterServiceImpl])
    }
  }

  val filterService = filterServer.serviceClient.implement[FilterService]

  def stop() = {
    kafkaSrv.stop()
  }

  def waitDuration = 1 minute

  def withTimeMetric[T](fn: () => T): (T, Long) = {
    val start = System.currentTimeMillis
    val res = fn()
    val end = System.currentTimeMillis
    (res, end - start)
  }

  def prepareUser(email: String, pass: String, name: Option[String] = None): UserAuth = {
    val userCreationDto = User(name.getOrElse("email"), Some(email), Some(pass))
    Await.ready(userService.create().invoke(userCreationDto), waitDuration)
    Await.result[UserAuth](userService.login().invoke(UserLogin(email, pass)), waitDuration)
  }

  def prepareThread(auth: UserAuth, name: String, descr: String): String = {
    val fut = threadsService
      .addThreadForUser(auth.id)
      .handleRequestHeader(utils.setTokenHeader(auth.token))
      .invoke(AddThread(name, Some(descr)))
    Await.result(fut, waitDuration)
  }

  def addEntityToThread(auth: UserAuth, threadId: String)(obj: OntoIndividualDto): OntoIndividualDto = {
    val fut = threadsService
      .createEntity(auth.id, threadId)
      .handleRequestHeader(utils.setTokenHeader(auth.token))
      .invoke(obj)
    Await.result[OntoIndividualDto](fut, waitDuration)
  }

  def updateEntityInThread(auth: UserAuth, threadId: String)(obj: OntoIndividualDto): OntoIndividualDto  = {
    val fut = threadsService
      .updateEntity(auth.id, threadId, obj.iri.getLocalId)
      .handleRequestHeader(utils.setTokenHeader(auth.token))
      .invoke(obj)
    Await.result[OntoIndividualDto](fut, waitDuration)
  }

  def getUserThread(auth: UserAuth)(threadId: String)(detailed: Boolean = false): UserThread = {
    val fut = threadsService
      .getUserThread(auth.id, threadId, Some(detailed))
      .handleRequestHeader(utils.setTokenHeader(auth.token))
      .invoke()
    Await.result[UserThread](fut, waitDuration)
  }

  def searchByFields(auth: UserAuth, threadId: String)(fields: Set[String], value: String): List[SearchResult] = {
    val fut = filterService
      .searchByFields(auth.id, threadId)
      .handleRequestHeader(utils.setTokenHeader(auth.token))
      .invoke(SearchParams(fields, value))
    Await.result[List[SearchResult]](fut, waitDuration)
  }

  def describeEntities(auth: UserAuth, threadId: String)(iris: Set[String]): Set[OntoIndividualDto] = {
    val fut = threadsService
      .describeEntities(auth.id, threadId)
      .handleRequestHeader(utils.setTokenHeader(auth.token))
      .invoke(iris)
    Await.result[Set[OntoIndividualDto]](fut, waitDuration)
  }

  def getIndividuals(auth: UserAuth, threadId: String)(params: EntityQueryParams): Set[OntoIndividualDto] = {
    val fut = threadsService.getIndividuals(auth.id, threadId)
      .handleRequestHeader(utils.setTokenHeader(auth.token))
      .invoke()
    Await.result[Set[OntoIndividualDto]](fut, waitDuration)
  }

  def getEntities(auth: UserAuth, threadId: String)(params: EntityQueryParams): Set[OntoIndividualDto] = {
    val fut = filterService.getEntities(auth.id, threadId)
      .handleRequestHeader(utils.setTokenHeader(auth.token))
      .invoke(params)
    Await.result[Set[OntoIndividualDto]](fut, waitDuration)
  }
}
