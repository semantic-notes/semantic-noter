package com.semn

import com.lightbend.lagom.scaladsl.api.transport.RequestHeader
import org.gardi.dto.{OntoIRI, OntoIndividualDto, OntoValue}
import org.gardi.users.UserThread
import org.gardi.utils.sparql.BaseOntoIRIs
import play.api.libs.json.Json

package object utils {
  def setTokenHeader(token: String)(req: RequestHeader): RequestHeader =
    req.withHeader("Authorization", token)

  def hasClass(cls: String)(ind: OntoIndividualDto) = ind.classes.exists(el => el.value == cls)
  def hasRelation(prop: String)(ind: OntoIndividualDto) = ind.relations.exists(el => el.iri == OntoIRI(prop))
  def hasRelationValue(prop: String, value: String)(ind: OntoIndividualDto) =
    ind.relations.exists(el => el.iri == OntoIRI(prop) && el.value == value )
  def hasDataValue(prop: String, value: String)(ind: OntoIndividualDto) =
    ind.dataProperties.exists(el => el.iri == OntoIRI(prop) && el.value == value )

  def ontIndToString(el: OntoIndividualDto) = {
    val jsObj = Json.toJson(el)
    Json.prettyPrint(jsObj)
  }

  def threadToString(thread: UserThread): String = {
    val jsObj = Json.toJson(thread)
    Json.prettyPrint(jsObj)
  }

  def genNewConcept(name: String): OntoIndividualDto = {
    new OntoIndividualDto(OntoIRI(""))
      .copy(
        classes = List(
          OntoValue(OntoIRIs.ontoClass, OntoIRIs.Concept)
        ),
        annotations = List(
          OntoValue(OntoIRIs.label, name)
        )
      )
  }

  def genNewInfoItem(title: String, text: String): OntoIndividualDto = {
    new OntoIndividualDto(OntoIRI(""))
      .copy(
        classes = List(
          OntoValue(OntoIRIs.ontoClass, OntoIRIs.InfoItemCls)
        ),
        dataProperties = List(
          OntoValue(OntoIRIs.hasText, text),
          OntoValue(OntoIRIs.hasTitle, title)
        )
      )
  }

  def genNewCustomField(iri: String, name: String, value: String): OntoIndividualDto = {
    new OntoIndividualDto(OntoIRI(iri))
      .copy(
        dataProperties = List(
          OntoValue(BaseOntoIRIs.SystemDataProps.hasDataPropName, name),
          OntoValue(BaseOntoIRIs.SystemDataProps.hasDataPropValue, value)
        )
      )
  }
}
