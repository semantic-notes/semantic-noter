package com.semn.utils

object OntoIRIs {
  val ontoClass = "http://www.w3.org/1999/02/22-rdf-syntax-ns#type"
  val InfoItemCls = "baseOntology:InfoItem"
  val Note = "baseOntology:Note"
  val System = "baseOntology:System"
  val Unhandled = "baseOntology:Unhandled"
  val Concept = "baseOntology:Concept"
  val label = "rdfs:label"
  val comment = "rdfs:comment"
  val hasConceptDescr = "baseOntology:hasItemDescription"
  val hasText = "baseOntology:hasText"
  val hasTitle = "baseOntology:hasTitle"
  val isPartOf = "baseOntology:isPartOf"
  val hasPart = "baseOntology:hasPart"
}