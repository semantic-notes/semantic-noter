package com.semn.utils

import java.io.File

import akka.NotUsed
import akka.actor.ActorSystem
import com.gardi.jena.fuseki.{FusekiService, TripleStoreInfo}
import com.gardi.shared.{OntoConfig, OntoParams}
import com.lightbend.lagom.scaladsl.api.ServiceCall
import com.typesafe.config.ConfigFactory
import org.apache.jena.fuseki.main.FusekiServer
import org.eclipse.jetty.server.{Request, RequestLog, Response}
import org.slf4j.{Logger, LoggerFactory}

import scala.concurrent.Future

class FusekiMemSrv(actorSystem: ActorSystem, ontoParams: Option[OntoParams] = None) extends FusekiService {
  private final val log: Logger = LoggerFactory.getLogger(classOf[FusekiService])
  def logger = log
  def getOntoConfigPath: Option[OntoParams] = ontoParams
  val baseOntoCfg = ConfigFactory
    .parseFile(new File(getOntoConfigPath.map(_.ontoConfigPath).getOrElse("configurations/onto.conf")))
  val cfg = OntoConfig(baseOntoCfg)

  val port = 3030

  val server = FusekiServer.create()
    .loopback(false)
    .port(port)
    .verbose(true)
    .parseConfigFile(cfg.storageConfig)
    .build()

  server.server.setRequestLog(new RequestLog {
    override def log(request: Request, response: Response): Unit = {
      //logger.info(s"${request.getMethod} : ${request}")
    }
  })

  server.start()
  log.info("Started")

  def stop() = {
    log.info("Stop")
    server.stop()
  }

  def _getSettings(): TripleStoreInfo = {
    val info = server.server.getURI
    try {
      val inet = java.net.InetAddress.getByName(serviceName)
      val url = s"${info.getScheme}://${serviceName}:${info.getPort}"
      TripleStoreInfo(url)
    } catch {
      case err: Throwable => {
        val url = s"${info.getScheme}://${info.getHost}:${info.getPort}"
        TripleStoreInfo(url)
      }
    }
  }

  override def getSettings(): ServiceCall[NotUsed, TripleStoreInfo] = ServiceCall { _ =>
    Future.successful(_getSettings())
  }
}
