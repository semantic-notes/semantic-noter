package com.semn.utils

import java.net.URI

import com.lightbend.lagom.scaladsl.api.Descriptor.Call
import com.lightbend.lagom.scaladsl.api.ServiceLocator
import com.lightbend.lagom.scaladsl.client.CircuitBreakingServiceLocator
import com.lightbend.lagom.scaladsl.cluster.ClusterComponents
import com.lightbend.lagom.scaladsl.persistence.cassandra.CassandraPersistenceComponents
import com.lightbend.lagom.scaladsl.playjson.{JsonSerializer, JsonSerializerRegistry}
import com.lightbend.lagom.scaladsl.server.{LagomApplication, LagomApplicationContext, LocalServiceLocator}
import com.lightbend.lagom.scaladsl.testkit.TestTopicComponents
import com.lightbend.lagom.spi.persistence.InMemoryOffsetStore
import org.gardi.security.SecurityModel
import play.api.libs.ws.ahc.AhcWSComponents

import scala.collection.immutable
import scala.concurrent.Future

abstract class TestApplication(context: LagomApplicationContext,
                               val staticLocations: Map[String, String] = Map())
  extends LagomApplication(context)
    with TestTopicComponents
    with CassandraPersistenceComponents
    with ClusterComponents
    with AhcWSComponents
    with SecurityModel
    with LocalServiceLocator {
  //override lazy val offsetStore = new InMemoryOffsetStore

  override lazy val serviceLocator: ServiceLocator =
    new CircuitBreakingServiceLocator(circuitBreakersPanel)(executionContext) {
      val services = lagomServer.serviceBindings.map(_.descriptor.name).toSet

      def getUri(name: String): Future[Option[URI]] = {
        lagomServicePort.map {
          case port if services(name) => Some(URI.create(s"http://localhost:$port"))
          case _ => None
        }(executionContext)
      }

      override def locate(name: String, serviceCall: Call[_, _]): Future[Option[URI]] = {
        getUri(name)
      }

      override def locateAll(name: String): Future[List[URI]] = {
        staticLocations.get(name) match {
          case Some(value) => Future.successful(List(new URI(value)))
          case _ => super.locateAll(name)
        }
      }
    }

  override def jsonSerializerRegistry: JsonSerializerRegistry =
    new JsonSerializerRegistry {
      override def serializers = immutable.Seq.empty[JsonSerializer[_]]
    }
}
