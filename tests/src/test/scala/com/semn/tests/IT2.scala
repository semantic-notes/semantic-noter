package com.semn.tests

import com.semn.utils
import com.semn.utils.{OntoIRIs, TestCluster}
import org.gardi.dto.{OntoIRI, OntoIndividualDto, OntoValue}
import org.scalatest.concurrent.ScalaFutures
import org.scalatest.{AsyncWordSpec, BeforeAndAfterAll, Matchers, OptionValues}
import org.slf4j.{Logger, LoggerFactory}

import scala.concurrent.duration._

class IT2 extends AsyncWordSpec with Matchers with BeforeAndAfterAll with ScalaFutures with OptionValues {
  private final val log: Logger = LoggerFactory.getLogger(classOf[IT2])

  implicit override val patienceConfig = PatienceConfig(30.seconds, 150.millis)

  val testCluster = new TestCluster()

  def fusekiService = testCluster.fusekiService
  def userService = testCluster.userService
  val ontoEntityService = testCluster.ontoEntityService
  val threadsService = testCluster.threadsService

  protected override def beforeAll() = {
    super.beforeAll()
  }

  protected override def afterAll() = {
    testCluster.stop()
    super.afterAll()
  }

  val userAuth = testCluster.prepareUser("test@mail.ru", "testPass")
  val threadId = testCluster.prepareThread(userAuth, "TestTh1", "TestTh1Descr")

  val getUserThread = testCluster.getUserThread(userAuth) _
  val addToCurrentThread: OntoIndividualDto => OntoIndividualDto = testCluster.addEntityToThread(userAuth, threadId)(_)
  val describeEntities = testCluster.describeEntities(userAuth, threadId)(_)

  val userThreadInitial = getUserThread(threadId)(true)
  val rootConceptIRI = addToCurrentThread(utils.genNewConcept("Root")).iri.toString

  "Base scenario 2" should {
    "add 100 InfoItems" in {
      val count = 1000
      var totalTime: Long = 0;
      var min = Long.MaxValue;
      var max = Long.MinValue
      (1 to count).foreach(ind => {
        val ret = testCluster.withTimeMetric[OntoIndividualDto](() => {
          addToCurrentThread(
            utils.genNewInfoItem(s"TestInfoItemTitle-${ind}", s"TestInfoItemText-${ind}")
              .copy(
                relations = List(OntoValue(OntoIRIs.isPartOf, rootConceptIRI))
              )
          )
        })
        totalTime = totalTime + ret._2
        if (ret._2 < min) { min = ret._2 }
        if (ret._2 > max) { max = ret._2 }
        log.info(utils.ontIndToString(ret._1))
      })

      val res = testCluster.withTimeMetric(() => describeEntities(Set(rootConceptIRI)).headOption)

      val entity = res._1

      entity.isDefined shouldBe true

      val hasPartRels = entity
        .map(_.relations)
        .map(items => items
          .filter(el => el.iri == OntoIRI(OntoIRIs.hasPart))
          .map(_.value))
        .getOrElse(Nil)

      log.info(s"Mid time ${  totalTime / count } ms | RootCpt: ${res._2} ms | Max ${max}, min ${min}")

      hasPartRels.length shouldBe count
    }
  }
}


