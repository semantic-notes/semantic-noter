package com.semn.tests

import com.lightbend.lagom.scaladsl.api.transport.RequestHeader
import com.semn.utils
import com.semn.utils.{OntoIRIs, TestCluster}
import org.gardi.dto.{CreateNoteDto, OntoIRI, OntoIndividualDto, OntoValue}
import org.gardi.users._
import org.scalatest.concurrent.ScalaFutures
import org.scalatest.{AsyncWordSpec, BeforeAndAfterAll, Matchers, OptionValues}
import org.slf4j.{Logger, LoggerFactory}

import scala.concurrent.ExecutionContext.Implicits.global

class IT1 extends AsyncWordSpec with Matchers with BeforeAndAfterAll with ScalaFutures with OptionValues {
  private final val log: Logger = LoggerFactory.getLogger(classOf[IT1])

  val testCluster = new TestCluster()

  def fusekiService = testCluster.fusekiService
  def userService = testCluster.userService
  val ontoEntityService = testCluster.ontoEntityService
  val threadsService = testCluster.threadsService

  protected override def beforeAll() = {
    super.beforeAll()
  }

  protected override def afterAll() = {
    testCluster.stop()
    super.afterAll()
  }

    val userCreationDto = User("test", Some("test@mail.com"), Some("testPass"))
    var userDto: Option[UserResponse] = None
    var userLoginData: Option[UserAuth] = None
    var userThread: Option[UserThread] = None
    var noteId: Option[String] = None
    var baseConcept: Option[OntoIndividualDto] = None

    def setHeaders(req: RequestHeader): RequestHeader = req.withHeader("Authorization", userLoginData.map(_.token).get)

  "Base scenario" should {
    "create user" in {
      userService.create()
        .invoke(userCreationDto)
        .map(resp => {
          log.info(s"Id: ${resp.id} - ${resp.username} - ${resp.email.get}")
          userDto = Some(resp)
          resp.email shouldBe userCreationDto.email
        })
    }
    "login user" in {
        userService.login()
          .invoke(UserLogin(userCreationDto.email.get, userCreationDto.password.get))
          .map(resp => {
            log.info(s"\nUser(${resp.id})\n")
            userLoginData = Some(resp)
            resp.id shouldBe userDto.map(_.id).get
          })
      }
    "create thread" in {
      threadsService
        .addThreadForUser(userLoginData.map(_.id).get)
        .handleRequestHeader(utils.setTokenHeader(userLoginData.map(_.token).get))
        .invoke(AddThread("TestThread", Some("test")))
        .map(resp => {
          log.info(s"\nAddThread response: ${resp}\n")
          1 shouldBe 1
        })
    }
    "list user threads" in {
      threadsService
        .listUserThreads(userLoginData.map(_.id).get)
        .handleRequestHeader(utils.setTokenHeader(userLoginData.map(_.token).get))
        .invoke()
        .map(resp => {
          val thread = resp.head
          log.info(s"\nFirstThread ${thread}\n")
          userThread = Some(thread)
          resp.length shouldBe 1
          thread.name shouldBe "TestThread"
        })
    }
    "add note" in {
      threadsService
        .addNote(userLoginData.map(_.id).get, userThread.map(_.uuid).get)
        .handleRequestHeader(utils.setTokenHeader(userLoginData.map(_.token).get))
        .invoke(CreateNoteDto("TestNote", Some("TestNoteTitle")))
        .map(resp => {
          log.info(s"\nNoteCreation ${resp}\n")
          noteId = Some(resp)
          1 shouldBe 1
        })
    }
    "check note in thread" in {
      threadsService
        .getUserThread(userLoginData.map(_.id).get, userThread.map(_.uuid).get, Some(true))
        .handleRequestHeader(utils.setTokenHeader(userLoginData.map(_.token).get))
        .invoke()
        .map(resp => {
          val targetIri = OntoIRI(s"${userThread.map(_.ontoModelUri).get}#${noteId.get}")
          val noteInd = resp
            .individuals
            .find(el => el.iri == targetIri)
            .getOrElse(null)
          noteInd.iri shouldBe targetIri
          utils.hasClass(OntoIRIs.InfoItemCls)(noteInd) shouldBe true
        })
    }
    "add concept" in {
      threadsService
        .createEntity(userLoginData.map(_.id).get, userThread.map(_.uuid).get)
        .handleRequestHeader(utils.setTokenHeader(userLoginData.map(_.token).get))
        .invoke(utils.genNewConcept("TestConcept"))
        .map(resp => {
          log.info(s"Concept DTO: ${utils.ontIndToString(resp)}")
          baseConcept = Some(resp)
          val isConcept = utils.hasClass(OntoIRIs.Concept)(resp)
          isConcept shouldBe true
        })
    }
    "add concept2" in {
      threadsService
        .createEntity(userLoginData.map(_.id).get, userThread.map(_.uuid).get)
        .handleRequestHeader(utils.setTokenHeader(userLoginData.map(_.token).get))
        .invoke(utils.genNewConcept("!!!TestConcept!!!!").copy(
          relations = List(
            OntoValue(OntoIRIs.hasPart, baseConcept.map(_.iri.iri).get)
          )
        ))
        .map(resp => {
          log.info(s"Concept DTO: ${utils.ontIndToString(resp)}")
          val isConcept = utils.hasClass(OntoIRIs.Concept)(resp)
          isConcept shouldBe true
        })
    }
    "check concept in thread" in {
      threadsService
        .getUserThread(userLoginData.map(_.id).get, userThread.map(_.uuid).get, Some(true))
        .handleRequestHeader(utils.setTokenHeader(userLoginData.map(_.token).get))
        .invoke()
        .map(resp => {
          val hasConcept = resp.individuals
            .exists(utils.hasClass(OntoIRIs.Concept))
          hasConcept shouldBe true
        })
    }
    "add note as IndObject" in {
      threadsService
        .createEntity(userLoginData.map(_.id).get, userThread.map(_.uuid).get)
        .handleRequestHeader(utils.setTokenHeader(userLoginData.map(_.token).get))
        .invoke(utils.genNewInfoItem("TestInfoItemTitle", "TestInfoItemText"))
        .map(resp => {
          log.info(utils.ontIndToString(resp))
          utils.hasClass(OntoIRIs.InfoItemCls)(resp) shouldBe true
        })
    }
    "check objects in thread" in {
      threadsService
        .getUserThread(userLoginData.map(_.id).get, userThread.map(_.uuid).get, Some(true))
        .handleRequestHeader(utils.setTokenHeader(userLoginData.map(_.token).get))
        .invoke()
        .map(resp => {
          val infoItem = resp.individuals.find(utils.hasDataValue(OntoIRIs.hasTitle, "TestInfoItemTitle"))
          log.info(s"${infoItem.map(utils.ontIndToString)}")
          infoItem
            .map(utils.hasDataValue(OntoIRIs.hasText, "TestInfoItemText"))
            .getOrElse(false) shouldBe true
        })
    }
  }
}


