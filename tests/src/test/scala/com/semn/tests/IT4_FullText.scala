package com.semn.tests

import com.semn.utils
import com.semn.utils.{OntoIRIs, TestCluster}
import org.gardi.dto.{OntoIRI, OntoIndividualDto, OntoValue}
import org.gardi.utils.sparql.BaseOntoIRIs
import org.scalatest.concurrent.ScalaFutures
import org.scalatest.{AsyncWordSpec, BeforeAndAfterAll, Matchers, OptionValues}
import org.slf4j.{Logger, LoggerFactory}

import scala.concurrent.duration._


class IT4_FullText extends AsyncWordSpec with Matchers with BeforeAndAfterAll with ScalaFutures with OptionValues {
  private final val log: Logger = LoggerFactory.getLogger(classOf[IT2])
  implicit override val patienceConfig = PatienceConfig(30.seconds, 150.millis)
  val testCluster = new TestCluster()

  def fusekiService = testCluster.fusekiService
  def userService = testCluster.userService
  val ontoEntityService = testCluster.ontoEntityService
  val threadsService = testCluster.threadsService

  protected override def beforeAll() = {
    super.beforeAll()
  }

  protected override def afterAll() = {
    testCluster.stop()
    super.afterAll()
  }

  val userAuth = testCluster.prepareUser("test@mail.ru", "testPass")
  val threadId = testCluster.prepareThread(userAuth, "TestTh1", "TestTh1Descr")

  val getUserThread = testCluster.getUserThread(userAuth)(_: String)(false)
  val addToCurrentThread = testCluster.addEntityToThread(userAuth, threadId)(_)
  val updateEntityInThread = testCluster.updateEntityInThread(userAuth, threadId)(_)
  val searchByFields = testCluster.searchByFields(userAuth, threadId)(_, _)
  val describeEntities = testCluster.describeEntities(userAuth, threadId)(_)

  val userThreadInitial = getUserThread(threadId)
  val rootConceptIRI = addToCurrentThread(utils.genNewConcept("TestConcept")).iri.toString
  var itemForChange: Option[OntoIndividualDto] = None

  import org.gardi.dto.OntoIndividualDto.Predicates._

  "Base scenario 3" should {
    "add new infoItem" in {
      val item = addToCurrentThread(
        utils.genNewInfoItem(s"TestInfoItemTitle-1", s"TestInfoItemText-1")
          .copy(
            relations = List(OntoValue(OntoIRIs.isPartOf, rootConceptIRI))
          )
      )

      log.info(utils.ontIndToString(item))

      itemForChange = Some(item)

      val rootCptId = item
        .relations
        .find(el => el.iri == OntoIRI(OntoIRIs.isPartOf) && el.value == rootConceptIRI)
        .map(_.value)
        .getOrElse("")

      val wasCreated = item.getDataPropertyWith(hasIri(BaseOntoIRIs.SystemDataProps.wasCreated))
      val wasUpdated = item.getDataPropertyWith(hasIri(BaseOntoIRIs.SystemDataProps.wasUpdated))

      rootCptId shouldBe rootConceptIRI
      wasCreated.isDefined shouldBe true
      wasUpdated.isDefined shouldBe true

      wasCreated.map(_.value).value shouldBe wasUpdated.map(_.value).value
    }
    "check the FullText" in {
      val fields = Set("baseOntology:hasText", "baseOntology:hasTitle")
      val result = searchByFields(fields, "Test*")
      result.length shouldBe 2
    }
    "check the FullText with Describe" in {

      val count = 10
      (1 to count).foreach(ind => {
        addToCurrentThread(
          utils.genNewInfoItem(s"Putin-is-bad ${ind}", s"Bad boys of Putin ${ind}")
            .copy(
              relations = List(OntoValue(OntoIRIs.isPartOf, rootConceptIRI))
            )
        )
      })

      val fields = Set("baseOntology:hasText", "baseOntology:hasTitle")
      val result = searchByFields(fields, "Putin*")
      val groups = result.groupBy(_.iri)
      val entities = describeEntities(groups.keys.map(_.iri).toSet)
      val checks = entities.toList.map(el => el.getDataPropertyWith(hasValueRegexp(".+(Putin).+")).isDefined)

      groups.keySet.size shouldBe count
      checks.contains(false) should not be(true)
    }
  }
}
