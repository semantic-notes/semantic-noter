package com.semn.tests

import com.semn.utils
import com.semn.utils.{OntoIRIs, TestCluster}
import org.gardi.dto.{OntoIRI, OntoIndividualDto, OntoValue}
import org.gardi.utils.sparql.BaseOntoIRIs
import org.scalatest.concurrent.ScalaFutures
import org.scalatest.{AsyncWordSpec, BeforeAndAfterAll, Matchers, OptionValues}
import org.slf4j.{Logger, LoggerFactory}

import scala.concurrent.duration._


class IT3 extends AsyncWordSpec with Matchers with BeforeAndAfterAll with ScalaFutures with OptionValues {
  private final val log: Logger = LoggerFactory.getLogger(classOf[IT2])
  implicit override val patienceConfig = PatienceConfig(30.seconds, 150.millis)
  val testCluster = new TestCluster()

  def fusekiService = testCluster.fusekiService
  def userService = testCluster.userService
  val ontoEntityService = testCluster.ontoEntityService
  val threadsService = testCluster.threadsService

  protected override def beforeAll() = {
    super.beforeAll()
  }

  protected override def afterAll() = {
    testCluster.stop()
    super.afterAll()
  }

  val userAuth = testCluster.prepareUser("test@mail.ru", "testPass")
  val threadId = testCluster.prepareThread(userAuth, "TestTh1", "TestTh1Descr")

  val getUserThread = testCluster.getUserThread(userAuth)(_: String)(false)
  val addToCurrentThread = testCluster.addEntityToThread(userAuth, threadId)(_)
  val updateEntityInThread = testCluster.updateEntityInThread(userAuth, threadId)(_)

  val userThreadInitial = getUserThread(threadId)
  val rootConceptIRI = addToCurrentThread(utils.genNewConcept("Root")).iri.toString
  var itemForChange: Option[OntoIndividualDto] = None

  import org.gardi.dto.OntoIndividualDto.Predicates._

  "Base scenario 3" should {
    "add new infoItem" in {
      val item = addToCurrentThread(
        utils.genNewInfoItem(s"TestInfoItemTitle-1", s"TestInfoItemText-1")
          .copy(
            relations = List(OntoValue(OntoIRIs.isPartOf, rootConceptIRI))
          )
      )
      log.info(utils.ontIndToString(item))

      itemForChange = Some(item)

      val rootCptId = item
        .relations
        .find(el => el.iri == OntoIRI(OntoIRIs.isPartOf) && el.value == rootConceptIRI)
        .map(_.value)
        .getOrElse("")

      val wasCreated = item.getDataPropertyWith(hasIri(BaseOntoIRIs.SystemDataProps.wasCreated))
      val wasUpdated = item.getDataPropertyWith(hasIri(BaseOntoIRIs.SystemDataProps.wasUpdated))

      rootCptId shouldBe rootConceptIRI
      wasCreated.isDefined shouldBe true
      wasUpdated.isDefined shouldBe true

      wasCreated.map(_.value).value shouldBe wasUpdated.map(_.value).value
    }
    "change infoItem" in {
      val oldItem = itemForChange.get
      val changedITem = updateEntityInThread(oldItem.copy(
        dataProperties = List(
          OntoValue(OntoIRIs.hasText, "TestInfoItemText-2"),
          OntoValue(OntoIRIs.hasTitle, "TestInfoItemTitle-2")
        )
      ))
      log.info(s"Changed item: ${utils.ontIndToString(changedITem)}")
      val wasCreated = changedITem.getDataPropertyWith(hasIri(BaseOntoIRIs.SystemDataProps.wasCreated))
      val wasUpdated = changedITem.getDataPropertyWith(hasIri(BaseOntoIRIs.SystemDataProps.wasUpdated))
      oldItem.iri shouldBe changedITem.iri
      wasCreated shouldNot equal(wasUpdated)
      changedITem.getDataPropertyWith(hasValue("TestInfoItemText-2")).isDefined shouldBe true
      changedITem.getDataPropertyWith(hasValue("TestInfoItemTitle-2")).isDefined shouldBe true
    }
    "remove text from infoItem" in {
      val oldItem = itemForChange.get
      val changedITem = updateEntityInThread(oldItem.copy(
        dataProperties = oldItem.dataProperties.filterNot(hasIri(OntoIRIs.hasText))
      ))
      log.info(s"Changed item: ${utils.ontIndToString(changedITem)}")
      oldItem.iri shouldBe changedITem.iri
      changedITem.getDataPropertyWith(hasValue("TestInfoItemText-1")).isDefined shouldBe false
      changedITem.getDataPropertyWith(hasValue("TestInfoItemTitle-1")).isDefined shouldBe true
    }
    "check the Thread" in {
      val thread = getUserThread(threadId)
      log.info(s"!!! ${utils.threadToString(thread)}")
      1 shouldBe 1
    }
  }
}
