package com.semn.tests

import com.semn.utils
import com.semn.utils.TestCluster
import org.gardi.dto.OntoIndividualDto.Predicates.hasValueRegexp
import org.gardi.dto.{OntoIndividualDto, OntoValue}
import org.gardi.utils.sparql.BaseOntoIRIs
import org.scalatest.concurrent.ScalaFutures
import org.scalatest.{AsyncWordSpec, BeforeAndAfterAll, Matchers, OptionValues}
import org.slf4j.{Logger, LoggerFactory}

import scala.concurrent.duration._


class IT5_CustomFields extends AsyncWordSpec with Matchers with BeforeAndAfterAll with ScalaFutures with OptionValues {
  private final val log: Logger = LoggerFactory.getLogger(classOf[IT2])
  implicit override val patienceConfig = PatienceConfig(30.seconds, 150.millis)
  val testCluster = new TestCluster()

  def fusekiService = testCluster.fusekiService
  def userService = testCluster.userService
  val ontoEntityService = testCluster.ontoEntityService
  val threadsService = testCluster.threadsService

  protected override def beforeAll() = {
    super.beforeAll()
  }

  protected override def afterAll() = {
    testCluster.stop()
    super.afterAll()
  }

  import org.gardi.dto.OntoIndividualDto.Predicates

  val userAuth = testCluster.prepareUser("test@mail.ru", "testPass")
  val threadId = testCluster.prepareThread(userAuth, "TestTh1", "TestTh1Descr")

  val getUserThread = testCluster.getUserThread(userAuth) _
  val addToCurrentThread = testCluster.addEntityToThread(userAuth, threadId)(_)
  val updateEntityInThread = testCluster.updateEntityInThread(userAuth, threadId)(_)
  val searchByFields = testCluster.searchByFields(userAuth, threadId)(_, _)
  val describeEntities = testCluster.describeEntities(userAuth, threadId)(_)

  val userThreadInitial = getUserThread(threadId)(false)
  val rootConceptIRI = addToCurrentThread(utils.genNewConcept("TestConcept")).iri.toString
  var itemForChange: Option[OntoIndividualDto] = None

  "Base scenario 5" should {
    "add new infoItem" in {
      val newItem =  utils.genNewInfoItem(s"Title-1", s"Text 1")
        .copy(
          relations = List(
            OntoValue(BaseOntoIRIs.SystemDataProps.hasDataProperty,  "_:b0"),
            OntoValue(BaseOntoIRIs.SystemDataProps.hasDataProperty,  "_:b1")
          ),
          blankNodes = List(
            utils.genNewCustomField("_:b0", "Field1", "Value1"),
            utils.genNewCustomField("_:b1", "Field2", "Value2")
          )
        )
      val objWithFields = addToCurrentThread(newItem)


      val fieldId = objWithFields.relations
        .filter(Predicates.hasIri(BaseOntoIRIs.SystemDataProps.hasDataProperty))
        .take(1)
        .map(_.value)
        .head

      val toChange = objWithFields.copy(
        blankNodes = objWithFields.blankNodes.map(el => {
          if (el.hasIRI(fieldId)) {
            el.copy(dataProperties = el.dataProperties.map(obj => {
              if (obj.hasIRI(BaseOntoIRIs.SystemDataProps.hasDataPropValue)) {
                obj.copy(value = "Value222222")
              } else {
                obj
              }
            }))
          } else {
            el
          }
        }))

      val changed = updateEntityInThread(toChange)

      val customFieldIds = changed.relations
        .filter(Predicates.hasIri(BaseOntoIRIs.SystemDataProps.hasDataProperty))
          .map(el => el.value)

      val customFields = changed.blankNodes.filter(el => customFieldIds.contains(el.iri.toString))

      customFields.length should be(2)
    }

    "test fulltext search" in {
      val fields = Set(BaseOntoIRIs.SystemDataProps.hasDataPropValue)
      val searchResult = searchByFields(fields, "Value*")


      val groups = searchResult.groupBy(_.iri)
      val entities = describeEntities(groups.keys.map(_.iri).toSet)

      val ontoValues = searchResult
        .flatten(sr => entities.filter(el => el.hasValueFor(sr)).map(_.getValueBy(sr)))
        .map(_.get)

      ontoValues.map(hasValueRegexp("(?:Value).+")).contains(false) should be (false)
      searchResult.map(_.isBlankNodeValue()).contains(false) should be (false)
    }
  }
}
