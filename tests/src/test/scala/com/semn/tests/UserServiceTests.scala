package com.semn.tests

import com.gardi.users.{UserService, UserServiceApplication}
import com.lightbend.lagom.scaladsl.server.LocalServiceLocator
import com.lightbend.lagom.scaladsl.testkit.{ProducerStubFactory, ServiceTest}
import com.semn.utils.FusekiMemSrv
import com.softwaremill.macwire._
import org.gardi.users.{User, UserAuth, UserLogin, UserResponse}
import org.scalatest.{AsyncWordSpec, BeforeAndAfterAll, Matchers}
import org.slf4j.{Logger, LoggerFactory}


//class UserServiceTest extends AsyncWordSpec with Matchers with BeforeAndAfterAll {
//  private final val log: Logger = LoggerFactory.getLogger(classOf[UserServiceTest])
//
//  lazy val server = ServiceTest.startServer(
//    ServiceTest.defaultSetup
////      .withCluster(true)
////      .withCassandra(true)
//  ) { ctx =>
//    new UserServiceApplication(ctx) with LocalServiceLocator {
//      val stubFactory = new ProducerStubFactory(actorSystem, materializer)
//      override lazy val fuseki = wire[FusekiMemSrv]
//    }
//  }
//
//  lazy val client = server.serviceClient.implement[UserService]
//
//  protected override def beforeAll() = server
//
//  protected override def afterAll() = {
//    server.stop()
//  }
//
//  Thread.sleep(1000)
//
//  val userCreationDto = User("test", Some("test@mail.com"), Some("testPass"))
//  var userDto: Option[UserResponse] = None
//  var userLoginData: Option[UserAuth] = None
//
//  "The UserService" should {
//    "create user" in {
//      Thread.sleep(1000)
//      client.create()
//        .invoke(userCreationDto)
//        .map(resp => {
//          log.info(s"Id: ${resp.id} - ${resp.username} - ${resp.email.get}")
//          userDto = Some(resp)
//          resp.email shouldBe userCreationDto.email
//        })
//      }
//    "login user" in {
//        Thread.sleep(1000)
//        client.login()
//          .invoke(UserLogin(userCreationDto.email.get, userCreationDto.password.get))
//          .map(resp => {
//            log.info(s"\nUser(${resp.id})\n")
//            userLoginData = Some(resp)
//            resp.id shouldBe userDto.map(_.id).get
//          })
//
//      }
//    "check token" in {
//      Thread.sleep(1000)
//      client.userData(userDto.map(_.id).get)
//        .handleRequestHeader(req => {
//          req.withHeader("Authorization", userLoginData.map(_.token).get)
//        })
//        .invoke()
//        .map(resp => {
//          log.info(s"\nUser(${resp})\n")
//          resp.id shouldBe userLoginData.map(_.id).get
//        })
//    }
//  }
//}

