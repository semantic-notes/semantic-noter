package com.semn.tests

import com.semn.utils
import com.semn.utils.{OntoIRIs, TestCluster}
import org.gardi.dto
import org.gardi.dto.OntoIndividualDto.Predicates.hasValueRegexp
import org.gardi.dto.{EntityQueryParams, OntoIndividualDto, OntoValue, SearchParams}
import org.gardi.utils.sparql.BaseOntoIRIs
import org.scalatest.concurrent.ScalaFutures
import org.scalatest.{AsyncWordSpec, BeforeAndAfterAll, Matchers, OptionValues}
import org.slf4j.{Logger, LoggerFactory}

import scala.concurrent.duration._

class IT6_BaseCases extends AsyncWordSpec with Matchers with BeforeAndAfterAll with ScalaFutures with OptionValues {
  private final val log: Logger = LoggerFactory.getLogger(classOf[IT2])
  implicit override val patienceConfig = PatienceConfig(30.seconds, 150.millis)
  val testCluster = new TestCluster()

  def fusekiService = testCluster.fusekiService
  def userService = testCluster.userService
  val ontoEntityService = testCluster.ontoEntityService
  val threadsService = testCluster.threadsService

  protected override def beforeAll() = {
    super.beforeAll()
  }

  protected override def afterAll() = {
    testCluster.stop()
    super.afterAll()
  }

  import org.gardi.dto.OntoIndividualDto.Predicates

  val userAuth = testCluster.prepareUser("test@mail.ru", "testPass")
  val threadId = testCluster.prepareThread(userAuth, "TestTh1", "TestTh1Descr")

  val getUserThread = testCluster.getUserThread(userAuth) _
  val addToCurrentThread = testCluster.addEntityToThread(userAuth, threadId)(_)
  val updateEntityInThread = testCluster.updateEntityInThread(userAuth, threadId)(_)
  val searchByFields = testCluster.searchByFields(userAuth, threadId)(_, _)
  val describeEntities = testCluster.describeEntities(userAuth, threadId)(_)
  val getIndividuals = testCluster.getIndividuals(userAuth, threadId)(_)
  val getEntities = testCluster.getEntities(userAuth, threadId)(_)

  val userThreadInitial = getUserThread(threadId)(false)
  val rootConceptIRI = addToCurrentThread(utils.genNewConcept("TestConcept")).iri.toString
  var itemForChange: Option[OntoIndividualDto] = None

  "Base scenario 5" should {
    "add new infoItem" in {
      val toCreate = (1 to 2).map(ind => {
        utils.genNewInfoItem(s"Title-${ind}", s"Text ${ind}")
          .copy(
            relations = List(
              OntoValue(OntoIRIs.isPartOf, rootConceptIRI),
              OntoValue(BaseOntoIRIs.SystemDataProps.hasDataProperty,  "_:b0"),
              OntoValue(BaseOntoIRIs.SystemDataProps.hasDataProperty,  "_:b1"),
              OntoValue("baseOntology:isUnhandled", "?thisIri")
            ),
            blankNodes = List(
              utils.genNewCustomField("_:b0", "Field1", s"Value1 ${ind}"),
              utils.genNewCustomField("_:b1", "Field2", s"Value2 ${ind}")
            )
          )
      })

      toCreate.foreach(ind => addToCurrentThread(ind))

      val rootCpt = describeEntities(Set(rootConceptIRI)).head

      val entities = getEntities(EntityQueryParams(
          searchParams = Some(SearchParams(
            fields = Set("baseOntology:hasText", "baseOntology:hasTitle", BaseOntoIRIs.SystemDataProps.hasDataPropValue),
            value = "Value2*"
          ))
        )
      )

      log.info(s"ROOT: ${utils.ontIndToString(rootCpt)}")
      //log.info(s"!!!! ${ entities.map(utils.ontIndToString).mkString("\n============\n") }")
      1 shouldBe 1
    }
  }
}