package com.smen

import com.lightbend.lagom.scaladsl.api._

trait TestSrv extends Service {
  override final def descriptor = {
    import Service._    // @formatter:off
    named("test-srv")
      .withCalls()
    // @formatter:on
  }
}
