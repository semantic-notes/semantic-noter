#!/bin/bash

function deployKb() {
    rp generate-kubernetes-resources $1 \
        --generate-all \
        --pod-controller-replicas 1 \
        --env JAVA_OPTS="-Dplay.http.secret.key=simple" \
	--namespace semantic-noter \
        --registry-disable-https --registry-disable-tls-validation --registry-use-local
}

SRV="gardi-srv.lan:32000"
VER=""

if [ -z "$1" ]; then
  VER="1.0-SNAPSHOT"
else
  VER=$1
fi

if [ ! -d "yaml" ]; then
  mkdir yaml
fi

#deployKb "$SRV/jenafusekiimpl:$VER" > yaml/1fuseki.yaml
deployKb "$SRV/user-service:$VER" > yaml/user.yaml
deployKb "$SRV/thread-service:$VER" > yaml/thread.yaml
    
