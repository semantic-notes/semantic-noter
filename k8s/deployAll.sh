#!/bin/bash

ACTION=""

if [ -z "$1" ]; then
  ACTION="apply"
else
  ACTION=$1
fi


find $PWD/yaml -type f -name "*.yaml" | sort -n | while read line; do 
 echo "Deploy $line"
 kubectl $ACTION --force -f $line
done
