#!/bin/bash
minikube start --cpus 4 --memory 8192
eval $(minikube docker-env)
minikube addons enable ingress
helm init --wait
helm repo add lightbend-helm-charts https://lightbend.github.io/helm-charts
helm update
helm install lightbend-helm-charts/reactive-sandbox --name reactive-sandbox