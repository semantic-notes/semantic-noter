import java.util

import sun.security.tools.PathList
import com.lightbend.lagom.core.LagomVersion

val disabledFork = Option(System.getProperty("disabledFork")).map(el => "true" equalsIgnoreCase el).getOrElse(false)
ivyLoggingLevel := UpdateLogging.Full
logLevel := Level.Debug
lagomCassandraCleanOnStart in ThisBuild := true

organization in ThisBuild := "com.gardi"

val depOffline = false
version in ThisBuild ~= (_.replace('+', '-'))
dynver in ThisBuild ~= (_.replace('+', '-'))

// the Scala version that will be used for cross-compiled libraries
scalaVersion in ThisBuild := "2.11.11"
val ontApiVersion = "1.4.2"
val jenaVersion = "3.12.0"
val owlApiVersion = "5.1.11"

val macwire = "com.softwaremill.macwire" %% "macros" % "2.3.0" % "provided"
val scalaTest = "org.scalatest" %% "scalatest" % "3.0.4" % Test
val owlApi = "net.sourceforge.owlapi" % "owlapi-distribution" % owlApiVersion
val owlApiOsgiDistr = "net.sourceforge.owlapi" % "owlapi-osgidistribution" % owlApiVersion
val ontApi = "ru.avicomp" % "ontapi" % ontApiVersion
val jenaTdb = "org.apache.jena" % "jena-tdb" % jenaVersion
val jenaRdfConnection = "org.apache.jena" % "jena-rdfconnection" % jenaVersion
val jenaPermissions = "org.apache.jena" % "jena-permissions" % jenaVersion
val jenaARQ = "org.apache.jena" % "jena-arq" % jenaVersion
val jenaOSGI = "org.apache.jena" % "jena-osgi" % jenaVersion
val jenaQueryBuilder = "org.apache.jena" % "jena-querybuilder" % jenaVersion
val jenaFusekiSrv = "org.apache.jena" % "jena-fuseki-main" % jenaVersion
val jenaText = "org.apache.jena" % "jena-text" % jenaVersion
val rxScala = "io.reactivex" %% "rxscala" % "0.26.5"
val akkaStreams = "com.typesafe.akka" %% "akka-stream" % "2.5.26"
val openlletDistr = "com.github.galigator.openllet" % "openllet-distribution" % "2.6.5-gardi"
val swrlApi = "edu.stanford.swrl" % "swrlapi" % "2.0.6"
val droolsSwrlApiEngine = "edu.stanford.swrl" % "swrlapi-drools-engine" % "2.0.6"
val droolsOsgi = "org.drools" % "drools-osgi-integration" % "6.5.0.Final"
val droolsKnwApi = "org.drools" % "knowledge-api" % "6.5.0.Final"
val akkaStreamKafka = "com.typesafe.akka" %% "akka-stream-kafka" % "1.0-RC1"
val lagomPac4j = "org.pac4j" %% "lagom-pac4j" % "1.0.0"
val pac4j_http = "org.pac4j" % "pac4j-http" % "3.3.0"
val pac4j_jwt = "org.pac4j" % "pac4j-jwt" % "3.3.0"
val pac4j_oauth = "org.pac4j" % "pac4j-oauth" % "3.3.0"
val pac4j_core = "org.pac4j" % "pac4j-core" % "3.3.0"
val idGenerator = "com.softwaremill.common" %% "id-generator" % "1.2.0"
val scalaUri = "io.lemonlabs" %% "scala-uri" % "1.4.1"
val apacheCommonLang = "org.apache.commons" % "commons-lang3" % "3.8.1"

val logging = Seq(
  "org.slf4j" % "slf4j-api" % "1.7.30",
)

val lagomScaladslAkkaDiscovery = "com.lightbend.lagom" %% "lagom-scaladsl-akka-discovery-service-locator" % LagomVersion.current

val akkaMgmtVersion = "1.0.5"
val akkaManagement = "com.lightbend.akka.management" %% "akka-management" % akkaMgmtVersion
val akkaMgmtHttp =   "com.lightbend.akka.management" %% "akka-management-cluster-http" % akkaMgmtVersion
val akkaClusterBootstrap = "com.lightbend.akka.management" %% "akka-management-cluster-bootstrap" % akkaMgmtVersion
val akkaDiscoveryK8s =  "com.lightbend.akka.discovery" %% "akka-discovery-kubernetes-api" % akkaMgmtVersion


val akkaManagementDeps = Seq(
  akkaDiscoveryK8s
)

val sparkVersion = "2.4.3"
val jacksonVersion = "2.9.9"
lazy val sparkDeps = Seq(
  "org.apache.spark" %% "spark-core" % sparkVersion,
  "org.apache.spark" %% "spark-sql" % sparkVersion,
  "org.apache.spark" %% "spark-mllib" % sparkVersion,
  "org.apache.spark" %% "spark-streaming" % sparkVersion,
  "com.fasterxml.jackson.module" % "jackson-module-scala_2.11" % jacksonVersion,
)

lazy val excludedRules = Seq(
   ExclusionRule(organization = "com.ibm.sparktc.sparkbench"),
   ExclusionRule("org.slf4j", "log4j-over-slf4j"),
   ExclusionRule("org.javabits.jgrapht", "jgrapht-core"),
)

val excludedDependencies = Seq(
  "org.slf4j" % "log4j-over-slf4j",
  "org.slf4j" % "slf4j-log4j12"
)

val sansaVersion = "0.6.0"

lazy val sansaStack = Seq(
    "net.sansa-stack" %% "sansa-rdf-spark" % sansaVersion excludeAll(excludedRules: _*),
    "net.sansa-stack" %% "sansa-owl-spark" % sansaVersion excludeAll(excludedRules: _*),
    "net.sansa-stack" %% "sansa-inference-spark" % sansaVersion excludeAll(excludedRules: _*),
    "net.sansa-stack" %% "sansa-query-spark" % sansaVersion excludeAll(excludedRules: _*),
    "net.sansa-stack" %% "sansa-ml-spark" % sansaVersion excludeAll(excludedRules: _*),
    "org.jgrapht" % "jgrapht-core" % "1.1.0"
)

val hdfsVersion = "3.2.1"

lazy val hdfs = "org.apache.hadoop" % "hadoop-hdfs" % hdfsVersion

lazy val owlApiDeps = Seq(
  jenaARQ,
  jenaOSGI,
  "org.jgrapht" % "jgrapht-core" % "1.1.0",
)

def dockerSettings = Seq(
  dockerUpdateLatest := true,
  dockerBaseImage := "adoptopenjdk/openjdk8",
  dockerUsername := sys.props.get("docker.username"),
  dockerRepository := sys.props.get("docker.registry"),
)

def evictionSettings: Seq[Setting[_]] = Seq(
  // This avoids a lot of dependency resolution warnings to be showed.
  // They are not required in Lagom since we have a more strict whitelist
  // of which dependencies are allowed. So it should be safe to not have
  // the build logs polluted with evictions warnings.
  evictionWarningOptions in update := EvictionWarningOptions.default
    .withWarnTransitiveEvictions(false)
    .withWarnDirectEvictions(false)
)


val root = file("")

lazy val dependencyFix = taskKey[Unit]("deps-fix")
lazy val startupTransition: State => State = { s: State =>
  "dependencyFix" :: s
}

lazy val fixedLibs = Map(
  //"jena-arq-3.9.0.jar" -> "org.apache.jena/jena-arq/jars"
)

lazy val filesToPackage = Seq(
  file("configurations/fusekiConfig.ttl") -> "configurations/fusekiConfig.ttl",
  file("configurations/onto.conf") -> "configurations/onto.conf",
  file("configurations/jwt-jwk.json") -> "configurations/jwt-jwk.json",
  file("configurations/totp.json") -> "configurations/totp.json",
  file("configurations/oauth2/google.json") -> "configurations/oauth2/google.json",
  file("data/ontology/semantic-noter-base.owl") -> "data/ontology/semantic-noter-base.owl",
  file("data/ontology/users.owl") -> "data/ontology/users.owl",
)

lazy val `semantic-noter` = (project in file("."))
  .settings(
    name := "semantic-noter",
    dependencyFix := {
      val ivyCachePath = file(s"${Path.userHome}/.ivy2/cache")
      val libsPath = file(s"${root.absolutePath}/core/lib")
      fixedLibs.keys.foreach(libFName => {
        val libFile = file(s"${libsPath}/${libFName}")
        val copyTo = fixedLibs(libFName)
        val target = file(s"${ivyCachePath.absolutePath}/${copyTo}/${libFName}")
        IO.copyFile(libFile.asFile, target.asFile)
      })
    },
    onLoad in Global := {
      val old = (onLoad in Global).value
      // compose the new transition on top of the existing one
      // in case your plugins are using this hook.
      startupTransition compose old
    }
  )
  .aggregate(
    shared,
    `jenaFuseki`,
    `jenaFusekiImpl`,
    `core`,
    `baseService`,
    `user-service`,
    `thread-service`,
    `onto-entity-service`,
    `filter-service`,
    `tests`
  )

lazy val jenaFuseki = (project in file("jena-fuseki"))
  .settings(
    libraryDependencies ++= Seq(
      lagomScaladslApi
    ),
    offline := depOffline,
  )

lazy val jenaFusekiImpl = (project in file("jena-fuseki-impl"))
  .enablePlugins(LagomScala)
  .settings(
    libraryDependencies ++= Seq(
      macwire,
      jenaFusekiSrv,
      jenaText,
      lagomScaladslPubSub,
      lagomScaladslKafkaBroker,
      lagomScaladslAkkaDiscovery,
      lagomScaladslPersistenceCassandra,
      rxScala,
      akkaStreams,
    ) ++ akkaManagementDeps,
  )
  .settings(evictionSettings: _*)
  .settings(lagomForkedTestSettings: _*)
  .settings(dockerSettings)
  .settings(
    dockerExposedPorts := Seq(3030, 3030)
  )
  .settings(offline := depOffline)
  .dependsOn(`jenaFuseki`, shared)

val coreLibs = Seq(
  macwire,
  idGenerator,
  jenaTdb,
  jenaRdfConnection,
  jenaPermissions,
  jenaARQ,
  jenaQueryBuilder,
  jenaOSGI,
  owlApi,
  owlApiOsgiDistr,
  ontApi,
  rxScala,
  akkaStreams,
  swrlApi,
  droolsSwrlApiEngine,
  openlletDistr,
  "com.typesafe" % "config" % "1.3.2",
  pac4j_jwt,
  pac4j_http,
  pac4j_oauth,
  lagomPac4j,
  scalaUri,
  apacheCommonLang,
)

lazy val core = (project in file("core"))
  .settings(
    resolvers += "Local Maven Repository" at "file://" + baseDirectory.value + "/../maven_local",
    resolvers += "ImageJ Public" at "https://maven.imagej.net/content/repositories/public/",
    resolvers += "netbeans repository" at "https://bits.netbeans.org/nexus/content/groups/netbeans/",
    resolvers += "Central Maven" at "https://central.maven.org/maven2/",
    resolvers += "Jboss" at "https://repository.jboss.org",
    resolvers += "Sonatype OSS Snapshots" at "https://oss.sonatype.org/content/repositories/snapshots",
    libraryDependencies ++= Seq(
      lagomScaladslApi,
      lagomScaladslServer,
      akkaStreamKafka,
    ),
    libraryDependencies ++= coreLibs,
    libraryDependencies ++= sparkDeps,
    libraryDependencies ++= sansaStack,
    libraryDependencies ++= Seq(hdfs),
    libraryDependencies ++= logging,
  )
  .settings(libraryDependencies ++= Seq(
    lagomScaladslTestKit,
    scalaTest
  ))
  .settings(dependencyOverrides ++= owlApiDeps)
  .settings(offline := depOffline)
  .settings(evictionSettings: _*)
  .settings(lagomForkedTestSettings: _*)
  .dependsOn(`jenaFuseki`, shared)

lazy val shared = (project in file("shared"))
  .settings(libraryDependencies ++= Seq(
    lagomScaladslApi,
    lagomScaladslTestKit,
    scalaTest
  ))

lazy val baseService = (project in file("base-service"))
  .settings(
    libraryDependencies ++= Seq(
      lagomScaladslApi,
      lagomScaladslKafkaBroker,
      lagomScaladslPubSub,
      lagomScaladslPersistenceCassandra,
      macwire,
    )
  )
  .settings(offline := depOffline)
  .settings(evictionSettings: _*)
  .dependsOn(`core`, shared, `jenaFuseki`)

lazy val `user-service` = (project in file("user-service"))
  .enablePlugins(LagomScala)
  .settings(
    libraryDependencies ++= Seq(
      lagomScaladslApi,
      lagomScaladslAkkaDiscovery,
      lagomScaladslPubSub,
      macwire,
    ) ++ akkaManagementDeps,
    mappings in Universal ++= filesToPackage,
  )
  .settings(libraryDependencies ++= Seq(
    lagomScaladslTestKit,
    scalaTest
  ))
  .settings(excludeDependencies ++= excludedDependencies)
  .settings(dependencyOverrides ++= logging)
  .settings(offline := depOffline)
  .settings(evictionSettings: _*)
  .settings(lagomForkedTestSettings: _*)
  .settings(dockerSettings)
  .dependsOn(`jenaFuseki`, `core`, shared, `baseService`)

lazy val `onto-entity-service` = (project in file("onto-entity-service"))
  .enablePlugins(LagomScala)
  .settings(
    libraryDependencies ++= Seq(
      lagomScaladslApi,
      lagomScaladslAkkaDiscovery,
      lagomScaladslPubSub,
      macwire,
      scalaTest,
    ) ++ akkaManagementDeps,
    mappings in Universal ++= filesToPackage,
  )
  .settings(excludeDependencies ++= excludedDependencies)
  .settings(dependencyOverrides ++= logging)
  .settings(offline := depOffline)
  .settings(evictionSettings: _*)
  .settings(lagomForkedTestSettings: _*)
  .settings(dockerSettings)
  .dependsOn(`jenaFuseki`, `core`, shared, `baseService`)

lazy val `thread-service` = (project in file("thread-service"))
  .enablePlugins(LagomScala)
  .settings(
    libraryDependencies ++= Seq(
      lagomScaladslApi,
      lagomScaladslAkkaDiscovery,
      lagomScaladslPubSub,
      macwire,
    ) ++ akkaManagementDeps,
    mappings in Universal ++= filesToPackage,
  )
  .settings(libraryDependencies ++= Seq(
    lagomScaladslTestKit,
    scalaTest
  ))
  .settings(excludeDependencies ++= excludedDependencies)
  .settings(dependencyOverrides ++= logging)
  .settings(offline := depOffline)
  .settings(evictionSettings: _*)
  .settings(lagomForkedTestSettings: _*)
  .settings(dockerSettings)
  .dependsOn(`jenaFuseki`, `jenaFusekiImpl`, `core`, shared, `baseService`, `onto-entity-service`)

lazy val `filter-service` = (project in file("filter-service"))
  .enablePlugins(LagomScala)
  .settings(
    libraryDependencies ++= Seq(
      lagomScaladslApi,
      lagomScaladslAkkaDiscovery,
      lagomScaladslPubSub,
      macwire,
      scalaTest,
    ) ++ akkaManagementDeps,
    mappings in Universal ++= filesToPackage,
  )
  .settings(excludeDependencies ++= excludedDependencies)
  .settings(dependencyOverrides ++= logging)
  .settings(offline := depOffline)
  .settings(evictionSettings: _*)
  .settings(lagomForkedTestSettings: _*)
  .settings(dockerSettings)
  .dependsOn(`jenaFuseki`, `core`, shared, `baseService`)

val testSettings = if (disabledFork) {
  Seq[Setting[_]](fork in Test := false)
} else {
  lagomForkedTestSettings
}

lazy val tests = (project in file("tests"))
  .settings(
    libraryDependencies ++= Seq(
      lagomScaladslApi,
      lagomScaladslCluster,
      lagomScaladslClient,
      lagomScaladslDevMode,
      lagomScaladslKafkaBroker,
      lagomScaladslKafkaClient,
      lagomScaladslPubSub,
      lagomScaladslPersistenceCassandra,
      macwire,
    ),
    libraryDependencies ++= Seq(
      jenaFusekiSrv,
      jenaText
    ),
    libraryDependencies ++= coreLibs,
    libraryDependencies ++= Seq(
      "org.eclipse.jetty.toolchain" % "jetty-osgi-servlet-api" % "3.1.0.M3"
    )
  )
  .settings(libraryDependencies ++= Seq(
    lagomScaladslTestKit,
    scalaTest
  ))
  .settings(excludeDependencies ++= excludedDependencies)
  .settings(dependencyOverrides ++= logging)
  .settings(offline := depOffline)
  .settings(evictionSettings: _*)
  .settings(testSettings: _*)
  .dependsOn(
    shared,
    `baseService`,
    `jenaFuseki`,
//    `jenaFusekiImpl`,
    `user-service`,
    `thread-service`,
    `onto-entity-service`,
    `filter-service`
  )
