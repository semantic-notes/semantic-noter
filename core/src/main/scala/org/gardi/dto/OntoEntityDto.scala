package org.gardi.dto

import java.net.URI

import com.fasterxml.jackson.annotation.JsonRawValue
import org.semanticweb.owlapi.model.{IRI, OWLEntity}
import play.api.libs.json._


case class OntoIRI(@JsonRawValue() val iri: String) {
  override def toString: String = iri
  def isEmpty = iri.isEmpty
  def getLocalId = iri.split('#').reverse.headOption.getOrElse(iri.split(':').tail.headOption.getOrElse(iri))
  def hasIRI(iri: String): Boolean = this.iri == iri
}
object OntoIRI {
  implicit def fromString(iri: String): OntoIRI = OntoIRI(iri)
  implicit def fromIri(iri: IRI): OntoIRI = OntoIRI(iri.toString)
  implicit def toIri(ontoIri: OntoIRI): IRI = IRI.create(ontoIri.iri)
  implicit def fromOWLEntity(owlEntity: OWLEntity): OntoIRI = fromIri(owlEntity.getIRI)
  implicit def toString(ontoIRI: OntoIRI): String = ontoIRI.iri
  implicit val format: Format[OntoIRI] = Format[OntoIRI] (
    Reads { js =>
      val jsStr = js.asInstanceOf[JsString]
      val iri = IRI.create(jsStr.value)
      JsSuccess(OntoIRI(iri.toString))
    },
    Writes {
      case value => JsString(value.iri)
    }
  )
}

sealed class OntoEntityDto(val iri: OntoIRI) {
  def hasIRI(iri: String): Boolean = this.iri.hasIRI(iri)
}
case class OntoValue(override val iri: OntoIRI,
                     @JsonRawValue val value: String,
                     val inferred: Option[Boolean] = Some(false)) extends OntoEntityDto(iri)
case class OntoAnnotationDto(override val iri: OntoIRI) extends OntoEntityDto(iri)
case class OntoClassDto(override val iri: OntoIRI,
                        var subItems: Option[Set[OntoClassDto]] = None,
                        val annotations: Option[List[OntoValue]] = None) extends OntoEntityDto(iri)
case class OntoRelationDto(override val iri: OntoIRI,
                           var subItems: Option[Set[OntoRelationDto]],
                           val annotations: Option[List[OntoValue]] = None) extends OntoEntityDto(iri)
case class OntoDataPropDto(override val iri: OntoIRI,
                           var subItems: Option[Set[OntoDataPropDto]],
                           val annotations: Option[List[OntoValue]] = None) extends OntoEntityDto(iri)

case class OntoIndividualDto(override val iri: OntoIRI,
                             var annotations: List[OntoValue] = Nil,
                             var classes: List[OntoValue] = Nil,
                             var relations: List[OntoValue] = Nil,
                             var dataProperties: List[OntoValue] = Nil,
                             var blankNodes: List[OntoIndividualDto] = Nil) extends OntoEntityDto(iri) {

  private def getValue(items: List[OntoValue])(pred: OntoValue => Boolean): Option[OntoValue] = items.find(pred)

  def getAnnotationWith(pred: OntoValue => Boolean): Option[OntoValue] = getValue(annotations)(pred)
  def getOntoClassWith(pred: OntoValue => Boolean): Option[OntoValue] = getValue(classes)(pred)
  def getRelationWith(pred: OntoValue => Boolean): Option[OntoValue] = getValue(relations)(pred)
  def getDataPropertyWith(pred: OntoValue => Boolean): Option[OntoValue] = getValue(dataProperties)(pred)
}

object OntoValue {
  implicit val format: Format[OntoValue] = Json.format
}

object OntoAnnotationDto {
  implicit val format: Format[OntoAnnotationDto] = Json.format
}

object OntoClassDto {
  implicit val format: Format[OntoClassDto] = Json.format
}

object OntoRelationDto {
  implicit val format: Format[OntoRelationDto] = Json.format
}

object OntoDataPropDto {
  implicit val format: Format[OntoDataPropDto] = Json.format
}

object OntoIndividualDto {
  implicit val format: Format[OntoIndividualDto] = Json.format
  object Predicates {
    def hasIri(iri: OntoIRI)(el: OntoValue) = el.iri == iri
    def hasValue(value: String)(el: OntoValue) = el.value == value
    def hasValueRegexp(regExp: String)(el: OntoValue) = el.value.matches(regExp)
  }

  implicit class Searchable(ontoInd: OntoIndividualDto) {
    def getValueBy(sr: SearchResult): Option[OntoValue] = {
      if (ontoInd.hasIRI(sr.iri)) {
        if (sr.isBlankNodeValue()) {
          sr.blankNode.flatMap(srBn => {
            ontoInd.blankNodes
              .find(bn => bn.hasIRI(srBn.blankId.getOrElse("")))
              .flatMap(bn => bn.getDataPropertyWith(Predicates.hasIri(srBn.predicate)))
          })
        } else {
          ontoInd.getDataPropertyWith(Predicates.hasIri(sr.predicate))
        }
      } else {
        None
      }
    }

    def hasValueFor(sr: SearchResult): Boolean = {
      if (ontoInd.hasIRI(sr.iri)) {
        if (sr.isBlankNodeValue()) {
          ontoInd.relations
            .find(el => el.hasIRI(sr.predicate) && el.value == sr.blankNode.flatMap(_.blankId).getOrElse(""))
            .isDefined
        } else {
          ontoInd.relations
            .find(el => el.hasIRI(sr.predicate) && el.value == sr.value.getOrElse(""))
            .isDefined
        }
      } else {
        false
      }
    }
  }
}
