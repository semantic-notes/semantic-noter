package org.gardi.dto

import play.api.libs.json.{Format, Json}

sealed case class SearchParams(fields: Set[SearchParams.IRI], value: String)

object SearchParams {
  type IRI = String
  implicit val format: Format[SearchParams] = Json.format
}
