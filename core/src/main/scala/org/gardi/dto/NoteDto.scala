package org.gardi.dto

import play.api.libs.json._

case class CreateNoteDto(val text: String, val title: Option[String])

case class NoteDto(val str: String)

object NoteDto {
  implicit val format: Format[NoteDto] = Json.format
}

object CreateNoteDto {
  implicit val format: Format[CreateNoteDto] = Json.format
}
