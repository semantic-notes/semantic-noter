package org.gardi.dto

import play.api.libs.json.{Format, Json}

sealed case class SBlankNode(blankId: Option[String], predicate: String, value: String)

sealed case class SearchResult(iri: OntoIRI,
                               predicate: String,
                               value: Option[String],
                               blankNode: Option[SBlankNode] = None
                              ) {
  def isBlankNodeValue(): Boolean = value.isEmpty && blankNode.isDefined
}

object SearchResult {
  implicit val format: Format[SearchResult] = Json.format
}

object SBlankNode {
  implicit val format: Format[SBlankNode] = Json.format
}
