package org.gardi.dto

import play.api.libs.json.{Format, Json}

case class EntityQueryParams(hasClasses: Option[Set[OntoIRI]] = None,
                             hasRelations: Option[Set[OntoIRI]] = None,
                             searchParams: Option[SearchParams] = None)

object EntityQueryParams {
  implicit val format: Format[EntityQueryParams] = Json.format
}