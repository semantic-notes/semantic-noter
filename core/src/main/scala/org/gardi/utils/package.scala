package org.gardi

import java.net.InetAddress
import java.util.stream.Collectors

import com.lightbend.lagom.scaladsl.api.transport.NotFound
import com.softwaremill.id.DefaultIdGenerator
import com.softwaremill.id.pretty._
import org.apache.jena.ontology.OntModel
import org.apache.jena.rdf.model._
import org.apache.jena.shared.Lock
import org.apache.jena.vocabulary.{OWL2, RDF, ReasonerVocabulary}
import org.gardi.dto.{OntoValue, _}
import org.gardi.onto.interfaces.{IOntContainer, IOntoApiProvider, IOntoLoader}
import org.gardi.onto.{TQStatement, TResultStatements, TResultVariables, query}
import org.gardi.utils.jena.toOwlApi
import org.gardi.utils.sparql.BaseOntoIRIs
import org.semanticweb.owlapi.model._
import org.semanticweb.owlapi.model.parameters.Imports
import org.semanticweb.owlapi.reasoner.OWLReasoner
import org.semanticweb.owlapi.search.EntitySearcher
import org.semanticweb.owlapi.util.StringAnnotationVisitor
import ru.avicomp.ontapi.OntologyModel

import scala.collection.JavaConverters._
import scala.collection.mutable
import scala.collection.mutable.ListBuffer


package object utils {
  type OntoEntity = Map[Resource, RDFNode]

  implicit def owlEntityToString(entity: OWLEntity): String = entity.getIRI.toString

  private def getAddressAsLong(): Long = {
    val localhost: InetAddress = InetAddress.getLocalHost

    import java.nio.{ByteBuffer, ByteOrder}

    val buffer: ByteBuffer = ByteBuffer.allocate(java.lang.Long.BYTES).order(ByteOrder.BIG_ENDIAN)
    buffer.put(Array[Byte](0, 0, 0, 0))
    buffer.put(localhost.getAddress)
    buffer.position(0)
    buffer.getLong
  }

  var idGenerator: Option[StringIdGenerator] = None

  private val owlLiteralStringVisitor = new StringAnnotationVisitor {
    override def visit(literal: OWLLiteral): String = {
      val result = super.visit(literal)
      if (literal.hasLang) {
        s"${result}@${literal.getLang}"
      } else {
        result
      }
    }
  }

  def initIdGenerator(workerId: Int) = {
    val customPrettifier = IdPrettifier.custom(
      encoder = new AlphabetCodec(Alphabet.Base23),
      delimiter = '-')

    val generator =  if (workerId > 0)
      new PrettyIdGenerator(new DefaultIdGenerator(workerId, 1), customPrettifier)
    else
      PrettyIdGenerator.singleNode

    idGenerator = Some(generator)
  }

  def getNextId(): String = {
    idGenerator match {
      case Some(generator) => generator.nextId().toLowerCase
      case None => throw new Error("Id Generator wasn't initialized")
    }
  }

  def getOWLClass(className: String)(implicit ontoApi: IOntoApiProvider, ontoLoader: IOntoLoader): OWLClass = {
    ontoApi.owlDataFactory.getOWLClass(className, ontoApi.prefixManager())
  }

  def getOWLRelation(rel: String)(implicit ontoApi: IOntoApiProvider, ontoLoader: IOntoLoader): OWLObjectProperty = {
    ontoApi.owlDataFactory.getOWLObjectProperty(rel, ontoApi.prefixManager())
  }

  def getOWLDataProp(prop: String)(implicit ontoApi: IOntoApiProvider, ontoLoader: IOntoLoader): OWLDataProperty = {
    ontoApi.owlDataFactory.getOWLDataProperty(prop, ontoApi.prefixManager())
  }

  def getIndividualByShortName(name: String, ontCntr: IOntContainer): Option[OWLNamedIndividual] = {
    ontCntr.queryReasoner(query.getOwlIndividualByName(name).buildSPARQL()) match {
      case TResultVariables(resuls) => {
        val res = resuls.map(el => {
          val rdfNode = el("ind").result
          //ontCntr.owlDataFactory.getOWLNamedIndividual()
          toOwlApi.rdfNodeToOwlEntity(ontCntr)(rdfNode).asInstanceOf[OWLNamedIndividual]
        })
        if (res.isEmpty) { None } else { Some(res(0)) }
      }
    }
  }

  def getIndividualFrom(iri: String)(implicit ontoApi: IOntoApiProvider): OWLNamedIndividual = {
    ontoApi.owlDataFactory.getOWLNamedIndividual(IRI.create(iri))
  }

  def getAnnotations(ontoCntr: IOntContainer): List[OntoAnnotationDto] = {
    ontoCntr.ontology.getAnnotationPropertiesInSignature(true)
      .asScala
      .map(el => {
        val iri = prefixes.toPrefixedOwlIri(el.getIRI)(ontoCntr.ontoApi.prefixMapping())
        OntoAnnotationDto(iri)
      }).toList
  }

  def getAnnotations(entity: OWLEntity, ontCntr: IOntContainer): List[OntoValue] = {
    ontCntr.transact(Lock.READ) { ctr: OntologyModel =>
      ctr.importsClosure().iterator().asScala
        .flatMap(onto => EntitySearcher.getAnnotations(entity, onto).iterator().asScala)
        .map[OntoValue]((el: OWLAnnotation) => {
          val propertyIri = prefixes.toPrefixedOwlIri(el.getProperty.getIRI)(ontCntr.ontoApi.prefixMapping())
          val value = Option(el.getValue.asLiteral().get())
            .map[String]((el: OWLAnnotationValue) => el.accept[String](owlLiteralStringVisitor))
            .getOrElse("")
          OntoValue(propertyIri, value)
        })
        .toList
    }
  }

  def getSubClassFor(owlClass: OWLClass, ontoCntr: IOntContainer): OntoClassDto = {
    val subClasses = ontoCntr.queryReasoner(
      s""" SELECT ?x {
          ?x <${ReasonerVocabulary.directSubClassOf}> ${owlClass.getIRI.toQuotedString}
        } """) match {
      case TResultVariables(results) => {
        results
          .filter(el => el.contains("x"))
          .map(el => toOwlApi.rdfNodeToOwlEntity(ontoCntr)(el("x").result))
          .filter(el => el != ontoCntr.owlDataFactory.getOWLNothing)
          .map(el => {
            getSubClassFor(el.asInstanceOf[OWLClass], ontoCntr)
          }).toSet
      }
    }

    val annotations = getAnnotations(owlClass, ontoCntr)

    OntoClassDto(
      prefixes.toPrefixedOwlIri(owlClass.getIRI)(ontoCntr.ontoApi.prefixMapping()),
      if (subClasses.isEmpty) { None } else { Some(subClasses) },
      if (annotations.isEmpty) { None } else { Some(annotations) }
    )
  }

  def getSubClassFor(owlClass: OWLClass, owlReasoner: OWLReasoner)(implicit ontoApi: IOntoApiProvider): OntoClassDto = {
    val subClasses = owlReasoner.getSubClasses(owlClass, true)
      .entities()
      .iterator()
      .asScala
      .filter(el => el != ontoApi.owlDataFactory.getOWLNothing)
      .map[OntoClassDto]((el: OWLClass) => getSubClassFor(el, owlReasoner))
      .toSet

    OntoClassDto(
      owlClass,
      if (subClasses.isEmpty) { None } else { Some(subClasses) }
    )
  }

  def getSubClassFor(owlClass: OWLClass, jenaReasoner: OntModel)(implicit ontoApi: IOntoApiProvider): OntoClassDto = {
    val ontClass = jenaReasoner.getOntClass(owlClass)
    val subClasses = ontClass.listSubClasses().toSet.asScala
        .filter(el => el.getURI != ontoApi.owlDataFactory.getOWLNothing.getIRI.toString)
        .map((el) => getSubClassFor(ontoApi.owlDataFactory.getOWLClass(el.getURI), jenaReasoner))
        .toSet

    OntoClassDto(
      owlClass,
      if (subClasses.isEmpty) { None } else { Some(subClasses) }
    )
  }

  def getSubRelationsFor(owlRelation: OWLObjectProperty, ontoCntr: IOntContainer): OntoRelationDto = {
    val subRelations = ontoCntr.queryReasoner(
      s""" SELECT ?x {
          ?x <${ReasonerVocabulary.directSubPropertyOf}> ${owlRelation.getIRI.toQuotedString}
        } """) match {
      case TResultVariables(results) => {
        results
          .map(el => toOwlApi.rdfNodeToOwlEntity(ontoCntr)(el("x").result))
          .filter(el => el != ontoCntr.owlDataFactory.getOWLBottomObjectProperty)
          .map(el => {
            val rel = el.asInstanceOf[OWLObjectProperty]
            getSubRelationsFor(rel, ontoCntr)
          }).toSet
      }
    }

    val annotations = getAnnotations(owlRelation, ontoCntr)

    OntoRelationDto(
      prefixes.toPrefixedOwlIri(owlRelation.getIRI)(ontoCntr.ontoApi.prefixMapping()),
      if (subRelations.isEmpty) { None } else { Some(subRelations) },
      if (annotations.isEmpty) { None } else { Some(annotations) }
    )
  }

  def getSubRelationsFor(owlRelation: OWLObjectProperty, owlReasoner: OWLReasoner)(implicit ontoApi: IOntoApiProvider): OntoRelationDto = {
    val subRelations = owlReasoner.getSubObjectProperties(owlRelation, true)
      .entities()
      .iterator()
      .asScala
      .filter(el => el != ontoApi.owlDataFactory.getOWLBottomObjectProperty)
      .map[OntoRelationDto]((el) => getSubRelationsFor(el.getNamedProperty, owlReasoner))
      .toSet

    OntoRelationDto(owlRelation, if (subRelations.isEmpty) { None } else { Some(subRelations) })
  }

  def getSubRelationsFor(owlRelation: OWLObjectProperty, jenaReasoner: OntModel)(implicit ontoApi: IOntoApiProvider): OntoRelationDto = {
    val ontRel = jenaReasoner.getOntProperty(owlRelation)
    val subRelations = ontRel.listSubProperties(true)
      .asScala
      .filter(el => el.getURI != ontoApi.owlDataFactory.getOWLBottomObjectProperty.getIRI.toString && el != ontRel)
      .map((el) => getSubRelationsFor(ontoApi.owlDataFactory.getOWLObjectProperty(el.getURI), jenaReasoner))
      .toSet

    OntoRelationDto(owlRelation, if (subRelations.isEmpty) { None } else { Some(subRelations) })
  }

  def getSubDataPropFor(owlData: OWLDataProperty, ontoCntr: IOntContainer): OntoDataPropDto = {
    val subData = ontoCntr.queryReasoner(
      s""" SELECT ?x {
          ?x <${ReasonerVocabulary.directSubPropertyOf}> ${owlData.getIRI.toQuotedString}
        } """) match {
      case TResultVariables(results) => {
        results
          .map(el => toOwlApi.rdfNodeToOwlEntity(ontoCntr)(el("x").result))
          .filter(el => el != ontoCntr.owlDataFactory.getOWLBottomDataProperty)
          .map(el => {
            val dataRel = el.asInstanceOf[OWLDataProperty]
            getSubDataPropFor(dataRel, ontoCntr)
          }).toSet
      }
    }

    val annotations = getAnnotations(owlData, ontoCntr)

    OntoDataPropDto(
      prefixes.toPrefixedOwlIri(owlData.getIRI)(ontoCntr.ontoApi.prefixMapping()),
      if (subData.isEmpty) { None } else { Some(subData) },
      if (annotations.isEmpty) { None } else { Some(annotations) }
    )
  }

  private def statementsToOntoInd(ontoCntr: IOntContainer)(iri: String, stmts: List[TQStatement]): OntoIndividualDto = {
    type OntoValueTuple = (Statement, Boolean)

    def toOntoValue(el: OntoValueTuple): OntoValue = jena.Converters.toOntoValue(ontoCntr)(el._1, el._2)

    val values = mutable.Map.empty[String, ListBuffer[OntoValueTuple]]
    def addValue(key: String, value: OntoValueTuple) = {
      values.get(key) match {
        case Some(items) => items.append(value)
        case None => values += key -> (new ListBuffer[OntoValueTuple] += value)
      }
    }

    def mapValue(key: String) = values.get(key).map(el => el.map(toOntoValue).toList).getOrElse(Nil)

    stmts
      .filter(el => jena.Filters.filterByIri(iri)(el.result))
      .filter(el => jena.Filters.filterGenericRelation(ontoCntr)(el.result))
      .filter(el => jena.Filters.filterOwlResources(el.result))
      .foreach(el => {
        val st = el.result
        val iri = IRI.create(st.getPredicate.toString)
        val items = ontoCntr
          .ontology
          .entitiesInSignature(iri, Imports.EXCLUDED)
          .collect(Collectors.toList())
          .asScala

        items.headOption match {
          case Some(entity) => {
            addValue(entity.getEntityType.getName, (st, el.isInferred))
          }
          case None => {
            if (st.getPredicate == RDF.`type`) {
              addValue(EntityType.CLASS.getName, (st, el.isInferred))
            } else if (st.getPredicate == OWL2.sameAs) {
              addValue(EntityType.OBJECT_PROPERTY.getName, (st, el.isInferred))
            }
          }
        }
      })

    OntoIndividualDto(iri,
      annotations = mapValue(EntityType.ANNOTATION_PROPERTY.getName),
      classes = mapValue(EntityType.CLASS.getName),
      relations = mapValue(EntityType.OBJECT_PROPERTY.getName),
      dataProperties = mapValue(EntityType.DATA_PROPERTY.getName)
    )
  }

  def getOntoIndividual(owlInd: OWLNamedIndividual, ontoCntr: IOntContainer): OntoIndividualDto = {
    def expandPrefix(uri: String): String = utils.prefixes.expandPrefix(ontoCntr.ontoApi.prefixMapping())(uri)
    def stmtsToOntoInd(statements: List[TQStatement]): OntoIndividualDto = {
      val blankNodesIriMapping = Map(statements
        .filter(el => el.result.getPredicate.hasURI(expandPrefix(BaseOntoIRIs.SystemDataProps.hasBlankId)))
        .map(el => {
          val rdfSub = el.result.getSubject
          val rdfValue = el.result.getObject
          rdfSub.toString -> rdfValue.asLiteral().toString
        }): _*
      )
      val groupByIri = statements.groupBy(_.result.getSubject.toString)

      val statementsToInd = statementsToOntoInd(ontoCntr) _
      val targetIri = owlInd.getIRI.toString;
      val targetInd = statementsToInd(targetIri, groupByIri(targetIri))
      val blankNodes = groupByIri.filterKeys(key => key != targetIri)
        .map(item => {
          statementsToInd(item._1, item._2)
            .copy(iri = OntoIRI(iri = blankNodesIriMapping.get(item._1).getOrElse(item._1)))
        })

      targetInd.copy(
        relations = targetInd.relations
          .map(el => blankNodesIriMapping.get(el.value).map(value => el.copy(value = value)).getOrElse(el)),
        blankNodes = blankNodes.toList
      )
    }

    val queryStr = s"DESCRIBE ${owlInd.getIRI.toQuotedString}"
    val stmts = ontoCntr.queryReasoner(queryStr) match {
      case TResultVariables(resultsVar) => {
        resultsVar.map(el => {
          val sub = el("sub").result.asResource()
          val pred = ResourceFactory.createProperty(el("pred").result.asResource().getURI)
          val obj = el("obj").result
          val isInferred = el.values.map(_.isInferred).reduce((lha, rha) => lha || rha)
          TQStatement(isInferred, ResourceFactory.createStatement(sub, pred, obj))
        })
      }
      case TResultStatements(results) => results
    }
    stmtsToOntoInd(stmts)
  }

  def getIndividuals(ontoCntr: IOntContainer, detailed: Boolean = false): Set[OntoIndividualDto] = {
    ontoCntr.queryReasoner(s"SELECT ?ind {?ind rdf:type owl:NamedIndividual}") match {
      case TResultVariables(results) => {
        results
          .map(el => toOwlApi.rdfNodeToOwlEntity(ontoCntr)(el("ind").result))
          .map(el => {
            val ind = el.asInstanceOf[OWLNamedIndividual]
            if (detailed) {
              getOntoIndividual(ind.asOWLNamedIndividual(), ontoCntr)
            } else {
              OntoIndividualDto(ind.getIRI)
            }
          }).toSet
      }
    }
  }

  def createEntity(ontoCntr: IOntContainer, entity: OntoIndividualDto): OntoIndividualDto = {
    val entityIri = getNextId()
    val getIri = (iri: String) => s":${iri}"

    val entityData = sparql.prepareEntityToSave(entity.copy(iri = entityIri), getIri)
    ontoCntr.updateQuery(query.createEntity(entityIri, entityData), true)
    getIndividualByShortName(entityIri, ontoCntr) match {
      case Some(iri) => {
        getOntoIndividual(iri, ontoCntr)
      }
      case None => throw NotFound(s"${entityIri} was not fond")
    }
  }

  def replaceEntity(ontoCntr: IOntContainer, owlIri: OWLNamedIndividual, entity: OntoIndividualDto): OntoIndividualDto = {
    val entityData = sparql.prepareEntityToSave(entity)
    val entityIri = entity.iri.iri
    ontoCntr.updateQueries(query.replaceEntity(entityIri, entityData), true)
    getOntoIndividual(owlIri, ontoCntr)
  }

  def deleteEntity(ontoCntr: IOntContainer, entity: OntoIndividualDto): Boolean = {
    val entityIri = entity.iri.iri
    val query =
      s"""
         |DELETE {
         |  ?sub ?pre ?obj
         |  ?obj ?pre1 ?obj1
         |}
         |WHERE {
         |  ?sub ?pre ?obj .
         |  BIND( STR(?sub) AS ?string ).
         |  BIND( STR(?sub) AS ?string ).
         |  FILTER (?string=\"${entityIri}\") .
         |  OPTIONAL {
         |    ?o  ?p1  ?o1  .
         |    FILTER (isBlank(?o))
         |  }
         |}
         |""".stripMargin

    //ontoCntr.updateQuery(query)
    true
  }
}