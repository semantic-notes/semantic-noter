package org.gardi.utils

import org.apache.jena.query.QuerySolution
import org.apache.jena.rdf.model.{AnonId, Literal, RDFNode, RDFVisitor, Resource, Statement}
import org.apache.jena.vocabulary.{OWL2, RDF, RDFS, ReasonerVocabulary}
import org.gardi.dto.{OntoIRI, OntoValue}
import org.gardi.onto.TResultVariables
import org.gardi.onto.interfaces.IOntContainer
import org.semanticweb.owlapi.model.{IRI, OWLEntity, OWLObject}

import scala.collection.JavaConverters._

package object jena {
  object Filters {

    def filterByIri(iri: String)(el: Statement): Boolean = {
      if (el.getSubject.isAnon()) {
        el.getSubject.getId.toString == iri
      } else {
        el.getSubject.hasURI(iri)
      }
    }

    def filterOwlResources(el: Statement): Boolean = {
      if (el.getObject.isResource) {
        val resource = el.getObject.asResource().getURI
        !(resource == OWL2.NamedIndividual.toString
          || resource == OWL2.Thing.toString)
      } else {
        true
      }
    }

    def filterGenericRelation(ontoCntr: IOntContainer)(st: Statement): Boolean = {
      val pred = st.getPredicate
      val queryParams = {
        pred match {
          case OWL2.sameAs => None
          case OWL2.differentFrom => None
          case RDF.`type` => None
          case RDFS.label => None
          case _ => Some((ReasonerVocabulary.directSubPropertyOf, st.getPredicate.getURI))
        }
      }

      queryParams match {
        case Some(param) => {
          val bottomData = OntoIRI(ontoCntr.shortPrefix(OWL2.bottomDataProperty.getURI))
          val bottomRel = OntoIRI(ontoCntr.shortPrefix(OWL2.bottomObjectProperty.getURI))
          val shortPred = ontoCntr.shortPrefix(param._2)
          val subRel = ontoCntr.getSubRelationsFor(shortPred)
          subRel.exists(el => el == bottomData || el == bottomRel)
        }
        case None => true
      }
    }

  }

  object Converters {

    def statementsToEntity(statements: Stream[Statement]): OntoEntity = {
      statements.map(st => (st.getPredicate.asResource() -> st.getObject)).toMap
    }

    def querySolutionToEntity(qs: Stream[QuerySolution],
                              sub: String = "?sub",
                              pred: String = "?pred",
                              obj: String = "?obj"): OntoEntity = {
      qs.map(el => (el.getResource(pred) -> el.get(obj))).toMap
    }

    def querySolutionGrpBy(qs: Stream[QuerySolution],
                           grpBy: String = "?sub",
                           pred: String = "?pred",
                           obj: String = "?obj"): Map[Resource, OntoEntity] = {
      qs.groupBy(el => el.get(grpBy)).map(el => (el._1.asResource(), querySolutionToEntity(el._2, grpBy)))
    }

    def toOntoValue(ontCntr: IOntContainer)(statement: Statement, isInferred: Boolean): OntoValue = {
      val rdfSub = statement.getSubject
      val rdfPred = statement.getPredicate
      val rdfValue = statement.getObject
      OntoValue(
        prefixes.rdfToPrefixedString(rdfPred)(ontCntr),
        sparql.unEscapeStr(prefixes.rdfToPrefixedString(rdfValue)(ontCntr)),
        Some(isInferred)
      )
    }
  }

  object toOwlApi {
    def rdfNodeToOwlEntity(ontCntr: IOntContainer)(el: RDFNode): OWLObject = {
      val df = ontCntr.owlDataFactory()
      el.visitWith(new RDFVisitor {
        override def visitBlank(r: Resource, id: AnonId): OWLObject = {
          ontCntr.ontology
            .getEntitiesInSignature(IRI.create(r.getURI))
            .asScala
            .filter(el => !el.isOWLAnnotationProperty)
            .head
        }

        override def visitURI(r: Resource, uri: String): OWLObject = {
          ontCntr.ontology
            .getEntitiesInSignature(IRI.create(uri))
            .asScala
            .filter(el => !el.isOWLAnnotationProperty)
            .head
        }

        override def visitLiteral(l: Literal): OWLObject = {
          val datatype = l.getDatatype
          val literal = l.getString
          df.getOWLLiteral(literal, datatype.getURI)
        }
      }).asInstanceOf[OWLObject]
    }
  }
}
