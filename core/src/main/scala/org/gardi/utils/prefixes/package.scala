package org.gardi.utils

import org.apache.jena.rdf.model.{AnonId, Literal, RDFNode, RDFVisitor, Resource}
import org.apache.jena.shared.PrefixMapping
import org.gardi.onto.interfaces.IOntContainer
import org.semanticweb.owlapi.model.IRI

package object prefixes {
  private val iriPrefixVisitor = new RDFVisitor {
    var prefixMapping: Option[PrefixMapping] = None

    def setPrefixMapping(pm: PrefixMapping): Unit = {
      prefixMapping = Some(pm);
    }

    override def visitBlank(r: Resource, id: AnonId): AnyRef = {
      s"${id.toString}"
    }

    override def visitURI(r: Resource, uri: String): AnyRef = {
      prefixMapping.map(pm => pm.shortForm(uri)).getOrElse(uri)
    }

    override def visitLiteral(l: Literal): AnyRef = {
      prefixMapping.map(pm => {
        val node = l.asNode()
        val rawValue = node.toString(pm,false)
        val fullUri = l.getDatatypeURI
        rawValue.replace(fullUri, pm.shortForm(fullUri))
      }).getOrElse(l.asNode().getURI)
    }

    def apply[T](pm: PrefixMapping)(op: (RDFVisitor) => T):T = {
      setPrefixMapping(pm)
      val res = op(this)
      setPrefixMapping(null)
      res
    }
  }

  def expandPrefix(pm: PrefixMapping)(uri: String): String = pm.expandPrefix(uri)
  def toPrefixedOwlIri(iri: IRI)(implicit pm: PrefixMapping): String = pm.shortForm(iri.toString)

  def rdfToPrefixedString(node: RDFNode)(implicit ontoCnt: IOntContainer): String = {
    val pm = ontoCnt.ontoApi.prefixMapping()
    iriPrefixVisitor(pm)((mapper) => node.visitWith(mapper).asInstanceOf[String])
  }

}
