package org.gardi.onto.impl

import org.apache.jena.shared.PrefixMapping
import org.apache.jena.vocabulary.{DC_11, OWL, RDF, RDFS, XSD}
import org.gardi.onto.interfaces.IOntoApiProvider
import org.semanticweb.owlapi.model.{OWLDataFactory, PrefixManager}
import org.semanticweb.owlapi.util.DefaultPrefixManager
import ru.avicomp.ontapi.{OntManagers, OntologyManager}

class OntoApiProvider extends IOntoApiProvider {
  val _ontManager = OntManagers.createConcurrentONT()
  val _owlFactory = ontManager.getOWLDataFactory()
  val _prefixManager = new DefaultPrefixManager()
  val _prefixMapping = PrefixMapping.Factory.create

  override def ontManager(): OntologyManager = _ontManager
  override def owlDataFactory(): OWLDataFactory = _owlFactory
  override def prefixMapping(): PrefixMapping = _prefixMapping
  override def prefixManager(): PrefixManager = _prefixManager

  override def setNsPrefix(prefix: String, iri: String): IOntoApiProvider = {
    _prefixMapping.setNsPrefix(prefix, iri)
    _prefixManager.setPrefix(prefix, iri)
    this
  }

  setNsPrefix("rdfs", RDFS.getURI)
  setNsPrefix("rdf", RDF.getURI)
  setNsPrefix("owl", OWL.getURI)
  setNsPrefix("xsd", XSD.getURI)
  setNsPrefix("swrla", "http://swrl.stanford.edu/ontologies/3.3/swrla.owl#")
  setNsPrefix("text", "http://jena.apache.org/text#")
}
