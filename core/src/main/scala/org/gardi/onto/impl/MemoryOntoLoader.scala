package org.gardi.onto.impl

import java.io.{File, FileInputStream}

import com.gardi.shared.OntoConfig
import org.gardi.LagomData
import org.gardi.onto.interfaces.{IOntContainer, IOntoApiProvider, IOntoLoader}
import org.gardi.onto.OntoReplication
import org.semanticweb.owlapi.model.{AddImport, IRI}
import org.slf4j.{Logger, LoggerFactory}
import ru.avicomp.ontapi.OntologyModel

sealed case class MemoLoaderParams(ontoConfig: OntoConfig, lagomData: Option[LagomData] = None)

class MemoryOntoLoader(ontoApiProvider: IOntoApiProvider, params: MemoLoaderParams) extends IOntoLoader {

  private final val log: Logger = LoggerFactory.getLogger("MemoOntoLoader")

  private val ontoConfig = params.ontoConfig
  private def datasetName() = ontoConfig.datasetName
  private def baseOntoPath() = ontoConfig.baseOntoPath

  private val basePrefix = s"sparql:$datasetName"
  private val baseOntoName = s"$basePrefix/baseOntology"

  var baseOnto: Option[OntologyModel] = None

  private def initBaseOntology(): Unit = {
    val inputStream = new FileInputStream(new File(baseOntoPath))
    this.baseOnto = Some(ontoApiProvider.ontManager.loadOntologyFromOntologyDocument(inputStream))
    this.baseOnto.foreach(m => {
      ontoApiProvider.setNsPrefix("baseOntology", s"${m.getOntologyID.getOntologyIRI.get()}#")
    })
  }

  initBaseOntology()

  private def createOntology(name: String): OntologyModel = {
    log.info(s"writing to tdb: $name")
    val newOnto = ontoApiProvider.ontManager.createOntology(IRI.create(getGraphName(s"$name#")))
    val baseOntoIri = baseOnto match {
      case Some(onto) => {
        Option(onto.getOntologyID.getDefaultDocumentIRI.get()) match {
          case Some(iri) => iri
          case None => throw new RuntimeException("Base hasn't IRI")
        }
      }
      case None => throw new RuntimeException("BaseOnto is None")
    }
    val baseImport = ontoApiProvider.owlDataFactory.getOWLImportsDeclaration(baseOntoIri)
    ontoApiProvider.ontManager.applyChange(new AddImport(newOnto, baseImport))
    newOnto
  }

  override def getGraphName(name: String): String = s"$basePrefix/$name"

  override def hasLoadedOnto(name: String): Boolean = {
    val iri = IRI.create(getGraphName(name) + "#")
    ontoApiProvider.ontManager.contains(iri)
  }

  override def createOntoModelByName(name: String): OntologyModel = {
    val graphName = getGraphName(name)
    val iri = IRI.create(graphName + "#")
    if (ontoApiProvider.ontManager.contains(iri)) {
      ontoApiProvider.ontManager.getOntology(iri)
    } else {
      val model = createOntology(name)
      model
    }
  }

  override def initOntologyByName(name: String, withReasoner: Boolean): IOntContainer = {
    val graphName = getGraphName(name)
    val model = createOntoModelByName(name)
    new OntContainer(
      ontoApi = ontoApiProvider,
      ontoLoader = this,
      graphName = graphName,
      ontology = model,
      params.lagomData,
      connection = None,
      replication = params.lagomData match {
        case Some(data) => Some(new OntoReplication(data.actorSystem, data.kafkaBrokers, graphName, Some(data)))
        case None => None
      })
  }

  override def dispose(): Unit = {}
}
