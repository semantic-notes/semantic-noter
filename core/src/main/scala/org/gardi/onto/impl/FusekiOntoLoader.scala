package org.gardi.onto.impl

import java.io.{File, FileInputStream}
import java.util.function.Consumer

import com.gardi.shared.OntoConfig
import org.apache.http.auth.{AuthScope, UsernamePasswordCredentials}
import org.apache.http.impl.client.{BasicCredentialsProvider, HttpClients}
import org.apache.jena.atlas.web.HttpException
import org.apache.jena.ontology.OntDocumentManager
import org.apache.jena.rdf.model.Statement
import org.apache.jena.rdfconnection.RDFConnectionFuseki
import org.gardi.LagomData
import org.gardi.onto.interfaces.{IOntContainer, IOntoApiProvider, IOntoLoader}
import org.gardi.onto.OntoReplication
import org.semanticweb.owlapi.model.{AddImport, IRI}
import org.slf4j.{Logger, LoggerFactory}
import ru.avicomp.ontapi.OntologyModel

import scala.collection.JavaConversions._

sealed case class FusekiParams(ontoConfig: OntoConfig, lagomData: Option[LagomData] = None)

class FusekiOntoLoader(ontoApiProvider: IOntoApiProvider, params: FusekiParams) extends IOntoLoader {

  private final val log: Logger = LoggerFactory.getLogger("FusekiOntoLoader")

  var baseOnto: Option[OntologyModel] = None
  var fusekiConn: Option[RDFConnectionFuseki] = None

  private val ontoConfig = params.ontoConfig
  private def datasetName() = ontoConfig.datasetName
  private def fusekiUrl() = ontoConfig.fusekiUrl
  private def baseOntoPath() = ontoConfig.baseOntoPath

  private val basePrefix = s"sparql:$datasetName"
  private val baseOntoName = s"$basePrefix/baseOntology"

  private val credsProvider = new BasicCredentialsProvider
  private val credentials = new UsernamePasswordCredentials("admin", ontoConfig.fusekiPasswd)

  credsProvider.setCredentials(AuthScope.ANY, credentials)

  val httpclient = HttpClients.custom.setDefaultCredentialsProvider(credsProvider).build

  var builder = RDFConnectionFuseki.create()
    .httpClient(httpclient)
    .destination(fusekiUrl)
    .gspEndpoint(datasetName)
    .updateEndpoint(datasetName)
    .queryEndpoint(datasetName)

  fusekiConn = Some(builder.build().asInstanceOf[RDFConnectionFuseki])
  tryToInit()

  private def initBaseOntology(): Unit = {
    fusekiConn match {
      case None => throw new RuntimeException("Fuseki connection doesn't exist")
      case Some(conn) => {
        val inputStream = new FileInputStream(new File(baseOntoPath))
        this.baseOnto = Some(ontoApiProvider.ontManager.loadOntologyFromOntologyDocument(inputStream))
        this.baseOnto.foreach(onto => {
          conn.put(baseOntoName, onto.asGraphModel())
          ontoApiProvider.ontManager.removeOntology(onto)
          val loaded = ontoApiProvider.ontManager.addOntology(conn.fetch(baseOntoName).getGraph)
          val baseIRI = Option(loaded.getOntologyID.getOntologyIRI.get()) match {
            case Some(iri) => iri
            case None => throw  new RuntimeException("Base onto hasn't IRI")
          }
          ontoApiProvider.setNsPrefix("baseOntology", s"${baseIRI.toString}#")
          OntDocumentManager.getInstance().addModel(
            baseIRI.toString,
            loaded.asGraphModel()
          )
        })
      }
    }
  }

  private def tryToInit(): Unit = {
    initBaseOntology()
  }

  private def createOntology(name: String): OntologyModel = {
    log.info(s"writing to tdb: $name")
    val newOnto = ontoApiProvider.ontManager.createOntology(IRI.create(getGraphName(s"$name#")))
    val baseOntoIri = baseOnto match {
      case Some(onto) => {
        Option(onto.getOntologyID.getDefaultDocumentIRI.get()) match {
          case Some(iri) => iri
          case None => throw new RuntimeException("Base hasn't IRI")
        }
      }
      case None => throw new RuntimeException("BaseOnto is None")
    }
    val baseImport = ontoApiProvider.owlDataFactory.getOWLImportsDeclaration(baseOntoIri)
    ontoApiProvider.ontManager.applyChange(new AddImport(newOnto, baseImport))
    newOnto
  }

  override def getGraphName(name: String): String = s"$basePrefix/$name"

  override def hasLoadedOnto(name: String): Boolean = {
    val iri = IRI.create(getGraphName(name) + "#")
    ontoApiProvider.ontManager.contains(iri)
  }

  override def createOntoModelByName(name: String): OntologyModel = {
    val model = fusekiConn.map(conn => {
      val graphName = getGraphName(name)
      val iri = IRI.create(graphName + "#")
      try {
        if (ontoApiProvider.ontManager.contains(iri)) {
          ontoApiProvider.ontManager.getOntology(iri)
        } else {
          val model = conn.fetch(graphName)
          val ontModel = ontoApiProvider.ontManager.addOntology(model.getGraph)
          ontModel.asGraphModel().setNsPrefixes(ontoApiProvider.prefixMapping())
          ontModel
        }
      } catch {
        case err: HttpException if err.getResponseCode == 404 => {
          val onto = createOntology(name)
          conn.put(graphName, onto.asGraphModel().getBaseModel)
          onto
        }
      }
    })
    model match {
      case Some(x) => x
      case None => throw new RuntimeException(s"Can't create ontology ${name}")
    }
  }

  override def initOntologyByName(name: String, withReasoner: Boolean): IOntContainer = {
    val model = fusekiConn.map(conn => {
      val graphName = getGraphName(name)
      val model = createOntoModelByName(name)
      new OntContainer(
        ontoApi = ontoApiProvider,
        ontoLoader = this,
        graphName = graphName,
        ontology = model,
        params.lagomData,
        connection = Some(conn),
        replication = params.lagomData match {
          case Some(data) => Some(new OntoReplication(data.actorSystem, data.kafkaBrokers, graphName, Some(data)))
          case None => None
        })
    })
    model match {
      case Some(x) => x
      case None => throw new RuntimeException(s"Can't init ontology ${name}")
    }
  }

  override def dispose(): Unit = {
    fusekiConn.foreach(conn => conn.close())
  }
}
