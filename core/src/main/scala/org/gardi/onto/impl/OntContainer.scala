package org.gardi.onto.impl

import akka.Done
import org.apache.jena.query.{QueryExecutionFactory, QueryFactory}
import org.apache.jena.rdfconnection.RDFConnectionFuseki
import org.apache.jena.shared.{Lock, LockMRSW}
import org.apache.jena.update.UpdateAction
import org.apache.jena.vocabulary.{OWL2, ReasonerVocabulary}
import org.gardi.LagomData
import org.gardi.dto.{OntoIRI, SBlankNode, SearchResult}
import org.gardi.onto.TReasonerType.TReasonerType
import org.gardi.onto._
import org.gardi.onto.interfaces.{IOntContainer, IOntoApiProvider, IOntoLoader}
import org.gardi.onto.reasoners.{JenaOWLReasoner, OpenlletJena, OpenlletOwlApi}
import org.semanticweb.owlapi.model.OWLDataFactory
import org.slf4j.{Logger, LoggerFactory}
import ru.avicomp.ontapi.{OntologyManager, OntologyModel}
import rx.lang.scala.{Observable, Subscription}

import scala.collection.JavaConverters._
import scala.concurrent.Future

sealed case class TQuerySet(queryToServices: String, queryToStorage: String)

class OntContainer(val ontoApi: IOntoApiProvider,
                   private val ontoLoader: IOntoLoader,
                   private val graphName: String,
                   val ontology: OntologyModel,
                   private val lagomData: Option[LagomData] = None,
                   private val connection: Option[RDFConnectionFuseki] = None,
                   private val reasonerType: TReasonerType = TReasonerType.OpenlletJena,
                   private val replication: Option[OntoReplication] = None) extends IOntContainer {

  private final val log: Logger =
    LoggerFactory.getLogger(lagomData match {
      case Some(data) => s":${data.serviceName}:${classOf[OntContainer]}"
      case None => classOf[OntContainer].toString
    })


  private val lock = new LockMRSW()

  import akka.Done._
  private val listener = new RdfGraphListener(connection, ontology, graphName)
  ontology.asGraphModel().register(listener)

  val reasoner = reasonerType match {
    case TReasonerType.JenaOWL => Some(JenaOWLReasoner.getWrapper(ontology)(ontoApi, ontoLoader))
    case TReasonerType.OwlApi => Some(OpenlletOwlApi.getWrapper(ontology))
    case TReasonerType.OpenlletJena => Some(OpenlletJena.getWrapper(ontology)(ontoApi, ontoLoader))
    case TReasonerType.SWRLRuleEngine => None /*Some(SWRLRuleEngineReasoner.getWrapper(ontology))*/
    case TReasonerType.None => None
  }

  val cacheOfClasses = reasoner.map(_ => {
    val query =s"""SELECT ?x ?y {
                  |  ?x <${ReasonerVocabulary.directSubClassOf}> ?y .
                  |}
                  |""".stripMargin
    queryReasoner(query) match {
      case TResultVariables(results) => {
        results
          .groupBy(el => shortPrefix(el("y").result.asResource().getURI))
          .mapValues(items => items.map(el => shortPrefix(el("x").result.asResource().getURI)))
      }
    }
  })

  val cacheOfRelations = reasoner.map(_ => {
    val query =s"""SELECT ?x ?y {
                  |  ?x <${ReasonerVocabulary.directSubPropertyOf}> ?y .
                  |}
                  |""".stripMargin
    queryReasoner(query) match {
      case TResultVariables(results) => {
        results
          .groupBy(el => shortPrefix(el("y").result.asResource().getURI))
          .mapValues(items => items.map(el => shortPrefix(el("x").result.asResource().getURI)))
          .toMap
      }
    }
  })

  def getSubClassesFor(shortClassIri: String): List[OntoIRI] = {
    cacheOfClasses
      .map(items => items(shortClassIri).map(el => OntoIRI(el)))
      .getOrElse(Nil)
  }

  def getSubRelationsFor(shortRelIri: String): List[OntoIRI] = {
    cacheOfRelations
      .map(items => items(shortRelIri).map(el => OntoIRI(el)))
      .getOrElse(Nil)
  }

  val subs: List[Subscription] = replication match {
    case Some(replication) => {
      List(
        listener.changesAsSparql.subscribe(emitToReplication(replication) _),
        replication.changes.subscribe(applyReplication(this) _)
      )
    }
    case None => List()
  }


  def ontManager(): OntologyManager = ontoApi.ontManager()
  def owlDataFactory(): OWLDataFactory = ontoApi.owlDataFactory()

  def graphId(): String = {
    ontology.getOntologyID.getOntologyIRI.get().toString.replace("#", "")
  }

  def shortPrefix(iri: String): String = {
    ontoApi.prefixMapping().shortForm(iri)
  }

  def expandPrefix(iri: String): String = {
    ontoApi.prefixMapping().expandPrefix(iri)
  }

  private def emitToReplication(replication: OntoReplication)(changes: String): Unit = {
    replication.pushChanges(changes)
  }

  private def applyReplication(obj: OntContainer)(sparqlQuery: String): Unit = {
    try {
      silentTrans(Lock.WRITE) { model =>
        UpdateAction.parseExecute(sparqlQuery, model.asGraphModel)
      }
    } catch {
      case err: Throwable => {
        println(s"${err.printStackTrace()}")
      }
    }
  }

  def changes: Observable[RdfChange] = listener.changes

  def dispose(): Future[Done] = {
    try {
      reasoner match  {
        case Some(wrapper) => wrapper.dispose()
        case None => Future.successful(done())
      }
    } catch {
      case err: Throwable => log.error(err.getMessage)
    }
    try {
      replication match {
        case Some(repl) => repl.dispose()
        case None => Future.successful(done())
      }
    } catch {
      case err: Throwable => log.error(err.getMessage)
    }
    try {
      ontManager.removeOntology(ontology)
    } catch {
      case err: Throwable => log.error(err.getMessage)
    }

    Future.successful(done())
  }

  def refreshReasoner(): Unit = {
    reasoner match {
      case Some(wrapper) => wrapper.refresh()
      case None => ()
    }
  }

  def silentTrans[T](lock: Boolean = Lock.WRITE)(op: (OntologyModel) => T): T = {
    listener.disable()
    try {
      this.lock.enterCriticalSection(lock)
      op(ontology)
    } finally {
      listener.enable()
      this.lock.leaveCriticalSection()
      if (lock == Lock.WRITE) {
        refreshReasoner()
      }
    }
  }

  def transact[T](lockType: Boolean = Lock.WRITE)(op: (OntologyModel) => T): T = {
    try {
      lock.enterCriticalSection(lockType)
      listener.beginTxn()
      val res = op(ontology)
      if (lockType == Lock.WRITE) {
        refreshReasoner()
      }
      res
    } catch {
      case err: Throwable => {
        System.err.println(err)
        listener.cancelTxn()
        throw err
      }
    } finally {
      listener.commitTxn()
      lock.leaveCriticalSection()
    }
  }

  def wrapQueryWithPrefix(query: String): String = {
    val prefixes = ontoApi
      .prefixMapping()
      .getNsPrefixMap.asScala.toSeq
        .map(value => s"PREFIX ${value._1}: <${value._2}>")
        .mkString("\n")

    s"""
       |${prefixes}
       |PREFIX : ${ontology.getOntologyID.getOntologyIRI.get.toQuotedString}
       |${query}
       |""".stripMargin
  }

  private def prepareQuerySet(model: OntologyModel)(query: TQueryBuilder): TQuerySet = {
    val queryToServices = wrapQueryWithPrefix(query.buildSPARQL())
    val queryToStorage = wrapQueryWithPrefix(query.copy(graph = Some(graphId())).buildSPARQL())
    TQuerySet(queryToServices, queryToStorage)
  }

  def updateQueries(queries: Seq[TQueryBuilder], replyQuery: Boolean = false): Unit = {
    try {
      def applyInternalQuery(model: OntologyModel)(query: String): Unit = {
        UpdateAction.parseExecute(query, model.asGraphModel)
      }

      transact(Lock.WRITE) { model =>
        if (replyQuery) {
          try {
            listener.disable()
            queries.map(prepareQuerySet(model)(_)).foreach(querySet => applyInternalQuery(model)(querySet.queryToServices))
            queries.map(prepareQuerySet(model)(_)).foreach(querySet => listener.emitQuery(querySet.queryToServices, querySet.queryToStorage))
          } finally {
            listener.enable()
          }
        } else {
          queries.map(prepareQuerySet(model)(_)).foreach(querySet => applyInternalQuery(model)(querySet.queryToServices))
        }
      }
    } catch {
      case err: Throwable => {
        println(s"${err.printStackTrace()}")
      }
    }
  }

  def updateQuery(query: TQueryBuilder, replyQuery: Boolean = false): Unit = {
    try {
      transact(Lock.WRITE) { model =>
        val wrappedQuery = wrapQueryWithPrefix(query.buildSPARQL())
        if (replyQuery) {
          try {
            listener.disable()
            UpdateAction.parseExecute(wrappedQuery, model.asGraphModel)
            val querySet = prepareQuerySet(model)(query)
            listener.emitQuery(querySet.queryToServices, querySet.queryToStorage)
          } finally {
            listener.enable()
          }
        } else {
          UpdateAction.parseExecute(wrappedQuery, model.asGraphModel)
        }
      }
    } catch {
      case err: Throwable => {
        println(s"${err.printStackTrace()}")
      }
    }
  }

  def queryBaseModel(queryStr: String): TQueryResults = {
    transact(Lock.READ) { model =>
      val prefixedQuery = wrapQueryWithPrefix(queryStr)
      val arqQuery = QueryFactory.create(prefixedQuery)
      val exec = QueryExecutionFactory.create(arqQuery, model.asGraphModel())
      try {
        if (arqQuery.isDescribeType) {
          val execModel = exec.execDescribe()
          val statements = execModel.listStatements()
            .toList.asScala
            .map(st => {
              TQStatement(false, st)
            })
            .toList
          TResultStatements(statements)
        } else {
          TResultNone()
        }
      } finally {
        exec.close()
      }
    }
  }

  def queryReasoner(queryStr: String): TQueryResults = {
    transact(Lock.READ) { _ =>
      reasoner match {
        case Some(wrapper) => wrapper.execQuery(wrapQueryWithPrefix(queryStr))
      }
    }
  }

  def searchByFields(fields: Set[OntoIRI], value: String): Seq[SearchResult] = {
    connection
      .map(conn => {
        val queryStr = wrapQueryWithPrefix(
          query
            .searchByFields(fields, value)
            .copy(graph = Some(graphId()))
            .buildSPARQL()
        )
        val res = conn
          .query(queryStr)
          .execSelect()
          .asScala
          .map(qs => {
            val baseInd = Option(qs.get("base"))
            val targetInd = baseInd.getOrElse(qs.get("sub"))
            baseInd match {
              case Some(baseInd) => {
                SearchResult(
                  iri =targetInd.asResource().getURI,
                  predicate = shortPrefix(qs.get("basePred").asResource().getURI),
                  value = None,
                  blankNode = Some(SBlankNode(
                    blankId = Option(qs.get("blankId")).map(_.asLiteral().toString),
                    predicate = shortPrefix(qs.get("valuePred").asResource().getURI),
                    value = qs.get("obj").asLiteral().toString
                  ))
                )
              }
              case None => {
                SearchResult(
                  iri = targetInd.asResource().getURI,
                  predicate = shortPrefix(qs.get("valuePred").asResource().getURI),
                  value = Some(qs.get("obj").asLiteral().toString),
                  blankNode = None
                )
              }
            }
          })
          .toSeq
        res
      })
      .getOrElse(Nil)
  }
}
