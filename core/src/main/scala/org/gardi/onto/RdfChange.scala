package org.gardi.onto

import org.apache.jena.rdf.model.Statement

sealed trait RdfChange { val statement: Statement }
case class RdfInsert(statement: Statement) extends RdfChange
case class RdfDelete(statement: Statement) extends RdfChange