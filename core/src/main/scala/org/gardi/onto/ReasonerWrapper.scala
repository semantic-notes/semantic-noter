package org.gardi.onto

import akka.Done
import org.apache.jena.rdf.model.{RDFNode, Statement}
import org.gardi.onto.TReasonerType.TReasonerType
import ru.avicomp.ontapi.OntologyModel

import scala.concurrent.Future

object TReasonerType extends Enumeration {
  type TReasonerType = Value
  val JenaOWL, OpenlletJena, OwlApi, SWRLRuleEngine, None = Value
}

object TQueryResultType extends Enumeration {
  type TReasonerType = Value
  val Variables, Statements = Value
}

abstract class TQueryResult[T]( val isInferred: Boolean)
case class TQVariable(override val isInferred: Boolean, val result: RDFNode) extends  TQueryResult(isInferred = isInferred)
case class TQStatement(override val isInferred: Boolean, val result: Statement) extends TQueryResult(isInferred = isInferred)

abstract class TQueryResults
case class TResultNone() extends TQueryResults
case class TResultVariables(val results: List[Map[String, TQVariable]]) extends TQueryResults
case class TResultStatements(val results: List[TQStatement]) extends  TQueryResults

case class ReasonerWrapper[T](val rType: TReasonerType,
                              private val reasoner: T,
                              private val original: OntologyModel,
                              private val initializer: (T) => Future[Done],
                              private val refresher: (T) => Future[Done],
                              private val disposer: (T) => Future[Done],
                              private val queryExecutor: (ReasonerWrapper[T], T, String) => TQueryResults,
                              private val changesApplier: (ReasonerWrapper[T], T, String) => Unit) {
  initializer(reasoner)

  def refresh(): Future[Done] = {
    refresher(reasoner)
  }

  def action[RT](op: (T) => RT): RT = op(reasoner)

  def dispose(): Future[Done] = {
    disposer(reasoner)
  }

  def execQuery(query: String): TQueryResults = {
    try {
      queryExecutor(this, reasoner, query)
    }
    finally {}
  }

  def applyChanges(query: String): Unit = {
    try {
      changesApplier(this, reasoner, query)
    } finally {}
  }
}
