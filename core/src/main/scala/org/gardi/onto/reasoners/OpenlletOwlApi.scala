package org.gardi.onto.reasoners

import java.util.function.Consumer
import java.util.stream.Collectors

import akka.Done.done
import openllet.aterm.{ATerm, ATermAppl, ATermList}
import openllet.core.utils.TermFactory
import openllet.owlapi.{OpenlletReasoner, OpenlletReasonerFactory}
import openllet.query.sparqldl.engine.QueryEngine
import openllet.query.sparqldl.model.ResultBinding
import openllet.query.sparqldl.parser.{QueryEngineBuilder, QueryParser}
import org.apache.jena.ontology.OntModel
import org.gardi.onto.{ReasonerWrapper, TQVariable, TReasonerType, TResultVariables}
import org.semanticweb.owlapi.model.{IRI, OWLEntity}
import ru.avicomp.ontapi.OntologyModel

import scala.collection.mutable
import scala.collection.mutable.ListBuffer
import scala.concurrent.Future
import scala.collection.JavaConverters._

class OpenlletOwlApi {}

object OpenlletOwlApi {
  val _parser: QueryParser = QueryEngineBuilder.getParser()

  def getWrapper(ontology: OntologyModel): ReasonerWrapper[OpenlletReasoner] = {
    ReasonerWrapper[OpenlletReasoner](
      TReasonerType.OwlApi, OpenlletReasonerFactory.getInstance.createReasoner(ontology),
      original = ontology,
      initializer = (reasoner: OpenlletReasoner) => Future.successful(done()),
      refresher = (reasoner: OpenlletReasoner) => {
        reasoner.refresh()
        reasoner.flush
        Future.successful(done())
      },
      disposer = (reasoner: OpenlletReasoner) => {
        reasoner.dispose()
        Future.successful(done())
      },
      queryExecutor = (wrapper: ReasonerWrapper[OpenlletReasoner],reasoner: OpenlletReasoner, query: String) => {
        val result = _parser.parse(query, reasoner.getKB)
        type RowEl = Map[String, TQVariable]
        val listBuilder = new ListBuffer[RowEl]
        QueryEngine.exec(result).iterator().forEachRemaining(new Consumer[ResultBinding] {
          override def accept(el: ResultBinding): Unit = {
            val map = mutable.Map[String, TQVariable]()
            el.getAllVariables.forEach( new Consumer[ATermAppl] {
              override def accept(variable: ATermAppl): Unit = {
                variable.getArguments.forEach(new Consumer[ATerm] {
                  override def accept(elVar: ATerm): Unit = {
                    val stringVar = elVar.toString
                    val iri = IRI.create(el.getValue(TermFactory.`var`(stringVar)).toString)
                    val item = ontology.entitiesInSignature(iri).collect(Collectors.toList()).asScala(0)
                    // TODO: !!! to add handling for inferred relations
                    assert(false)
//                    map(stringVar) = TQVariable(false, item)
                  }
                })
              }
            })
            listBuilder.append(map.toMap)
          }
        })
        TResultVariables(listBuilder.toList)
      },
      changesApplier = (wrapper: ReasonerWrapper[OpenlletReasoner], model: OpenlletReasoner, query: String) => {

      }
    )
  }
}