package org.gardi.onto.reasoners

import akka.Done.done
import openllet.jena.PelletReasonerFactory
import openllet.jena.graph.query.GraphQueryHandler
import org.apache.jena.graph.Node
import org.apache.jena.ontology.OntModel
import org.apache.jena.query.{QueryExecutionFactory, QueryFactory}
import org.apache.jena.rdf.model.ModelFactory
import org.apache.jena.update.UpdateAction
import org.apache.jena.vocabulary.OWL
import org.gardi.onto._
import org.gardi.onto.interfaces.{IOntoApiProvider, IOntoLoader}
import ru.avicomp.ontapi.OntologyModel

import scala.collection.JavaConverters._
import scala.collection.mutable
import scala.collection.mutable.ListBuffer
import scala.concurrent.Future

class OpenlletJena {}

object OpenlletJena {

  def getWrapper(ontology: OntologyModel)(implicit ontoApi: IOntoApiProvider, ontoLoader: IOntoLoader): ReasonerWrapper[OntModel] = {

    val spec = PelletReasonerFactory.THE_SPEC
    ontoLoader.baseOnto match {
      case Some(onto) => {
        val baseIRI = Option(onto.getOntologyID.getOntologyIRI.get()) match {
            case Some(iri) => iri
            case None => throw new RuntimeException("Base onto hasn't IRI")
        }
        spec.getDocumentManager.addModel(baseIRI.toString, onto.asGraphModel())
      }
      case None => ()
    }

    import openllet.core.OpenlletOptions
    OpenlletOptions.PROCESS_JENA_UPDATES_INCREMENTALLY = true
    OpenlletOptions.USE_COMPLETION_QUEUE = true
    OpenlletOptions.USE_INCREMENTAL_CONSISTENCY = true
    OpenlletOptions.USE_SMART_RESTORE = false
    GraphQueryHandler.setBuiltinFilters(Set[Node](OWL.differentFrom.asNode(), OWL.sameAs.asNode()).asJava)

    val infModel = ModelFactory.createOntologyModel(spec, ontology.asGraphModel())
    import openllet.jena.PelletInfGraph
    val pelletJenaGraph = infModel.getGraph.asInstanceOf[PelletInfGraph]

    ReasonerWrapper[OntModel](
      TReasonerType.OpenlletJena, infModel,
      original = ontology,
      initializer = (reasoner: OntModel) => {
        reasoner.withDefaultMappings(ontoApi.prefixMapping())
        reasoner.setStrictMode(false)
        reasoner.prepare()
        Future.successful(done())
      },
      refresher = (reasoner: OntModel) => {
        reasoner.rebind()
        Future.successful(done())
      },
      disposer = (reasoner: OntModel) => {
        Future.successful(done())
      },
      queryExecutor = (wrapper: ReasonerWrapper[OntModel], model: OntModel, query: String) => {
        type RowEl = Map[String, TQVariable]
        val arqQuery = QueryFactory.create(query)
        val exec = QueryExecutionFactory.create(arqQuery, model)
        val listBuilder = new ListBuffer[RowEl]

        try {
          if (arqQuery.isSelectType) {
            val result = exec.execSelect().asScala

            result.foreach(el => {
              val map = mutable.Map[String, TQVariable]()
              el.varNames().asScala.foreach(elVar => {
                val stringVar = elVar
                val elNode = el.get(elVar)

                val isInferred = !model.isInBaseModel(elNode)
                map(stringVar) = TQVariable(isInferred, elNode)
              })
              listBuilder.append(map.toMap)
            })

            TResultVariables(listBuilder.toList)
          }
          else if (arqQuery.isDescribeType) {
            val execModel = exec.execDescribe()
            val statements = execModel.listStatements()
              .toList.asScala
              .map(st => {
                val isInferred = !model.isInBaseModel(st)
                TQStatement(isInferred, st)
              })
              .toList
            TResultStatements(statements)
          } else {
            TResultNone()
          }
        } finally {
          exec.close()
        }
      },
      changesApplier = (wrapper: ReasonerWrapper[OntModel], model: OntModel, query: String) => {}
    )
  }
}