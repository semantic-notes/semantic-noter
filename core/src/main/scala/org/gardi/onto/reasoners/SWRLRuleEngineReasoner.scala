package org.gardi.onto.reasoners

import akka.Done.done
import org.gardi.onto.{ReasonerWrapper, TReasonerType, TResultNone}
import org.swrlapi.core.SWRLRuleEngine
import org.swrlapi.factory.SWRLAPIFactory
import ru.avicomp.ontapi.OntologyModel

import scala.concurrent.Future

class SWRLRuleEngineReasoner {}

object SWRLRuleEngineReasoner {
  def getWrapper(ontology: OntologyModel): ReasonerWrapper[SWRLRuleEngine] = {
    ReasonerWrapper(
      TReasonerType.SWRLRuleEngine, SWRLAPIFactory.createSWRLRuleEngine(ontology),
      original = ontology,
      initializer = (r: SWRLRuleEngine) => { Future.successful(done()) },
      refresher = (r: SWRLRuleEngine) => { Future.successful(done()) },
      disposer = (r: SWRLRuleEngine) => { Future.successful(done())},
      queryExecutor = ( wrapper: ReasonerWrapper[SWRLRuleEngine],
                        r: SWRLRuleEngine,
                        query: String
                      ) => TResultNone(),
      changesApplier = (wrapper: ReasonerWrapper[SWRLRuleEngine], model: SWRLRuleEngine, query: String) => {

      }
    )
  }
}