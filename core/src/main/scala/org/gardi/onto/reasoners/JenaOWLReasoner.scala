package org.gardi.onto.reasoners

import akka.Done.done
import org.apache.jena.ontology.OntModel
import org.apache.jena.query.{QueryExecutionFactory, QueryFactory}
import org.gardi.onto.{ReasonerWrapper, TQStatement, TQVariable, TReasonerType, TResultNone, TResultStatements, TResultVariables}
import org.gardi.onto.interfaces.{IOntoApiProvider, IOntoLoader}
import ru.avicomp.ontapi.OntologyModel
import org.apache.jena.reasoner.Reasoner
import org.apache.jena.reasoner.ReasonerRegistry
import org.apache.jena.rdf.model.InfModel
import org.apache.jena.rdf.model.ModelFactory

import scala.collection.JavaConverters._
import scala.collection.mutable
import scala.collection.mutable.ListBuffer
import scala.concurrent.Future

class JenaOWLReasoner {

}

object JenaOWLReasoner {
  def getWrapper(ontology: OntologyModel)
                (implicit ontoApi: IOntoApiProvider, ontoLoader: IOntoLoader): ReasonerWrapper[InfModel] = {


    val reasonerObj = ReasonerRegistry.getOWLReasoner

    val infModel = ModelFactory.createInfModel(reasonerObj, ontology.asGraphModel())

    ReasonerWrapper[InfModel](
      TReasonerType.JenaOWL, infModel,
      original = ontology,
      initializer = (reasoner: InfModel) => {
        reasoner.withDefaultMappings(ontoApi.prefixMapping())
        //reasoner.setStrictMode(false)
        reasoner.prepare()
        Future.successful(done())
      },
      refresher = (reasoner: InfModel) => {
        reasoner.rebind()
        Future.successful(done())
      },
      disposer = (reasoner: InfModel) => {
        Future.successful(done())
      },
      queryExecutor = (wrapper: ReasonerWrapper[InfModel], model: InfModel, query: String) => {
        type RowEl = Map[String, TQVariable]
        val arqQuery = QueryFactory.create(query)
        val exec = QueryExecutionFactory.create(arqQuery, model)
        val listBuilder = new ListBuffer[RowEl]

        try {
          if (arqQuery.isSelectType) {
            val result = exec.execSelect().asScala

            result.foreach(el => {
              val map = mutable.Map[String, TQVariable]()
              el.varNames().asScala.foreach(elVar => {
                val stringVar = elVar
                val elNode = el.get(elVar)

                //val isInferred = !model.isInBaseModel(elNode)
                val isInferred = !model.containsResource(elNode)
                map(stringVar) = TQVariable(isInferred, elNode)
              })
              listBuilder.append(map.toMap)
            })

            TResultVariables(listBuilder.toList)
          }
          else if (arqQuery.isDescribeType) {
            val execModel = exec.execDescribe()
            val statements = execModel.listStatements()
              .toList.asScala
              .map(st => {
                val isInferred = !model.contains(st)//!model.isInBaseModel(st)
                TQStatement(isInferred, st)
              })
              .toList
            TResultStatements(statements)
          } else {
            TResultNone()
          }
        } finally {
          exec.close()
        }
      },
      changesApplier = (wrapper: ReasonerWrapper[InfModel], model: InfModel, query: String) => {}
    )
  }
}
