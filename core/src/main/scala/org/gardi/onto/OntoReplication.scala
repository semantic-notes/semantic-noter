package org.gardi.onto

import akka.Done
import akka.actor.ActorSystem
import akka.kafka.scaladsl.Consumer.DrainingControl
import akka.kafka.scaladsl.{Consumer, Producer}
import akka.kafka.{ConsumerSettings, ProducerMessage, ProducerSettings, Subscriptions}
import akka.stream.scaladsl.{Keep, Sink, Source}
import akka.stream.{ActorMaterializer, OverflowStrategy}
import org.apache.kafka.clients.consumer.ConsumerConfig
import org.apache.kafka.clients.producer.{ProducerConfig, ProducerRecord}
import org.apache.kafka.common.TopicPartition
import org.apache.kafka.common.serialization.{StringDeserializer, StringSerializer}
import org.gardi.LagomData
import play.api.libs.json.{Format, JsError, JsSuccess, Json}
import rx.lang.scala.subjects.PublishSubject
import org.slf4j.{Logger, LoggerFactory}

import scala.concurrent.duration._
import scala.concurrent.{Await, ExecutionContextExecutor, Future}
import scala.util.Success

case class ReplicationData(val uuid: String, val sparqlChange: String)

object ReplicationData {
  implicit val format: Format[ReplicationData] = Json.format
}

class OntoReplication(val actorSystem: ActorSystem,
                      val brokers: String,
                      val ontoName: String,
                      val lagomData: Option[LagomData] = None
                     ) {
  val topic = ontoName.replaceAll("(:|\\/)", "-")
  val uuid = lagomData match {
    case Some(data) => data.serviceName
    case None => java.util.UUID.randomUUID.toString
  }

  val threadId = Thread.currentThread().getId
  val ontoUuid = ontoName.replaceAll(":|\\/", "-")

  val log: Logger = LoggerFactory.getLogger(s"${classOf[OntoReplication]}.${uuid}.${ontoUuid}-${threadId}")

  val consumerUuid = s"${uuid}:${java.util.UUID.randomUUID.toString}"
  val producerUuid = s"onto-producer-${uuid}-${ontoUuid}-thread-${threadId}"

  private implicit val system = actorSystem
  private implicit val materializer = ActorMaterializer()

  implicit val ec: ExecutionContextExecutor = system.dispatcher

  val now = System.currentTimeMillis
  val messagesSince: Long = now

  val changes = PublishSubject[String]()

  private val producerSettings =
    ProducerSettings(system, new StringSerializer, new StringSerializer)
      .withBootstrapServers(brokers)
      .withProperty(ProducerConfig.CLIENT_ID_CONFIG, producerUuid)

  private val consumerSettings =
    ConsumerSettings(system, new StringDeserializer, new StringDeserializer)
      .withBootstrapServers(brokers)
      .withGroupId(s"$topic-group-${consumerUuid}")
      .withClientId(s"onto-consumer-${consumerUuid}")
      .withProperty(ConsumerConfig.ENABLE_AUTO_COMMIT_CONFIG, "true")
      .withProperty(ConsumerConfig.AUTO_OFFSET_RESET_CONFIG, "earliest")

  val subscription = Subscriptions.assignmentOffsetsForTimes(new TopicPartition(topic, 0) -> messagesSince)

  val control =
    Consumer
      .plainSource(consumerSettings, subscription)
      .filter(msg => msg.timestamp() >= now)
      .map(msg => {
        val el = msg
        val jsValue = Json.parse(el.value())
        val replicationData = ReplicationData.format.reads(jsValue)
        replicationData match {
          case JsSuccess(value, path) => {
            if (value.uuid != uuid && value.sparqlChange.length > 0) {
              changes.onNext(value.sparqlChange)
            }
          }
          case JsError(errors) => println(errors)
        }
        msg
      })
      .toMat(Sink.ignore)(Keep.both)
      .mapMaterializedValue(DrainingControl.apply)
      .run()

  private val ontoChangesProducer = Source.queue[String](100, OverflowStrategy.backpressure)
    .map { elem =>
      val jsValue = ReplicationData.format.writes(new ReplicationData(uuid, elem))
      ProducerMessage.single(
        new ProducerRecord[String, String](topic, jsValue.toString())
      )
    }
    .via(Producer.flexiFlow(producerSettings))
    .toMat(Sink.ignore)(Keep.left)
    .run()

  def pushChanges(changes: String): Unit = {
    ontoChangesProducer.offer(changes)
  }

  def dispose(): Future[Done] = {
    log.debug(s"Dispose replication ${producerUuid}")
    import akka.Done._
    val wait = control.stop().andThen {case Success(x) => control.shutdown() }
    ontoChangesProducer.complete()
    Await.result(wait, Duration(100, MILLISECONDS))
    Future.successful(done())
  }

}
