package org.gardi.onto

import java.text.SimpleDateFormat
import java.util.{Calendar, Date, TimeZone}

import org.gardi.utils
import org.gardi.dto.{CreateNoteDto, OntoIRI}
import org.gardi.utils.sparql.BaseOntoIRIs

sealed case class TQueryBuilder(action: String,
                                body: String,
                                where: Option[String] = None,
                                graph: Option[String] = None,
                                limit: Option[Integer] = None,
                                queryGenerator: Option[TQueryBuilder => String] = None)
{
  def buildSPARQL(): String = {
    queryGenerator match {
      case None => {
        if (TQueryBuilder.WithMarker.contains(action)) {
          s"""${graph.map(id => s"WITH <${id}> ").getOrElse("")}${action} {
             |  ${body}
             |}
             |${where.map(qr => s"WHERE { ${qr} }").getOrElse("")}
             |${limit.map(lim => s"LIMIT $lim").getOrElse("")}
             |""".stripMargin
        } else {
          s"""${action} {
             |  ${graph.map(id => s"GRAPH <$id> { ${body} }").getOrElse(body)}
             |}
             |${where.map(qr => s"WHERE { ${qr} }").getOrElse("")}
             |${limit.map(lim => s"LIMIT $lim").getOrElse("")}
             |""".stripMargin
        }
      }
      case Some(gen) => gen(this)
    }
  }
}

object TQueryBuilder {
  val WithMarker = Set("DELETE", "INSERT")
}

class TCustomQuery(genQuery: (TQueryBuilder) => String) extends TQueryBuilder("", "") {
  override def buildSPARQL(): String = genQuery(this)
}

package object query {
  private val timestampFormatter = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSS'Z'")
  timestampFormatter.setTimeZone(TimeZone.getTimeZone("UTC"))

  def toXsdTimeStamp(timestamp: Date): String = s"""\"${timestampFormatter.format(timestamp)}\"^^xsd:dateTimeStamp"""

  def removeBlankNodesFor(entityIri: String): TQueryBuilder = {
    TQueryBuilder(
      action = "DELETE",
      body =
        """
          |?sub ?pre ?bn .
          |?bn ?pre2 ?obj2 .
          |""".stripMargin,
      where = Some(
        s"""
           |?sub ?pre ?bn .
           |BIND( STR(?sub) AS ?string ) .
           |FILTER (?string=\"${entityIri}\")
           |?bn ?pre2 ?obj2 FILTER (isBlank(?bn)).
          |""".stripMargin)

    )
  }

  def cleanEntitySiblingData(entityIri: String): TQueryBuilder = {
    TQueryBuilder(
      action = "DELETE",
      body = "?sub ?pre ?obj .",
      where = Some(
        s"""
          |?sub ?pre ?obj .
          |BIND( STR(?sub) AS ?string ).
          |FILTER (?string=\"${entityIri}\" && ?obj != owl:NamedIndividual) .
          |FILTER (?string=\"${entityIri}\" && ?pre != ${BaseOntoIRIs.SystemDataProps.wasCreated}) .
          |""".stripMargin)
    )
  }

  def insertUpdatedData(entityIri: String, data: Map[String, String]): TQueryBuilder = {
    val timestamp = Calendar.getInstance().getTime()
    val classes = data("classes")
    val relations = data("relations")
    val dataProps = data("dataProps")
    val annotations = data("annotations")
    TQueryBuilder(
      action = "INSERT DATA",
      body =
        s"""<${entityIri}> ${BaseOntoIRIs.SystemDataProps.wasUpdated} ${toXsdTimeStamp(timestamp)} .
           |${classes}
           |${relations}
           |${dataProps}
           |${annotations}
           |""".stripMargin
    )
  }

  def createEntity(entityIri: String, data: Map[String, String]): TQueryBuilder = {
    val timestamp = Calendar.getInstance().getTime()
    val classes = data("classes")
    val relations = data("relations")
    val dataProps = data("dataProps")
    val annotations = data("annotations")

    TQueryBuilder(
      action = "INSERT DATA",
      body =
      s""":${entityIri} rdf:type owl:NamedIndividual .
         |:${entityIri} ${BaseOntoIRIs.SystemDataProps.wasCreated} ${toXsdTimeStamp(timestamp)} .
         |:${entityIri} ${BaseOntoIRIs.SystemDataProps.wasUpdated} ${toXsdTimeStamp(timestamp)} .
         |${classes}
         |${relations}
         |${dataProps}
         |${annotations}
         |""".stripMargin
    )
  }

  def replaceEntity(entityIri: String, data: Map[String, String]): List[TQueryBuilder] = {
    List(
      removeBlankNodesFor(entityIri),
      cleanEntitySiblingData(entityIri),
      insertUpdatedData(entityIri, data)
    )
  }

  def getOwlIndividualByName(name: String): TQueryBuilder = {
    TQueryBuilder(
      action = "SELECT DISTINCT ?ind",
      body =
        s"""
           |?ind rdf:type owl:NamedIndividual .
           |BIND( STR(?ind) AS ?string ) .
           |FILTER CONTAINS(?string, "#${name}") .
           |""".stripMargin,
      limit = Some(1)
    )
  }

  def searchByFields(fields: Set[OntoIRI], value: String): TQueryBuilder = {
    def genQueryForField(field: OntoIRI): String = {
      s"""{ (?sub ?pred ?obj ?graph) text:query (${field.iri} "${utils.sparql.escapeStr(value)}") . }"""
    }
    val whereBody =
      s"""${fields.map(genQueryForField).mkString(s"\n|   union\n|    ")}
         |optional {
         |  ?base ?basePred ?sub FILTER (isBlank(?sub)) .
         |}
         |optional {
         |  ?sub ?valuePred ?obj .
         |}
         |optional {
         |  ?sub baseOntology:hasBlankId ?blankId .
         |}
         |""".stripMargin

    val genQuery = (qb: TQueryBuilder) =>
      s"""
         |SELECT ?sub ?pred ?obj ?graph ?base ?basePred ?valuePred ?blankId
         |WHERE {
         |${qb.graph.map(graphId => s"graph <$graphId> { ${whereBody} }").getOrElse(whereBody)}
         |}
         |""".stripMargin
    TQueryBuilder("", "", queryGenerator = Some(genQuery))
  }

  def describeEntityQuery(entityIri: String): TQueryBuilder = {
    val genQuery = (qb: TQueryBuilder) => {
      s"""
         |SELECT DISTINCT ?sub ?pred ?obj WHERE {
         |  {
         |    ?sub ?pred ?obj .
         |    FILTER (?sub = <${entityIri}>) .
         |  }
         |    UNION
         |  {
         |    ?sub ?pred ?obj .
         |    ?sub2 ?pred2 ?sub .
         |    FILTER (?sub2 = <${entityIri}>) .
         |    FILTER(isBlank(?sub)) .
         |  }
         |}
         |""".stripMargin
    }
    TQueryBuilder("", "", queryGenerator = Some(genQuery))
  }

  def addNote(nodeUri: String, newNote: CreateNoteDto): TQueryBuilder = {
    TQueryBuilder(
      action = "INSERT DATA",
      body =
        s"""
           |:${nodeUri} rdf:type owl:NamedIndividual .
           |:${nodeUri} rdf:type baseOntology:InfoItem .
           |:${nodeUri} baseOntology:isUnhandled :${nodeUri} .
           |:${nodeUri} baseOntology:hasText "${utils.sparql.escapeStr(newNote.text)}" .
           |${newNote.title
              .map(title => s""":${nodeUri} baseOntology:hasTitle \"${utils.sparql.escapeStr(title)}\" .""")
              .getOrElse("")
             }
           """.stripMargin
    )
  }
}
