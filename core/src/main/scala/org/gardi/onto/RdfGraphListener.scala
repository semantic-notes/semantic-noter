package org.gardi.onto

import java.util
import java.util.function.Consumer

import org.apache.jena.arq.querybuilder.UpdateBuilder
import org.apache.jena.graph.NodeFactory
import org.apache.jena.query.ReadWrite
import org.apache.jena.rdf.model.{Model, ModelChangedListener, Statement, StmtIterator}
import org.apache.jena.rdfconnection.RDFConnectionFuseki
import org.apache.jena.sparql.core.Quad
import org.apache.jena.sparql.modify.request.{QuadDataAcc, UpdateData, UpdateDataDelete, UpdateDataInsert}
import org.apache.jena.system.Txn
import org.slf4j.{Logger, LoggerFactory}
import ru.avicomp.ontapi.OntologyModel
import rx.lang.scala.subjects.PublishSubject

import scala.concurrent.Future
import scala.concurrent.ExecutionContext.Implicits.global

class RdfGraphListener(private val connection: Option[RDFConnectionFuseki],
                       private val model: OntologyModel,
                       private val graphName: String) extends ModelChangedListener {

  private val log: Logger = LoggerFactory.getLogger(s"${classOf[RdfGraphListener]}.${graphName}")
  private var _disabled: Boolean = false
  private var isTxn: Boolean = false
  private var txnChanges = new scala.collection.mutable.MutableList[RdfChange]()

  def disable(): Unit = _disabled = true
  def enable(): Unit = _disabled = false

  val changes = PublishSubject[RdfChange]()
  val changesAsSparql = PublishSubject[String]()

  private def inTxn(): Boolean = {
    this.isTxn.synchronized {
      this.isTxn
    }
  }

  private def setTxn(value: Boolean): Unit = {
    this.isTxn.synchronized {
      this.isTxn = value
    }
  }

  def beginTxn(): Unit = {
    setTxn(true)
    txnChanges.synchronized {
      txnChanges.clear()
    }
  }

  def commitTxn(): Unit = {
    txnChanges.synchronized {
      if (!txnChanges.isEmpty) {
        handleRdfChanges(txnChanges)
        changesAsSparql.onNext(changesToLocalSparql(txnChanges))
        txnChanges.clear()
      }
    }
    setTxn(false)
  }

  def cancelTxn(): Unit = {
    txnChanges.synchronized {
      txnChanges.clear()
    }
    setTxn(false)
  }

  private def addChangeToTxn(change: RdfChange): Unit = {
    txnChanges.synchronized {
      txnChanges += change
    }
  }

  private def changesToLocalSparql(items: Seq[RdfChange]): String = {
    val grp = items.groupBy( el => el match {
      case el: RdfInsert => "insert"
      case el: RdfDelete => "delete"
    })

    val grpChanges = List("delete" -> grp.get("delete"), "insert" -> grp.get("insert")).map((el) => {
      el._1 match {
        case "delete" => {
          if (el._2.isDefined) {
            val builder = new UpdateBuilder()
            val changes = el._2.getOrElse(Nil)
            changes.foreach(change => builder.addDelete(change.statement.asTriple()))
            builder.build().toString()
          } else {
            ""
          }

        }
        case "insert" => {
          if (el._2.isDefined) {
            val builder = new UpdateBuilder()
            val changes = el._2.getOrElse(Nil)
            changes.foreach(change => builder.addInsert(change.statement.asTriple()))
            builder.build().toString()
          } else {
            ""
          }

        }
      }
    })

    grpChanges.toStream.filter(el => !el.isEmpty).mkString("; \n")
  }

  private def handleRdfChanges(items: Seq[RdfChange]): Unit = {
    val grp = items.groupBy( el => el match {
      case el: RdfInsert => "insert"
      case el: RdfDelete => "delete"
    })

    val updates = scala.collection.mutable.MutableList[UpdateData]()

    grp get "delete" match {
      case Some(toDelete) => {
        updates += new UpdateDataDelete(collectStatementsAsQuads(toDelete))
      }
      case None => ()
    }

    grp get "insert" match {
      case Some(toInsert) => {
        updates += new UpdateDataInsert(collectStatementsAsQuads(toInsert))
      }
      case None => ()
    }

    val sparqlForRemoteStr = updates.map(_.toString).toStream.mkString(";\n")

    connection.foreach(conn => {
      Future {
        Txn.executeWrite(conn, new Runnable {
          override def run(): Unit = conn.update(sparqlForRemoteStr)
        })
      }
    })
  }


  def collectStatementsAsTriples(rdfChanges: Seq[RdfChange]): QuadDataAcc = {
    val id = NodeFactory.createURI(graphName)
    rdfChanges
      .map(_.statement.asTriple)
      .aggregate(new QuadDataAcc())((acc, el) => {
        acc.addTriple(el)
        acc
      }, (a, b) => b)
  }

  def collectStatementsAsQuads(rdfChanges: Seq[RdfChange]): QuadDataAcc = {
    val id = NodeFactory.createURI(graphName)
    rdfChanges
      .map(_.statement.asTriple)
      .aggregate(new QuadDataAcc())((acc, el) => {
        acc.addQuad(Quad.create(id, el))
        acc
      }, (a, b) => b)
  }

  def emitInsert(st: Statement): Unit = {
    if (!_disabled) {
      val change = RdfInsert(st)
      if (inTxn) {
       addChangeToTxn(change)
      }
      changes.onNext(change)
    } else ()
  }

  def emitQuery(queryToServices: String, queryToStorage: String): Unit = {
    changesAsSparql.onNext(queryToServices)
    connection.foreach(conn => {
      Txn.executeWrite(conn, new Runnable {
        override def run(): Unit = {
          conn.update(queryToStorage)
        }
      })
    })
  }

  def emitDelete(st: Statement): Unit = {
    if (!_disabled) {
      val change = RdfDelete(st)
      if (inTxn) {
        addChangeToTxn(change)
      }
      changes.onNext(change)
    } else ()
  }

  override def addedStatement(st: Statement): Unit = emitInsert(st)

  override def addedStatements(statements: Array[Statement]): Unit = statements.foreach(emitInsert)

  override def addedStatements(statements: util.List[Statement]): Unit = {
    statements.forEach(new Consumer[Statement] {
      override def accept(t: Statement): Unit = emitInsert(t)
    })
  }

  override def addedStatements(statements: StmtIterator): Unit = {
    statements.forEachRemaining(new Consumer[Statement] {
      override def accept(t: Statement): Unit = emitInsert(t)
    })
  }

  override def addedStatements(m: Model): Unit = {}

  override def removedStatement(st: Statement): Unit = emitDelete(st)

  override def removedStatements(statements: Array[Statement]): Unit = statements.foreach(emitDelete)

  override def removedStatements(statements: util.List[Statement]): Unit = {
    statements.forEach(new Consumer[Statement] {
      override def accept(t: Statement): Unit = emitDelete(t)
    })
  }

  override def removedStatements(statements: StmtIterator): Unit = {
    statements.forEachRemaining(new Consumer[Statement] {
      override def accept(t: Statement): Unit = emitDelete(t)
    })
  }

  override def removedStatements(m: Model): Unit = {}

  override def notifyEvent(m: Model, event: Any): Unit = {}
}
