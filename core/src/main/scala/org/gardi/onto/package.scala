package org.gardi

import akka.actor.ActorSystem
import org.slf4j.{Logger, LoggerFactory}

case class LagomData(val actorSystem: ActorSystem,
                     val serviceName: String,
                     val kafkaBrokers: String)

package object onto {

  private final val log: Logger =
    LoggerFactory.getLogger("OntoPkg")

  val baseOntoNtPath = "./data/ontology/semantic-noter-base-nt.owl"
  val baseOntoFsPath = "./data/ontology/semantic-noter-base-fs.owl"

}
