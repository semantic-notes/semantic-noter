package org.gardi.onto.interfaces

import ru.avicomp.ontapi.OntologyModel

abstract class IOntoLoader {
  def baseOnto(): Option[OntologyModel]
  def getGraphName(name: String): String
  def hasLoadedOnto(name: String): Boolean
  def createOntoModelByName(name: String): OntologyModel
  def initOntologyByName(name: String, withReasoner: Boolean = true): IOntContainer
  def dispose(): Unit
}
