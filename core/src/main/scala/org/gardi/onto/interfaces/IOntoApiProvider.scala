package org.gardi.onto.interfaces

import org.apache.jena.shared.PrefixMapping
import org.semanticweb.owlapi.model.{OWLDataFactory, PrefixManager}
import ru.avicomp.ontapi.OntologyManager

abstract  class IOntoApiProvider {
  def ontManager(): OntologyManager
  def owlDataFactory(): OWLDataFactory
  def prefixMapping(): PrefixMapping
  def prefixManager(): PrefixManager

  def setNsPrefix(prefix: String, iri: String): IOntoApiProvider
}
