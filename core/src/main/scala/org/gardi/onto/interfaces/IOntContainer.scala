package org.gardi.onto.interfaces

import akka.Done
import org.apache.jena.shared.Lock
import org.gardi.dto.{OntoIRI, SearchResult}
import org.gardi.onto.{RdfChange, TQueryBuilder, TQueryResults}
import org.semanticweb.owlapi.model.OWLDataFactory
import ru.avicomp.ontapi.{OntologyManager, OntologyModel}
import rx.lang.scala.Observable

import scala.concurrent.Future

trait IOntContainer {
  def ontManager(): OntologyManager
  def owlDataFactory(): OWLDataFactory

  def ontoApi: IOntoApiProvider
  def ontology: OntologyModel
  def graphId(): String

  def shortPrefix(iri: String): String
  def expandPrefix(iri: String): String
  def getSubClassesFor(shortClassIri: String): List[OntoIRI]
  def getSubRelationsFor(shortRelIri: String): List[OntoIRI]

  def changes: Observable[RdfChange]

  def refreshReasoner(): Unit

  def silentTrans[T](lock: Boolean = Lock.WRITE)(op: (OntologyModel) => T): T
  def transact[T](lockType: Boolean = Lock.WRITE)(op: (OntologyModel) => T): T

  def wrapQueryWithPrefix(query: String): String

  def updateQueries(queries: Seq[TQueryBuilder], replyQuery: Boolean = false): Unit
  def updateQuery(query: TQueryBuilder, replyQuery: Boolean = false): Unit
  def queryBaseModel(queryStr: String): TQueryResults
  def queryReasoner(queryStr: String): TQueryResults
  def searchByFields(fields: Set[OntoIRI], value: String): Seq[SearchResult]

  def dispose(): Future[Done]
}
