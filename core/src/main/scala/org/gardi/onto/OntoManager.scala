package org.gardi.onto

import java.lang.System.currentTimeMillis

import org.apache.jena.shared.Lock
import org.gardi.LagomData
import org.gardi.onto.interfaces.{IOntContainer, IOntoLoader}
import org.slf4j.{Logger, LoggerFactory}
import rx.lang.scala.{Observable, Subscription}

import scala.collection.mutable
import scala.concurrent.{Await, Future}
import scala.concurrent.duration._
import scala.concurrent.ExecutionContext.Implicits.global


case class ObesrvabeleOntology(val onto: IOntContainer,
                               val watchDog: Subscription,
                               var lastAccess: Long,
                               var disabledWatchDog: Boolean) {
  def fillWatchDog(): Unit = lastAccess = currentTimeMillis()
  def disableWD(): Unit = disabledWatchDog = true
  def enableWD(): Unit = disabledWatchDog = false
}

class OntoManager(private val ontoLoader: IOntoLoader,
                  private val lagomData: Option[LagomData] = None,
                  private val watchDogDuration: Duration = 10 minutes) {

  private final val log: Logger =
    LoggerFactory.getLogger(lagomData match {
      case Some(data) => s"${classOf[OntoManager]}.${data.serviceName}"
      case None => classOf[OntoManager].toString
    })

  private val ontologies = mutable.Map[String, ObesrvabeleOntology]()

  private def getForUser(uuid: String, name: String): ObesrvabeleOntology = {
    val key = s"${uuid}:${name}"
    ontologies.synchronized {
      ontologies.get(key) match {
        case Some(wrapper) => { wrapper }
        case None => {
          log.info(s"Init onto ${name} for user ${uuid}")
          val onto = ontoLoader.initOntologyByName(name)
          val wrapper = ObesrvabeleOntology(onto,
            Observable.interval(watchDogDuration).subscribe(
              (time) => {
                log.debug(s"Check onto ${name} for user ${uuid}")
                val wrapper = ontologies(key)
                val duration = Duration(currentTimeMillis() - wrapper.lastAccess, MILLISECONDS)
                if (!wrapper.disabledWatchDog && duration >= watchDogDuration) {
                  wrapper.watchDog.unsubscribe()
                  wrapper.onto.dispose().onComplete((res) => {
                    ontologies.remove(key)
                    log.info(s"Dispose onto ${name} for user ${uuid}")
                  })
                }
              },
              (err) => err.printStackTrace()
            ), currentTimeMillis(), false)

          ontologies(key) = wrapper
          wrapper
        }
      }
    }
  }

  def action[T](uuid: String, name: String, lock: Boolean = Lock.WRITE)(op: (IOntContainer) => T): T = {
    val wrapper = getForUser(uuid, name)
    try {
      wrapper.disableWD()
      op(wrapper.onto)
    } finally {
      wrapper.fillWatchDog()
      wrapper.enableWD()
    }
  }

  def dispose(): Unit = {
    val waits = ontologies.values.map((wrapper: ObesrvabeleOntology) => {
      wrapper.onto.dispose()
    })
    Future.sequence(waits).onComplete((_) => {
      ontologies.clear()
    })
  }

}
