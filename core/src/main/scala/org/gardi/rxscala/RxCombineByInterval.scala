package org.gardi.rxscala

import java.util.concurrent.TimeUnit

import rx.Observable.{OnSubscribe, Transformer}
import rx.{Observable, Subscriber}
import rx.functions.{Action0, Action1}
import rx.observables.SyncOnSubscribe

import scala.collection.mutable.ListBuffer
import scala.concurrent.duration.Duration


class RxCombineByInterval[T](private val duration: Duration,
                             private val threshold: Int) extends Transformer[T, Seq[T]] {
  type Observer = Subscriber[_ >: Seq[T]]
  var delaySubs: Option[rx.Subscription] = None
  val items: ListBuffer[T] = ListBuffer()

  def unsibscribe(): Unit = (delaySubs: @unchecked) match {
    case Some(subs) => {
      if(!subs.isUnsubscribed) {
        subs.unsubscribe()
      }
      delaySubs = None
    }
    case None => ()
  }

  def tryToSchedule(observer: Observer): Unit = {
    (delaySubs: @unchecked) match {
      case Some(_) => {}
      case None => {
        delaySubs = Some(rx.Observable.just(0)
          .delay(duration.toMillis, TimeUnit.MILLISECONDS)
            .subscribe(new Action1[Int]() {
              override def call(t: Int): Unit = emit(observer)
            }))
      }
    }
  }

  def emit(observer: Observer): Unit = {
    items.synchronized {
      if (!items.isEmpty) {
        observer.onNext(items.toList)
        items.clear()
        unsibscribe()
      }
    }
  }

  override def call(s: rx.Observable[T]): rx.Observable[Seq[T]] = {
    rx.Observable.create(new Observable.OnSubscribe[Seq[T]]() {
      override def call(observer: Observer): Unit = {
        s.subscribe(
          new Action1[T] {
            override def call(value: T): Unit = {
              items.synchronized {
                items.append(value)
                if (items.length == threshold) {
                  unsibscribe()
                  emit(observer)
                } else {
                  tryToSchedule(observer)
                }
              }
            }
          },
          new Action1[Throwable] {
            override def call(err: Throwable): Unit = observer.onError(err)
          },
          new Action0 {
            override def call(): Unit = observer.onCompleted()
          }
        )
      }
    })
  }
}
