package org.gardi

import scala.concurrent.duration.Duration
import rx.lang.scala.Observable
import rx.lang.scala.JavaConversions.{toJavaObservable, toScalaObservable}

package object rxscala {
  implicit class ForObservble[T](x: Observable[T]) {
    def combineByInterval(duration: Duration, threshold: Int): Observable[Seq[T]] = {
      toScalaObservable(toJavaObservable(x).compose(new RxCombineByInterval[T](duration, threshold)))
    }
  }
}
