package org.gardi.users.interfaces

import org.apache.jena.rdf.model.Resource
import org.gardi.users.{AddThread, UserThread}
import org.gardi.utils.OntoEntity
import org.pac4j.core.profile.CommonProfile

abstract class IThreadsManager {
  def addThreadForUser(user: CommonProfile, thread: AddThread, threadURI: String): String

  def entityToThread(res: Resource, entity: OntoEntity): UserThread

  def asResource(uuid: String): Resource

  def getThreadForUser(user: CommonProfile, uuid: String): OntoEntity

  def getThreadsForUser(user: CommonProfile): Map[Resource, OntoEntity]

  def getOntoNameFromThread(thread: OntoEntity): String

  def getOntoNameFromId(profile: CommonProfile, threadId: String): String

  def getOntoNameFromId(uid: String, threadId: String): String
}
