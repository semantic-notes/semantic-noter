package org.gardi.users.interfaces

import com.typesafe.config.Config
import org.gardi.users.UserOntoRel.UserOntoRel
import org.gardi.users.{User, UserInfo, UserResponse}
import org.pac4j.core.profile.CommonProfile

abstract class IUsersManager {
  def ontoConfig: Config
  def getMd5(name: String): String
  def initialize(): Unit
  def createUser(newUser: User, originId: Option[(UserOntoRel, String)] = None): UserResponse
  def updateUserInfo(id: String, userOntoRel: UserOntoRel, value: String): Unit
  def checkPassword(email: String, password: String): Boolean

  def getUserFromQuery(query: String): UserInfo
  def getUserByEmail(email: String): UserInfo
  def getUserBy(rel: UserOntoRel, value: String): Option[UserInfo]
  def userToCommonProfile(user: UserInfo): CommonProfile
}
