package org.gardi.users

import org.joda.time.DateTime
import play.api.libs.json.{Format, Json}

object UserOntoRel extends Enumeration {
  type UserOntoRel = Value
  protected case class Val(val relName: String) extends super.Val {
    def toLocalRel(): String = s":${relName}"

    override def toString(): String = relName
  }
  implicit def valueToThis(v: Value): Val = v.asInstanceOf[Val]
  implicit def thisToString(v: UserOntoRel): String = v.relName

  val UUID = Val("hasUUID")
  val Name = Val("hasName")
  val PasswordHash = Val("hasPasswordHash")
  val AccessToken = Val("hasAccessToken")
  val RefreshToken = Val("hasRefreshToken")
  val Email = Val("hasEmail")
  val TelegramId = Val("hasTelegramId")
  val GoogleId = Val("hasGoogleId")
}

case class User(username: String,
                email: Option[String] = None,
                password: Option[String] = None,
                id: Option[String] = None)
object User {
  implicit val format: Format[User] = Json.format
}

case class UserInfo(id: String,
                    username: String,
                    accessToken: Option[String] = None,
                    refreshToken: Option[String] = None,
                    email: Option[String] = None,
                    telegramId: Option[String] = None,
                    googleId: Option[String] = None,
                    facebookId: Option[String] = None)

object UserInfo {
  implicit def userInfoToUserResponse(obj: UserInfo): UserResponse = UserResponse(obj.id, obj.username, obj.email, true)
}

case class UserResponse(id: String,
                        username: String,
                        email: Option[String] = None,
                        verified: Boolean)

object UserResponse {
  implicit val format: Format[UserResponse] = Json.format
}

case class UserAuth(id: String, tokenExpTime: Int, token: String, refresh: Option[String] = None)
object UserAuth {
  implicit val format: Format[UserAuth] = Json.format
}

case class UserLoginToken(token: String)
object UserLoginToken {
  implicit val format: Format[UserLoginToken] = Json.format
}

case class UserLogin(email: String, password: String)
object UserLogin {
  implicit val format: Format[UserLogin] = Json.format
}

case class UserLoginCode(id: String,
                         from: String,
                         totp: String,
                         validTo: Long)
object UserLoginCode {
  implicit val format: Format[UserLoginCode] = Json.format
}
