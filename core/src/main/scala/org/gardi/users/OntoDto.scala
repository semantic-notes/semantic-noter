package org.gardi.users

import org.gardi.dto._
import play.api.libs.json._

import scala.collection.mutable

case class AddThread(val name: String, val description: Option[String])
object AddThread {
  implicit val format: Format[AddThread] = Json.format
}

case class UserThread(val uuid: String,
                      val ontoModelUri: String,
                      val name: String,
                      val description: Option[String] = None,
                      var prefixMapping: Map[String, String] = Map.empty,
                      var annotations: List[OntoAnnotationDto] = Nil,
                      var classes: Option[OntoClassDto] = None,
                      var relations: Option[OntoRelationDto] = None,
                      var dataProperties: Option[OntoDataPropDto] = None,
                      var individuals: Set[OntoIndividualDto] = Set.empty)
object UserThread {
  implicit val formatMapStr_Str: Format[Map[String, String]] = Format[Map[String, String]] (
    Reads { js =>
      val jsObj = js.asInstanceOf[JsObject]
      JsSuccess(jsObj.value.mapValues[String](el => {
        val jsStr = el.asInstanceOf[JsString]
        jsStr.value
      }).toMap)
    },
    Writes {
      case value =>
        Json.toJsObject(value)
    }
  )

  implicit val format: Format[UserThread] = Json.format
}
