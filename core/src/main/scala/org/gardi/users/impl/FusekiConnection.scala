package org.gardi.users.impl

import com.gardi.shared.OntoConfig
import com.google.common.base.Supplier
import org.apache.http.auth.{AuthScope, UsernamePasswordCredentials}
import org.apache.http.impl.client.{BasicCredentialsProvider, HttpClients}
import org.apache.jena.rdfconnection.RDFConnectionFuseki
import org.apache.jena.system.Txn
import org.apache.jena.vocabulary.{OWL, RDF, RDFS}
import org.slf4j.{Logger, LoggerFactory}

class FusekiConnection(params: OntoConfig) {
  private final val log: Logger = LoggerFactory.getLogger("org.gardi.users.fuseki.connection")

  val baseOntoPath = params.baseOntoPath
  val baseUsersOntoPath = params.usersOntoPath
  val datasetName = params.datasetName
  var fusekiUrl = params.fusekiUrl

  val basePrefix = s"sparql:$datasetName"
  val usersGraphName = s"$basePrefix/users"

  val credsProvider = new BasicCredentialsProvider
  val credentials = new UsernamePasswordCredentials("admin", params.fusekiPasswd)
  credsProvider.setCredentials(AuthScope.ANY, credentials)
  val httpclient = HttpClients.custom
    .setDefaultCredentialsProvider(credsProvider)
    .evictExpiredConnections
    .build

  val builder = RDFConnectionFuseki.create()
    .httpClient(httpclient)
    .destination(fusekiUrl)
    .gspEndpoint(datasetName)
    .updateEndpoint(datasetName)
    .queryEndpoint(datasetName)

  val fusekiConn = builder.build().asInstanceOf[RDFConnectionFuseki]

  def wrapQueryWithPrefix(query: String): String = {
    s"""
      PREFIX : <${usersGraphName}#>
      PREFIX rdf: <${RDF.getURI}>
      PREFIX rdfs: <${RDFS.getURI}>
      PREFIX owl: <${OWL.getURI}>
      $query
      """
  }

  def close(): Unit = {
    fusekiConn.close()
  }

  def write(fn: (RDFConnectionFuseki) => Unit): Unit = {
    Txn.executeWrite(fusekiConn, new Runnable {
      override def run(): Unit = fn(fusekiConn)
    })
  }

  def read[T](fn: (RDFConnectionFuseki) => T): T = {
    Txn.calculateRead(fusekiConn, new Supplier[T] {
      override def get(): T = fn(fusekiConn)
    })
  }

  def action[T](fn: (RDFConnectionFuseki) => T): T = {
    fn(fusekiConn)
  }
}
