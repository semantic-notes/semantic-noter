package org.gardi.users.impl

import java.io.{File, FileInputStream}
import java.util.function.Consumer

import com.typesafe.config.ConfigFactory
import org.apache.jena.atlas.web.HttpException
import org.apache.jena.query.QuerySolution
import org.gardi.users.UserOntoRel.UserOntoRel
import org.gardi.users._
import org.gardi.users.interfaces.IUsersManager
import org.gardi.utils
import org.pac4j.core.context.Pac4jConstants
import org.pac4j.core.profile.CommonProfile
import org.pac4j.core.profile.definition.CommonProfileDefinition
import org.pac4j.http.credentials.CredentialUtil
import org.slf4j.{Logger, LoggerFactory}
import ru.avicomp.ontapi.OntManagers

import scala.collection.JavaConverters._
import scala.collection.mutable


class FusekiUsersManager(fusekiConnection: FusekiConnection) extends IUsersManager {

  private final val log: Logger = LoggerFactory.getLogger("org.gardi.users.fuseki")

  val ontoConfig = ConfigFactory.parseFile(new File("configurations/onto.conf"))

  val basePrefix = fusekiConnection.basePrefix
  val usersGraphName = fusekiConnection.usersGraphName

  def initialize(): Unit = {
    def doInitUserOnto(): Unit = {
      val ontManager = OntManagers.createConcurrentONT()
      val inputStream = new FileInputStream(new File(fusekiConnection.baseUsersOntoPath))
      val onto = ontManager.loadOntologyFromOntologyDocument(inputStream)
      fusekiConnection.action((conn) => {
        conn.put(usersGraphName, onto.asGraphModel())
      })
      ontManager.removeOntology(onto)
    }

    try {
      val query = s"SELECT ?s FROM <$usersGraphName> WHERE { ?s ?p ?o } LIMIT 1"
      var isEmpty = true;
      fusekiConnection.action((conn) => {
        conn.querySelect(query, new Consumer[QuerySolution] {
          override def accept(qs: QuerySolution): Unit = isEmpty = false
        })
      })
      if (isEmpty) {
        doInitUserOnto()
      }
    } catch {
      case err: HttpException if err.getResponseCode == 404 => doInitUserOnto()
    }
  }

  override def getMd5(name: String): String = CredentialUtil.encryptMD5(name)

  override def createUser(newUser: User, originId: Option[(UserOntoRel, String)]): UserResponse = {
    val uuid = utils.getNextId()

    val pswdHsh = newUser.password match {
      case Some(passwd) => {
        val salt = ontoConfig.getString("security.pswd-salt")
        Some(CredentialUtil.encryptMD5(salt, passwd))
      }
      case None => None
    }

    val query = s"""
         INSERT DATA { GRAPH <${usersGraphName}> {
              :${uuid} rdf:type owl:NamedIndividual .
              :${uuid} rdf:type :User .
              :${uuid} :${UserOntoRel.UUID} "${uuid}" .
              :${uuid} :${UserOntoRel.Name} "${newUser.username}" .
              ${newUser.email.map(email => s""":${uuid} :${UserOntoRel.Email} "${email}" .""").getOrElse("")}
              ${pswdHsh.map(pswdHsh => s""":${uuid} :${UserOntoRel.PasswordHash} "${pswdHsh}" .""").getOrElse("")}
              ${originId.map(el => s""":${uuid} :${el._1} "${el._2}" .""").getOrElse("")}
            }
         }
       """
    fusekiConnection.write((conn) => {
      conn.update(fusekiConnection.wrapQueryWithPrefix(query))
    })
    UserResponse(uuid, newUser.username, newUser.email, true)
  }

  override def updateUserInfo(id: String, userOntoRel: UserOntoRel, value: String): Unit = {
    val query = s"""
         WITH <${usersGraphName}>
         DELETE {
           GRAPH <${usersGraphName}> {
             ?subj ${userOntoRel} ?obj .
           }
         }
         INSERT {
            GRAPH <${usersGraphName}> {
              ?subj ${userOntoRel} "${value}" .
            }
         }
         WHERE {
           ?subj :${UserOntoRel.UUID} "${id}" .
         }
       """
    fusekiConnection.write((conn) => {
      conn.update(fusekiConnection.wrapQueryWithPrefix(query))
    })
  }

  override def checkPassword(email: String, password: String): Boolean = {
    val salt = ontoConfig.getString("security.pswd-salt")
    val pswdHsh = CredentialUtil.encryptMD5(salt, password)
    var isValid = false
    val query = s"""
        SELECT ?sub ?obj FROM <$usersGraphName> {
           ?sub :${UserOntoRel.Email} "${email}" .
           ?sub :${UserOntoRel.PasswordHash} ?obj .
        } LIMIT 1
       """
    fusekiConnection.read((conn) => {
      conn.querySelect(fusekiConnection.wrapQueryWithPrefix(query), new Consumer[QuerySolution] {
        override def accept(qs: QuerySolution): Unit = {
          val hash = qs.getLiteral("?obj").getString
          if (pswdHsh == hash) {
            isValid = true
          }
        }
      })
    })
    isValid
  }

  override def getUserFromQuery(query: String): UserInfo = {
    val fields = new mutable.HashMap[String, String]()
    fusekiConnection.read((conn) => {
      conn.querySelect(fusekiConnection.wrapQueryWithPrefix(query), new Consumer[QuerySolution] {
        override def accept(qs: QuerySolution): Unit = {
          val obj = qs.get("?obj")
          if (obj.isLiteral) {
            val key = qs.get("?pred").toString.replace(s"$usersGraphName#", "")
            fields(key) = qs.get("?obj").toString
          }
        }
      })
    })

    UserInfo(
      fields(UserOntoRel.UUID),
      fields(UserOntoRel.Name),
      fields.get(UserOntoRel.AccessToken),
      fields.get(UserOntoRel.RefreshToken),
      fields.get(UserOntoRel.Email),
      fields.get(UserOntoRel.TelegramId),
      fields.get(UserOntoRel.GoogleId)
    )
  }

  override def getUserByEmail(email: String): UserInfo = {
    val query = s"""
        SELECT ?sub ?pred ?obj FROM <$usersGraphName> {
           ?sub :${UserOntoRel.Email} "${email}" .
           ?sub ?pred ?obj .
        }
       """

    getUserFromQuery(query)
  }

  override def getUserBy(rel: UserOntoRel, value: String): Option[UserInfo] = {
    val query = s"""
        SELECT ?sub ?pred ?obj FROM <$usersGraphName> WHERE {
           ?sub :${rel} "${value}" .
           ?sub ?pred ?obj .
        }
       """

    try {
      Some(getUserFromQuery(query))
    } catch {
      case _: java.util.NoSuchElementException => None
    }
  }

  override def userToCommonProfile(user: UserInfo): CommonProfile = {
    val profile = new CommonProfile()
    profile.setId(user.id.toString)
    profile.addAttributes(Map[java.lang.String, java.lang.Object](
      Pac4jConstants.USERNAME -> user.username,
      CommonProfileDefinition.EMAIL -> user.email.orNull
    ).asJava)
    profile
  }
}
