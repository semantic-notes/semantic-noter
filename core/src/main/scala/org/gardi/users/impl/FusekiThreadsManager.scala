package org.gardi.users.impl

import org.apache.jena.query.QueryExecution
import org.apache.jena.rdf.model.{Resource, ResourceFactory}
import org.gardi.users.interfaces.IThreadsManager
import org.gardi.users.{AddThread, UserOntoRel, UserThread}
import org.gardi.utils.OntoEntity
import org.pac4j.core.profile.CommonProfile
import org.pac4j.http.credentials.CredentialUtil
import org.slf4j.{Logger, LoggerFactory}

import scala.collection.JavaConverters._

class FusekiThreadsManager(fusekiConnection: FusekiConnection) extends IThreadsManager {
  private final val log: Logger = LoggerFactory.getLogger("org.gardi.users.thread.fuseki")

  val basePrefix = fusekiConnection.basePrefix
  val usersGraphName = fusekiConnection.usersGraphName

  override def addThreadForUser(user: CommonProfile, thread: AddThread, threadURI: String): String = {
    val uuid = CredentialUtil.encryptMD5(thread.name)
    val query = s"""
         INSERT DATA { GRAPH <${usersGraphName}> {
              :${uuid} rdf:type owl:NamedIndividual .
              :${uuid} rdf:type :Thread .
              :${uuid} :hasThreadName "${thread.name}" .
              ${thread.description match {
                case Some(descr) => s""":${uuid} :hasThreadDescr "${descr}" ."""
                case None => ""
              }}
              :${uuid} :hasSPARqlURI "${threadURI}" .
              :${user.getId} :hasThread :${uuid} .
            }
         }
       """
    fusekiConnection.write((conn) => {
      conn.update(fusekiConnection.wrapQueryWithPrefix(query))
    })
    uuid
  }

  override def entityToThread(res: Resource, entity: OntoEntity): UserThread = {
    def getLocalName(): String = res.getURI.split("#")(1)
    UserThread(
      getLocalName,
      entity(ResourceFactory.createResource(s"$usersGraphName#hasSPARqlURI")).asLiteral().getString,
      entity(ResourceFactory.createResource(s"$usersGraphName#hasThreadName")).asLiteral().getString,
      entity.get(ResourceFactory.createResource(s"$usersGraphName#hasThreadDescr")).map(el => el.asLiteral().getString))
  }

  override def asResource(uuid: String): Resource = ResourceFactory.createResource(s"$usersGraphName#$uuid")

  override def getThreadForUser(user: CommonProfile, uuid: String): OntoEntity = {
    val query = s"""
        DESCRIBE :$uuid
        FROM <$usersGraphName>
       """

    fusekiConnection.read[OntoEntity]((conn) => {
      val model = conn.queryDescribe(fusekiConnection.wrapQueryWithPrefix(query))
      import org.gardi.utils._
      val ret = jena.Converters.statementsToEntity(model.listStatements().asScala.toStream)
      model.close()
      ret
    })
  }

  override def getThreadsForUser(user: CommonProfile): Map[Resource, OntoEntity] = {
    val query = s"""
        SELECT ?thr ?pred ?obj
        FROM <$usersGraphName>
        WHERE {
           ?usr :${UserOntoRel.UUID} "${user.getId}" .
           ?usr :hasThread ?thr .
           ?thr ?pred ?obj .
        }
       """
    fusekiConnection.read[Map[Resource, OntoEntity]]((conn) => {
      val exec: QueryExecution = conn.query(fusekiConnection.wrapQueryWithPrefix(query))
      import org.gardi.utils._
      val ret = jena.Converters.querySolutionGrpBy(exec.execSelect().asScala.toStream, "?thr")
      exec.close()
      ret
    })
  }

  override def getOntoNameFromThread(thread: OntoEntity): String = {
    thread(asResource("hasSPARqlURI")).asLiteral().getString.split("/")(1)
  }

  override def getOntoNameFromId(profile: CommonProfile, threadId: String): String = {
    val thread = getThreadForUser(profile, threadId)
    thread(asResource("hasSPARqlURI")).asLiteral().getString.split("/")(1)
  }

  override def getOntoNameFromId(uid: String, threadId: String): String = {
    val profile = new CommonProfile()
    profile.setId(uid)
    getOntoNameFromId(profile, threadId)
  }
}
