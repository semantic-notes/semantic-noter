package org.gardi.security

import java.security.{Key, KeyPair}

import javax.crypto.Cipher
import javax.crypto.spec.{IvParameterSpec, SecretKeySpec}
import org.apache.commons.codec.binary.Base64
import org.pac4j.jwt.util.JWKHelper

import scala.io.Source

class CryptoEngine(val algo: String, val encKey: Key, val decKey: Key) {
  val encEng = Cipher.getInstance(algo)
  encEng.init(Cipher.ENCRYPT_MODE, encKey)
  val decEng = Cipher.getInstance(algo)
  decEng.init(Cipher.DECRYPT_MODE, decKey)

  def encrypt(value: String): String = {
    val bytes = value.getBytes("utf-8")
    new String(Base64.encodeBase64(encEng.doFinal(bytes)), "utf-8")
  }

  def decrypt(value: String): String = {
    val bytes = value.getBytes("utf-8")
    new String(decEng.doFinal(Base64.decodeBase64(bytes)), "utf-8")
  }
}

package object crypto {
  private val loginCodeKey = "configurations/login-code-jwk.json"

  lazy val loginCodeCryptoEng = initLoginCodeEngine()

  def initLoginCodeEngine(): CryptoEngine = {
//    val fileContents = Source.fromFile(loginCodeKey).getLines.mkString
//    val keyPair = JWKHelper.buildRSAKeyPairFromJwk(fileContents)
    val key = new SecretKeySpec(Base64.decodeBase64("DxVnlUlQSu3E5acRu7HPwg=="), "AES")
    //new CryptoEngine("RSA", keyPair.getPublic, keyPair.getPrivate)
    new CryptoEngine("AES", key, key)
  }
}
