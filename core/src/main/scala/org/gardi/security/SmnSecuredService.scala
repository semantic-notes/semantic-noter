package org.gardi.security

import java.util.Collections.singletonList

import com.lightbend.lagom.scaladsl.api.transport.Forbidden
import org.pac4j.core.client.Client
import org.pac4j.core.credentials.Credentials
import org.pac4j.core.profile.{AnonymousProfile, CommonProfile}
import org.pac4j.lagom.scaladsl.SecuredService
import com.lightbend.lagom.scaladsl.server.ServerServiceCall
import org.pac4j.core.authorization.authorizer.Authorizer
import totp.{Authenticator, TOTPSecret}

import scala.concurrent.Future

trait SmnSecuredService extends SecuredService {

  /**
    * Service call composition for authentication.
    *
    * @param clientName Name of authentication client
    * @param serviceCall Service call
    * @tparam Request Type of request
    * @tparam Response Type of response
    * @return Service call with authentication logic
    */
  override def authenticate[Request, Response](
                                       clientName: String, serviceCall: CommonProfile => ServerServiceCall[Request, Response]): ServerServiceCall[Request, Response] =
    ServerServiceCall.compose { requestHeader =>
      val profile = try {
        val clients = securityConfig.getClients
        val defaultClient = clients.findClient(clientName).asInstanceOf[Client[Credentials, CommonProfile]]
        val context = new LagomWebContext(requestHeader)
        val credentials = defaultClient.getCredentials(context)
        defaultClient.getUserProfile(credentials, context)
      } catch {
        case ex: Exception =>
          println(ex.getMessage)
          // We can throw only TransportException.
          // Otherwise exception will be sent to the client with stack trace.
          new AnonymousProfile
      }

      serviceCall.apply(Option(profile).getOrElse(new AnonymousProfile))
    }

  /**
    * Service call composition for authorization.
    *
    * @param clientName Name of authentication client
    * @param authorizer Authorizer (may be composite)
    * @param serviceCall Service call
    * @tparam Request Type of request
    * @tparam Response Type of response
    * @return Service call with authorization logic
    */
  override def authorize[Request, Response](clientName: String,
                                            authorizer: Authorizer[CommonProfile],
                                            serviceCall: CommonProfile => ServerServiceCall[Request, Response]): ServerServiceCall[Request, Response] =
    authenticate(clientName, (profile: CommonProfile) => ServerServiceCall.compose { requestHeader =>
      val authorized = try {
        authorizer != null && authorizer.isAuthorized(new LagomWebContext(requestHeader), singletonList(profile))
      } catch {
        case ex: Exception =>
          // We can throw only TransportException.
          // Otherwise exception will be sent to the client with stack trace.
          false
      }
      if (!authorized) throw Forbidden("Authorization failed")
      serviceCall.apply(profile)
    })

  def checkTOTP(serviceName: String,
                totp: String,
                time: Long = System.currentTimeMillis / 30000,
                returnDigits: Int = 6,
                crypto: String = "HmacSha1",
                windowSize: Int = 3): Boolean = {
    val key = SecurityData.toStringValue(SecurityData.totpData \ serviceName \ "secret")
    val secret = TOTPSecret(key)
    Authenticator.totpSeq(secret, time, returnDigits, crypto, windowSize).contains(totp.trim)
  }

  def genTOTP(serviceName: String,
              time: Long = System.currentTimeMillis / 30000,
              returnDigits: Int = 6,
              crypto: String = "HmacSha1",
              windowSize: Int = 3) : String = {
    val key = SecurityData.toStringValue(SecurityData.totpData \ serviceName \ "secret")
    val secret = TOTPSecret(key)
    Authenticator.totp(secret, time, returnDigits, crypto)
  }

}
