package org.gardi

import java.io.File
import java.security.interfaces.{RSAPrivateKey, RSAPublicKey}

import com.nimbusds.jose.{EncryptionMethod, JWEAlgorithm}
import com.typesafe.config.ConfigFactory
import org.joda.time.DateTime
import org.pac4j.core.config.Config
import org.pac4j.core.context.HttpConstants
import org.pac4j.core.context.HttpConstants.{AUTHORIZATION_HEADER, BEARER_HEADER_PREFIX}
import org.pac4j.core.profile.CommonProfile
import org.pac4j.http.client.direct.HeaderClient
import org.pac4j.jwt.config.encryption.RSAEncryptionConfiguration
import org.pac4j.jwt.credentials.authenticator.JwtAuthenticator
import org.pac4j.jwt.profile.JwtGenerator
import org.pac4j.jwt.util.JWKHelper
import org.pac4j.oauth.client.Google2Client
import org.pac4j.oauth.client.Google2Client.Google2Scope

import scala.io.Source


package object security {
  type SecConfig = Config
  private val ontoConfig = ConfigFactory.parseFile(new File("configurations/onto.conf"))
  private val file = "configurations/jwt-jwk.json"
  private val fileContents = Source.fromFile(file).getLines.mkString
  private val algorithm = JWEAlgorithm.RSA_OAEP_256
  private val encryptionMethod = EncryptionMethod.A256GCM
  private val keyPair = JWKHelper.buildRSAKeyPairFromJwk(fileContents)

  def init(): Unit = {
    import java.security.KeyPairGenerator
    import java.util.UUID

    import com.nimbusds.jose.jwk.{KeyUse, RSAKey}
    // Generate the RSA key pair// Generate the RSA key pair

    val gen = KeyPairGenerator.getInstance("RSA")
    gen.initialize(2048)
    val keyPair = gen.generateKeyPair

    // Convert to JWK format
    val jwk = new RSAKey
      .Builder(keyPair.getPublic().asInstanceOf[RSAPublicKey])
      .privateKey(keyPair.getPrivate.asInstanceOf[RSAPrivateKey])
      .keyUse(KeyUse.ENCRYPTION)
      .keyID(UUID.randomUUID.toString)
      .build
  }

  def initializeJwtAuthenticator(): JwtAuthenticator = {
    val jwtAuthenticator = new JwtAuthenticator
    jwtAuthenticator.addEncryptionConfiguration(new RSAEncryptionConfiguration(keyPair, algorithm, encryptionMethod))
    jwtAuthenticator
  }

  def initializeJwtGenerator(): JwtGenerator[CommonProfile] = {
    val generator = new JwtGenerator[CommonProfile]()
    generator.setEncryptionConfiguration(new RSAEncryptionConfiguration(keyPair, algorithm, encryptionMethod))
    generator
  }

  def genRefreshToken(refershProfile: CommonProfile): Seq[String] = {
    refershProfile.addAttribute("initializedTime", DateTime.now().getMillis)
    refershProfile.addAttribute("type", "refresh")
    val authGen = org.gardi.security.initializeJwtGenerator()
    val expirationTimeInMs = ontoConfig.getInt("security.refreshExpTimeMs")
    authGen.setExpirationTime(DateTime.now.plusMillis(expirationTimeInMs).toDate)
    Seq(s"${HttpConstants.BEARER_HEADER_PREFIX}", authGen.generate(refershProfile))
  }

  def genToken(profile: CommonProfile): Seq[String] = {
    val authGen = org.gardi.security.initializeJwtGenerator()
    val expirationTimeInMs = ontoConfig.getInt("security.expirationTimeInMs")
    authGen.setExpirationTime(DateTime.now.plusMillis(expirationTimeInMs).toDate)
    Seq(s"${HttpConstants.BEARER_HEADER_PREFIX}", authGen.generate(profile))
  }

  def tokenExpTime(): Int = ontoConfig.getInt("security.expirationTimeInMs")
}
