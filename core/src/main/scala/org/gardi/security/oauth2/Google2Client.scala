package org.gardi.security.oauth2

import java.net.URLEncoder

import com.github.scribejava.apis.GoogleApi20
import org.gardi.security.oauth2.Google2Scope.Google2Scope
import org.pac4j.core.context.WebContext
import org.pac4j.core.http.callback.QueryParameterCallbackUrlResolver
import org.pac4j.core.http.url.UrlResolver
import org.pac4j.core.logout.GoogleLogoutActionBuilder
import org.pac4j.core.util.CommonHelper
import org.pac4j.oauth.client.OAuth20Client
import org.pac4j.oauth.exception.OAuthCredentialsException
import org.pac4j.oauth.profile.google2.Google2Profile
import org.pac4j.oauth.profile.google2.Google2ProfileDefinition
import java.util.function.{ Function ⇒ JFunction, Predicate ⇒ JPredicate, BiPredicate }

/**
  * <p>This class is the OAuth client to authenticate users in Google using OAuth protocol version 2.0.</p>
  * <p>The <i>scope</i> is by default : {@link Google2Scope#EMAIL_AND_PROFILE}, but it can also but set to : {@link Google2Scope#PROFILE}
  * or {@link Google2Scope#EMAIL}.</p>
  * <p>It returns a {@link org.pac4j.oauth.profile.google2.Google2Profile}.</p>
  * <p>More information at https://developers.google.com/accounts/docs/OAuth2Login</p>
  *
  * @author Jerome Leleu
  * @since 1.2.0
  */
object Google2Scope extends Enumeration {
  type Google2Scope = Value
  val EMAIL, PROFILE, EMAIL_AND_PROFILE = Value
}

object Google2Client {
  protected val PROFILE_SCOPE = "profile"
  protected val EMAIL_SCOPE = "email"
}

class Google2Client() extends OAuth20Client[Google2Profile] {
  protected var scope = Google2Scope.EMAIL_AND_PROFILE

  def this(key: String, secret: String) {
    this()
    setKey(key)
    setSecret(secret)
  }

  override protected def clientInit(): Unit = {
    CommonHelper.assertNotNull("scope", this.scope)
    val scopeValue = if (scope == Google2Scope.EMAIL) {
      Google2Client.EMAIL_SCOPE
    } else if (scope == Google2Scope.PROFILE) {
      Google2Client.PROFILE_SCOPE
    } else {
      Google2Client.PROFILE_SCOPE + " " + Google2Client.EMAIL_SCOPE
    }
    configuration.setApi(GoogleApi20.instance)
    configuration.setProfileDefinition(new Google2ProfileDefinition)
    configuration.setScope(scopeValue)
    configuration.setWithState(false)
    configuration.setHasBeenCancelledFactory(new JFunction[WebContext, java.lang.Boolean ] {
      override def apply(ctx: WebContext): java.lang.Boolean = {
        val error = ctx.getRequestParameter(OAuthCredentialsException.ERROR)
        // user has denied permissions
        if ("access_denied" == error) {
          true
        } else {
          false
        }
      }
    })
    defaultLogoutActionBuilder(new GoogleLogoutActionBuilder[Google2Profile])

//    this.setCallbackUrlResolver(new QueryParameterCallbackUrlResolver {
//      override def compute(urlResolver: UrlResolver,
//                           url: String, clientName:
//                           String,
//                           context: WebContext): String = urlResolver.compute(url, context)
//    })

    super.clientInit()
  }

  def getScope = scope

  def setScope(scope: Google2Scope): Unit = {
    this.scope = scope
  }
}

