package org.gardi.security

import com.lightbend.lagom.scaladsl.api.transport.RequestHeader
import org.pac4j.core.context.session.SessionStore

class LagomSessionStore(requestHeader: RequestHeader) extends SessionStore[LagomWebContext] {
  override def getOrCreateSessionId(context: LagomWebContext): String = {
    ""
  }

  override def get(context: LagomWebContext, key: String): AnyRef = {
    ""
  }

  override def set(context: LagomWebContext, key: String, value: Any): Unit = {
    println(s"$key : $value")
  }

  override def destroySession(context: LagomWebContext): Boolean = ???

  override def getTrackableSession(context: LagomWebContext): AnyRef = ???

  override def buildFromTrackableSession(context: LagomWebContext, trackableSession: Any): SessionStore[LagomWebContext] = ???

  override def renewSession(context: LagomWebContext): Boolean = ???
}
