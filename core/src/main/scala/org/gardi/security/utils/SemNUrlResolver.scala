package org.gardi.security.utils

import org.pac4j.core.context.WebContext
import org.pac4j.core.http.callback.CallbackUrlResolver
import org.pac4j.core.http.url.UrlResolver

class SemNUrlResolver(val clientName: String) extends CallbackUrlResolver {
  val callbackUrlParam = "callback-url"

  override def compute(urlResolver: UrlResolver, url: String, clientName: String, context: WebContext): String = {
    getUrlFromHttpHeader(context)
  }

  override def matches(clientName: String, context: WebContext): Boolean = this.clientName == clientName

  def getUrlFromHttpHeader(context: WebContext): String = context.getRequestHeader(callbackUrlParam)

}
