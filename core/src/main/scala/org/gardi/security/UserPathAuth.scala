package org.gardi.security
import java.util
import java.util.function.Predicate

import org.pac4j.core.authorization.authorizer.Authorizer
import org.pac4j.core.context.WebContext
import org.pac4j.core.profile.CommonProfile
import org.pac4j.core.authorization.authorizer.IsAuthenticatedAuthorizer

class UserPathAuth(private val uuid: String) extends IsAuthenticatedAuthorizer[CommonProfile] {
  override def isAuthorized(context: WebContext, profiles: util.List[CommonProfile]): Boolean = {
    if (super.isAuthorized(context, profiles)) {
      val userPorfiles = profiles.stream()
        .filter(new Predicate[CommonProfile] {
          override def test(prof: CommonProfile): Boolean = prof.getId == uuid
        })
        .toArray
      !userPorfiles.isEmpty
    } else {
      false
    }
  }
}

object UserPathAuth {
  def isUserPathFor(uuid: String): UserPathAuth = new UserPathAuth(uuid)
}
