package org.gardi.security

import java.util
import scala.collection.JavaConverters._

import com.lightbend.lagom.scaladsl.api.transport.RequestHeader
import io.lemonlabs.uri.Url
import org.pac4j.core.context.session.SessionStore
import org.pac4j.core.context.{Cookie, WebContext}
import org.pac4j.core.exception.TechnicalException

class LagomWebContext(requestHeader: RequestHeader) extends WebContext {

  val url = Url.parse(requestHeader.uri.toString)
  val sessionStore = new LagomSessionStore(requestHeader)

  override def getSessionStore: SessionStore[_ <: WebContext] = sessionStore

  override def getRequestParameter(s: String): String = url.query.param(s).orNull

  override def getRequestParameters: util.Map[String, Array[String]] =
    (url.query.paramMap map {case (key, value) => (key, Array(value: _*))}).asJava

  override def getRequestAttribute(s: String): AnyRef = throw new TechnicalException("Operation not supported")

  override def setRequestAttribute(s: String, o: Any): Unit = throw new TechnicalException("Operation not supported")

  override def getRequestHeader(name: String): String = requestHeader.getHeader(name) match {
    case Some(value) => value
    case None => throw new TechnicalException(s"Header $name not found")
  }

  override def getRequestMethod: String = requestHeader.method.name

  override def getRemoteAddr: String = throw new TechnicalException("Operation not supported")

  override def writeResponseContent(s: String): Unit = throw new TechnicalException("Operation not supported")

  override def setResponseStatus(i: Int): Unit = throw new TechnicalException("Operation not supported")

  override def setResponseHeader(s: String, s1: String): Unit = {}

  override def setResponseContentType(s: String): Unit = throw new TechnicalException("Operation not supported")

  override def getServerName: String = url.hostOption.map(_.toString()).orNull

  override def getServerPort: Int = url.port.getOrElse(-1)

  override def getScheme: String = url.schemeOption.orNull

  override def isSecure: Boolean = false

  override def getFullRequestURL: String = url.toUrl.toString()

  override def getRequestCookies: util.Collection[Cookie] = throw new TechnicalException("Operation not supported")

  override def addResponseCookie(cookie: Cookie): Unit = throw new TechnicalException("Operation not supported")

  override def getPath: String = requestHeader.uri.getPath

}

