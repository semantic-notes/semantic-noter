package org.gardi.security


import java.io.FileInputStream

import com.lightbend.lagom.scaladsl.api.LagomConfigComponent
import org.gardi.security.oauth2.{Google2Client, Google2Scope}
import org.gardi.security.utils.SemNUrlResolver
import org.pac4j.core.authorization.authorizer.IsAnonymousAuthorizer.isAnonymous
import org.pac4j.core.authorization.authorizer.IsAuthenticatedAuthorizer.isAuthenticated
import org.pac4j.core.context.HttpConstants.{AUTHORIZATION_HEADER, BEARER_HEADER_PREFIX}
import org.pac4j.core.profile.CommonProfile
import org.pac4j.http.client.direct.HeaderClient
import play.api.libs.json.{JsLookupResult, JsString, JsValue, Json}

trait SecurityModel extends LagomConfigComponent {
  val CLIENT_NAME = "HttpHeaderClient"
  val HEADER_JWT_CLIENT ="JwtHttpHeaderClient"



  lazy val client: HeaderClient = {
    val headerClient = new HeaderClient
    headerClient.setHeaderName(AUTHORIZATION_HEADER)
    headerClient.setPrefixHeader(BEARER_HEADER_PREFIX)
    headerClient.setAuthenticator(initializeJwtAuthenticator())
    headerClient.setName(HEADER_JWT_CLIENT)
    headerClient
  }

  lazy val googleClient = {
    val clientName = "Google"
    val client = new Google2Client(
      SecurityData.toStringValue(SecurityData.googleKey \ "web" \ "client_id"),
      SecurityData.toStringValue(SecurityData.googleKey \ "web" \ "client_secret")
    )
    client.setScope(Google2Scope.EMAIL)
    client.setCallbackUrl("http://fake")
    client.setCallbackUrlResolver(new SemNUrlResolver(clientName))
    client.setName(clientName)
    client
  }

  lazy val serviceConfig: SecConfig = {
    val config = new SecConfig(client, googleClient)
    config.getClients.setDefaultSecurityClients(client.getName)
    config.addAuthorizer("_anonymous_", isAnonymous[CommonProfile])
    config.addAuthorizer("_authenticated_", isAuthenticated[CommonProfile])
    config
  }
}

object SecurityData {
  lazy val googleKey = {
    val stream = new FileInputStream("configurations/oauth2/google.json")
    try { Json.parse(stream) } finally { stream.close() }
  }

  lazy val totpData = {
    val stream = new FileInputStream("configurations/totp.json")
    try { Json.parse(stream) } finally { stream.close() }
  }

  def toStringValue(json: JsLookupResult): String = json.toOption.map(el => el.asInstanceOf[JsString].value).orNull
}
