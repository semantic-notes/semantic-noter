package com.gardi.filterService

import akka.actor.ActorSystem
import com.gardi.base.service.BaseService
import com.gardi.jena.fuseki.FusekiService
import com.gardi.shared.OntoParams
import com.lightbend.lagom.scaladsl.api.ServiceCall
import com.lightbend.lagom.scaladsl.server.ServerServiceCall
import com.typesafe.config.Config
import org.gardi.dto.{EntityQueryParams, OntoIRI, OntoIndividualDto, SearchParams, SearchResult}
import org.gardi.onto.interfaces.IOntContainer
import org.gardi.security.{SecConfig, SmnSecuredService, UserPathAuth}
import org.pac4j.core.profile.CommonProfile
import org.slf4j.{Logger, LoggerFactory}
import play.api.inject.ApplicationLifecycle

import scala.concurrent.duration._
import scala.concurrent.{Await, ExecutionContext, Future}

class FilterServiceImpl(config: Config,
                        actorSystem: ActorSystem,
                        override val securityConfig: SecConfig,
                        applicationLifecycle: ApplicationLifecycle,
                        fuseki: FusekiService,
                        ontoParams: Option[OntoParams] = None)(implicit ec: ExecutionContext)
  extends FilterService with BaseService with SmnSecuredService {

  final val log: Logger = LoggerFactory.getLogger(classOf[FilterService])

  override def serviceId(): Int = 3

  val future = initService(config, actorSystem, applicationLifecycle, fuseki)(ec)
  Await.ready(future, 10 seconds)

  def searchByFields(uuid: String, threadId: String): ServiceCall[SearchParams, List[SearchResult]] = {
    authorize(
      UserPathAuth.isUserPathFor(uuid),
      (profile: CommonProfile) => ServerServiceCall { params: SearchParams =>
        val ontoName = threadsManager.map(man => man.getOntoNameFromId(uuid, threadId)).get
        ontoManager.map(manager => {
          manager.action(uuid, ontoName) { ontCntr: IOntContainer =>
            val results = ontCntr.searchByFields(params.fields.map(el => OntoIRI(el)), params.value).toList
            Future.successful(results)
          }
        }).get
      })
  }

  def getEntities(uuid: String, threadId: String): ServiceCall[EntityQueryParams, Set[OntoIndividualDto]] = {
    authorize(
      UserPathAuth.isUserPathFor(uuid),
      (profile: CommonProfile) => ServerServiceCall { params: EntityQueryParams =>
        val ontoName = threadsManager.map(man => man.getOntoNameFromId(uuid, threadId)).get
        val res = ontoManager.map(manager => {
          manager.action[Set[OntoIndividualDto]](uuid, ontoName) { ontCntr: IOntContainer =>
            val result = params.searchParams
              .map(searchParams => {
                ontCntr.searchByFields(searchParams.fields.map(el => OntoIRI(el)), searchParams.value)
              })
              .map(result => {
                import org.gardi.utils
                result.map(el => utils.getOntoIndividual(utils.getIndividualFrom(el.iri), ontCntr)).toSet
              })
            result.getOrElse(Set[OntoIndividualDto]())
          }
        }).get
        Future.successful(res)
      }
    )
  }

}