package com.gardi.filterService

import com.lightbend.lagom.scaladsl.api.transport.Method
import com.lightbend.lagom.scaladsl.api.{Service, ServiceCall}
import org.gardi.dto.{EntityQueryParams, OntoIndividualDto, SearchParams, SearchResult}

trait FilterService extends Service {

  def searchByFields(uuid: String,
                     threadId: String): ServiceCall[SearchParams, List[SearchResult]]

  def getEntities(uuid: String,
                  threadId: String): ServiceCall[EntityQueryParams, Set[OntoIndividualDto]]

  override final def descriptor = {
    import Service._    // @formatter:off
    named("filter-service")
      .withCalls(
        restCall(Method.GET, "/api/users/:uuid/threads/:threadId/searchByFields", searchByFields _),
        restCall(Method.GET, "/api/users/:uuid/threads/:threadId/filteredInd", getEntities _)
      )
      .withAutoAcl(true)
    //      .withExceptionSerializer(new DefaultExceptionSerializer(Environment.simple(mode = Mode.Prod)))
    // @formatter:on
  }
}
