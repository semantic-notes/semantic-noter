include "application"

play.http.secret.key = "I/qT+sQU26zBosDMcjjOiJ1LS/4nRDlT1HHT3rTZ6Ng="

play.filters.hosts {
  # Requests that are not from one of these hosts will be rejected.
  #allowed = [${?ALLOWED_HOST}]
}

play {
  server {
    pidfile.path = "/dev/null"
  }
}

lagom.broker.kafka {
  # If this is an empty string, then the Lagom service locator lookup will not be done,
  # and the brokers configuration will be used instead.
  service-name = ""

  # The URLs of the Kafka brokers. Separate each URL with a comma.
  # This will be ignored if the service-name configuration is non empty.
  brokers = ${?KAFKA_BROKERS_SERVICE_URL}
}

akka.management {

  http.routes {
    # registers http management to be included in akka-management's http endpoint
    cluster-management = "akka.management.cluster.ClusterHttpManagementRouteProvider"
  }

  cluster {
    health-check {
      # Ready health check returns 200 when cluster membership is in the following states.
      # Intended to be used to indicate this node is ready for user traffic so Up/WeaklyUp
      # Valid values: "Joining", "WeaklyUp", "Up", "Leaving", "Exiting", "Down", "Removed"
      ready-states = ["Up", "WeaklyUp"]
    }
  }

}

akka {
  discovery {
    method = akka-dns
  }
}

akka.management {
  cluster.bootstrap {
    contact-point-discovery {
      discovery-method = akka-dns
      required-contact-point-nr = 1
    }
  }

  http {
    port = 8558
    bind-hostname = "0.0.0.0"
  }
}

#Shutdown if we have not joined a cluster after one minute.
akka.cluster.shutdown-after-unsuccessful-join-seed-nodes = 60s

# Necessary to ensure Lagom successfully exits the JVM on shutdown. The second one should no longer be needed after Lagom 1.5.0 or later.
lagom.cluster.exit-jvm-when-system-terminated = on