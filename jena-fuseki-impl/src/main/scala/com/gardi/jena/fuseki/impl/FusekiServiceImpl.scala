package com.gardi.jena.fuseki.impl

import java.io.File

import akka.NotUsed
import akka.actor.ActorSystem
import com.gardi.jena.fuseki.{FusekiService, TripleStoreInfo}
import com.gardi.shared.{OntoConfig, OntoParams}
import com.lightbend.lagom.scaladsl.api.ServiceCall
import com.lightbend.lagom.scaladsl.server.LagomApplicationContext
import com.typesafe.config.{Config, ConfigFactory}
import org.apache.jena.fuseki.main.FusekiServer
import org.slf4j.{Logger, LoggerFactory}
import play.api.inject.ApplicationLifecycle

import scala.concurrent.Future

class FusekiServiceImpl(config: Config,
                        actorSystem: ActorSystem,
                        context: LagomApplicationContext,
                        applicationLifecycle: ApplicationLifecycle,
                        ontoParams: Option[OntoParams] = None) extends FusekiService {

  private final val log: Logger =
    LoggerFactory.getLogger(classOf[FusekiService])

  def getOntoConfigPath: Option[OntoParams] = ontoParams
  val baseOntoCfg = ConfigFactory
    .parseFile(new File(getOntoConfigPath.map(_.ontoConfigPath).getOrElse("configurations/onto.conf")))
  val cfg = OntoConfig(baseOntoCfg)

  val port = 3030

  val server = FusekiServer.create()
    .loopback(false)
    .port(port)
    .parseConfigFile(cfg.storageConfig)
    .build()

  server.start()
  log.info("Fuseki server is started...")

  applicationLifecycle.addStopHook(() => {
    server.stop()
    log.info("Fuseki server is finished")
    Future.successful()
  })

  def _getSettings(): TripleStoreInfo = {
    val info = server.server.getURI
    try {
      val inet = java.net.InetAddress.getByName(serviceName)
      val url = s"${info.getScheme}://${serviceName}:${info.getPort}"
      TripleStoreInfo(url)
    } catch {
      case err: Throwable => {
        val url = s"${info.getScheme}://${info.getHost}:${info.getPort}"
        TripleStoreInfo(url)
      }
    }
  }

  override def getSettings(): ServiceCall[NotUsed, TripleStoreInfo] = ServiceCall { _ =>
    Future.successful(_getSettings())
  }
}
