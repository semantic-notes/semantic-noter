package com.gardi.shared

import com.typesafe.config.Config

sealed case class OntoConfig(datasetName: String,
                             usersOntoPath: String,
                             baseOntoPath: String,
                             storageConfig: String,
                             fusekiUrl: String,
                             fusekiPasswd: String)

object OntoConfig {
  def apply(cfg: Config): OntoConfig = {
    new OntoConfig(
      cfg.getString("jena.fuseki.dataset"),
      cfg.getString("ontology-cfg.users-onto-path"),
      cfg.getString("ontology-cfg.base-onto-path"),
      cfg.getString("jena.fuseki.storageConfig"),
      cfg.getString("jena.fuseki.url"),
      cfg.getString("jena.fuseki.creds.admin")
    )
  }
}
